// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

pragma Singleton
import QtQuick 2.6
import Sailfish.Silica 1.0

MyPaletteProto { signal paletteChanged() }

// vim: expandtab ts=4 st=4 filetype=javascript
