// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"
import "controls"

Column {
    id: col
    width: parent.width
    spacing: Theme.paddingSmall
    anchors.horizontalCenter: parent.horizontalCenter
    Connections{
        target: MyPalette
        onPaletteChanged: col.collapseAll()
    }
    function collapseAll() {
        prisl.visible = false
        secsl.visible = false
        hilsl.visible = false
        shisl.visible = false
        bghsl.visible = false
        dhisl.visible = false
        glosl.visible = false
    }
    CollapsingHeader { text: qsTr("Primary Color"); target: prisl; collapsedColor: MyPalette.primaryColor }
    ColorSlider { id: prisl
        visible: false
        onOutcolorChanged: MyPalette.primaryColor = outcolor
        onVisibleChanged: { incolor = MyPalette.primaryColor }
    }
    CollapsingHeader { text: qsTr("Secondary Color") ; target: secsl; collapsedColor: MyPalette.secondaryColor }
    ColorSlider { id: secsl
        visible: false
        onOutcolorChanged: MyPalette.secondaryColor = outcolor
        onVisibleChanged: { incolor = MyPalette.secondaryColor }
        Component.onCompleted: { incolor = MyPalette.secondaryColor }
    }
    CollapsingHeader { text: qsTr("Highlight Color"); target: hilsl; collapsedColor: MyPalette.highlightColor }
    ColorSlider { id: hilsl
        visible: false
        onOutcolorChanged: MyPalette.highlightColor = outcolor
        onVisibleChanged: { incolor = MyPalette.highlightColor }
        Component.onCompleted: { incolor = MyPalette.highlightColor }
    }
    CollapsingHeader { text: qsTr("Secondary Highlight Color"); target: shisl; collapsedColor: MyPalette.secondaryHighlightColor }
    ColorSlider { id: shisl
        visible: false
        onOutcolorChanged: MyPalette.secondaryHighlightColor = outcolor
        onVisibleChanged: { incolor = MyPalette.secondaryHighlightColor }
        Component.onCompleted: { incolor = MyPalette.secondaryHighlightColor }
    }
    CollapsingHeader { text: qsTr("Background Highlight Color"); target: bghsl; collapsedColor: MyPalette.highlightBackgroundColor }
    ColorSlider { id: bghsl
        visible: false
        onOutcolorChanged: MyPalette.highlightBackgroundColor = outcolor
        onVisibleChanged: { incolor = MyPalette.highlightBackgroundColor }
        Component.onCompleted: { incolor = MyPalette.highlightBackgroundColor }
    }
    CollapsingHeader { text: qsTr("Dim Highlight Color"); target: dhisl; collapsedColor:  MyPalette.highlightDimmerColor }
    ColorSlider { id: dhisl
        visible: false
        onOutcolorChanged: MyPalette.highlightDimmerColor = outcolor
        onVisibleChanged: { incolor = MyPalette.highlightDimmerColor }
        Component.onCompleted: { incolor = MyPalette.highlightDimmerColor }
    }
    CollapsingHeader { text: qsTr("Background Glow Color"); target: glosl }
    GlowSlider { id: glosl
        visible: false
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
