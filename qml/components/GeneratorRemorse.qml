// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import ".."

RemorsePopup {
  onCanceled: {
    colorsInitialized = false
    initColors()
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript
