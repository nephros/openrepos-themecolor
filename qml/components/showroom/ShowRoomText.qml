// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import ".."

Flow { id: textsr
  anchors.horizontalCenter: parent.horizontalCenter

  /* ************* Background Colors *****************/
  ShowRoomTextBG {
    bgcolor: "transparent"
    text1: qsTr("%1 text", "%1 is the name of a color").arg(qsTr("Primary"))
    text2: qsTr("No Background")
    textcolor1: MyPalette.primaryColor
    textcolor2: MyPalette.primaryColor
  }
  ShowRoomTextBG {
    bgcolor: "transparent"
    text1: qsTr("%1 text", "%1 is the name of a color").arg(qsTr("Secondary"))
    text2: qsTr("No Background")
    textcolor1: MyPalette.secondaryColor
    textcolor2: MyPalette.secondaryColor
  }
  ShowRoomTextBG {
    bgcolor: Theme.rgba(MyPalette.highlightBackgroundColor, MyPalette.highlightBackgroundOpacity)
    text1: qsTr("%1 text", "%1 is the name of a color").arg(qsTr("Secondary"))
    text2: qsTr("Highlight Background")
    textcolor1: MyPalette.secondaryColor
    textcolor2: MyPalette.secondaryColor
  }
  ShowRoomTextBG {
    bgcolor: MyPalette.overlayBackgroundColor
    bgopacity: Theme.opacityOverlay
    text1: qsTr("%1 text", "%1 is the name of a color").arg(qsTr("Primary"))
    text2: qsTr("Overlay Background")
    textcolor1: MyPalette.primaryColor
    textcolor2: MyPalette.primaryColor
  }
  ShowRoomTextBG {
    bgcolor: MyPalette._coverOverlayColor
    text1: qsTr("%1 text", "%1 is the name of a color").arg(qsTr("Secondary"))
    text2: qsTr("Cover Background")
    textcolor1: MyPalette.secondaryColor
    textcolor2: MyPalette.secondaryColor
  }
 ShowRoomTextBG {
   bgcolor: MyPalette._wallpaperOverlayColor
    text1: qsTr("%1 text", "%1 is the name of a color").arg(qsTr("Primary"))
    text2: qsTr("Wallpaper Overlay")
    textcolor1: MyPalette.primaryColor
    textcolor2: MyPalette.primaryColor
 }

}

// vim: expandtab ts=4 softtabstop=4 filetype=javascript
