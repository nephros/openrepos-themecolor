// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import ".."

Switch {
  property color iconcolor
  icon.sourceSize.height: Theme.iconSizeMedium;
  icon.source: "image://theme/icon-m-ambience?" + iconcolor
  TouchBlocker { anchors.fill: parent }
}
// vim: expandtab ts=4 st=4 filetype=javascript
