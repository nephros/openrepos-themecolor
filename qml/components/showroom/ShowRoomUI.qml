// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import ".."

Flow { id: uisr
    anchors.horizontalCenter: parent.horizontalCenter
    /* ************* Remorse Simulator *****************/
    Item { id: remorsesim
        width: isLandscape ? parent.width/3 : parent.width
        height: Theme.itemSizeMedium
        Label { id: pbitem
            width: parent.width
            height: Theme.itemSizeLarge
            anchors.horizontalCenter: parent.horizontalCenter;
            horizontalAlignment : Text.alignHCenter
            text: qsTr("Waiting…")
        }
        Timer {
            id: timer
            interval: 7000
            repeat: true
            running: uisr.visible
            triggeredOnStart: true
            onTriggered: remorse.execute(pbitem,qsTr("Feeling Remorse"), function() {}, interval/3)
        }
        RemorseItem { id: remorse;
            palette.highlightBackgroundColor: MyPalette.highlightBackgroundColor
            //_backgroundColor: MyPalette.highlightDimmerColor
        }
    }
    /* ************* TopMenu Simulator *****************/
    SilicaItem { id: topmenu
        width:remorsesim.width
        height: Theme.itemSizeLarge
        Rectangle {
            z: -1
            anchors.fill: parent
            anchors.centerIn: parent
            anchors.horizontalCenter: parent.horizontalCenter;
            color: MyPalette.overlayBackgroundColor
            opacity: Theme.opacityOverlay
        }
        Row {
            id: tbrow
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter;
            //height: Theme.iconSizeMedium
            spacing: Theme.iconSizeMedium
            ColorIndicator3 { iconcolor: MyPalette.highlightColor;   icon: "image://theme/icon-m-wlan-2" }
            ColorIndicator3 { iconcolor: MyPalette.primaryColor; icon: "image://theme/icon-m-location" }
            ColorIndicator3 { iconcolor: MyPalette.highlightColor; icon: "image://theme/icon-m-bluetooth"; }
            ColorIndicator3 { iconcolor: MyPalette.primaryColor; icon: "image://theme/icon-m-ambience"; }
        }
    }
    /* ************* Icon Buttons *****************/
    SilicaItem {
        width:remorsesim.width
        height:ibrow.height
        Row { id: ibrow
            anchors.horizontalCenter: parent.horizontalCenter
            ColorIndicator1 { enabled: false; iconcolor: MyPalette.primaryColor}
            ColorIndicator1 { checked: false; iconcolor: MyPalette.primaryColor }
            ColorIndicator1 { checked: true;  iconcolor: MyPalette.secondaryColor }
            ColorIndicator1 { checked: false; iconcolor: MyPalette.highlightColor }
            ColorIndicator1 { checked: true;  iconcolor: MyPalette.secondaryHighlightColor }
        }
    }
     /* ************* App Cover Simulator *****************/
    SilicaItem {
        width:remorsesim.width
        height:crow.height
        Row { id: crow; spacing: Theme.paddingLarge
            anchors.horizontalCenter: parent.horizontalCenter
            Repeater {
                model: 3
                delegate: Rectangle {
                    width: Theme.coverSizeSmall.width - parent.spacing
                    height: Theme.coverSizeSmall.height - parent.spacing
                    radius: Theme.paddingMedium
                    //color: (index === 0) ? MyPalette.wallpaperOverlayColor : MyPalette.coverOverlayColor
                    //  opacity: MyPalette.coverOverlayOpacity
                    color: switch (index) {
                        //case 0: return Theme.rgba(MyPalette.highlightBackgroundColor, MyPalette.opacityLow); break
                        //case 1: return Theme.rgba(Theme.highlightFromColor(MyPalette.coverOverlayColor, Theme.colorScheme), MyPalette.opacityLow); break
                        case 0: return Theme.rgba(MyPalette.coverOverlayColor, MyPalette.opacityFaint); break
                        case 1: return Theme.rgba(MyPalette.highlightColor, Theme.opacityFaint); break
                        case 2: return Theme.rgba(MyPalette.overlayColor, MyPalette.opacityFaint); break
                    }
                    Image {
                        source: Theme._patternImage
                        fillMode: Image.Tile
                        anchors.fill: parent
                        opacity: Theme.opacityFaint
                        Component.onCompleted: {
                            var w = sourceSize.width;
                            var h = sourceSize.height;
                            sourceSize.width = w * Screen.widthRatio;
                            sourceSize.height = h * Screen.widthRatio;
                        }
                    }
                    Label {
                        anchors.centerIn: parent
                        text: switch (index) {
                            case 0: return qsTr("App")
                            case 1: return qsTr("Clicked")
                            case 2: return qsTr("Dead")
                        }
                    }
                }
            }
        }
    }
    /* ************* Pulley Menu Simulator *****************/
    ListItem { id: pmenu
        width: remorsesim.width
        highlighted: true
        contentHeight: 0
        //contentHeight: Theme.itemSizeMedium
        Component.onCompleted: openMenu()
        onClicked: openMenu()
        onVisibleChanged: if (visible && !menuOpen) openMenu()
        menu: ctxmenu
        ContextMenu { id: ctxmenu
            width:pmenu.width
            closeOnActivation: false
            _closeOnOutsideClick: false
            backgroundColor: Theme.rgba(MyPalette.highlightBackgroundColor, MyPalette.highlightBackgroundOpacity)
            MenuItem { text: qsTr("MenuItem");                                   color: MyPalette.highlightColor; height: Theme.itemSizeExtraSmall; highlighted: true}
            MenuItem { text: qsTr("MenuItem") + " " + qsTr("disabled");                                           height: Theme.itemSizeExtraSmall; enabled: false; }
            MenuItem { text: qsTr("MenuItem") + " " + qsTr("selected");          color: MyPalette.primaryColor;   height: Theme.itemSizeExtraSmall; down: false; highlighted: false
                HighlightBar {
                    width: parent.width
                    highlightedItem: parent;
                    anchors.horizontalCenter: parent.horizontalCenter;
                    anchors.verticalCenter: parent.verticalCenter;
                    color: Theme.rgba(MyPalette.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
                }
            }
            MenuItem { text: " "; height: Theme.itemSizeTiny; enabled: false; }
        }
        //TouchBlocker { anchors.fill: pmenu; z: 100 }
    }
    /* ************* Buttons *****************/
    SilicaItem {
        width:remorsesim.width
        height:remorsesim.height
        Row { id: brow
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: Theme.paddingSmall
            Button {          text: qsTr("Button"); width: Theme.buttonWidthExtraSmall; color: MyPalette.primaryColor }
            SecondaryButton { text: qsTr("Button"); width: Theme.buttonWidthExtraSmall; color: MyPalette.primaryColor }
        }
    }
   SilicaItem {
        width: parent.width
        height: kb.height
        FakeVKB{ id: kb
            anchors.horizontalCenter: parent.horizontalCenter
            width:remorsesim.width
        }
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
