// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../"

SilicaItem {
  property var keys: [
    "1", "2", "3", "4", "5", "6", "7",
    "Q", "W", "E", "r", "t", "y", "0",
    "@", "#", "$", "%", "^", "&", "9"
  ]
  property int wantRows: 3

  height: grid.height
  width: parent.width
  Grid {
    id: grid
    columns: Math.round( keys.length / wantRows )
    anchors.horizontalCenter: parent.horizontalCenter;
    rowSpacing: Theme.paddingLarge*2
    padding: Theme.paddingLarge
    horizontalItemAlignment: Grid.AlignHCenter
    verticalItemAlignment: Grid.AlignVCenter
    Repeater {
      model: keys.length
      delegate: Label {
        width: Theme.itemSizeExtraSmall - Theme.paddingSmall
        height: Theme.itemSizeSmall
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: Theme.fontSizeMedium
        color: MyPalette.primaryColor
        text: keys[index]
        Rectangle { anchors.right: parent.right; anchors.top: parent.top; height: parent.height; width: 1;
          gradient: Gradient {
            GradientStop { position: -0.2; color: Qt.lighter(Theme.rgba(MyPalette.highlightBackgroundColor, 1.0), 1.5) }
            GradientStop { position: 0.5; color: Theme.rgba(MyPalette.highlightBackgroundColor, 1.0) }
            GradientStop { position: 1.2; color: "transparent" }
          }
        }
        Rectangle { anchors.left: parent.left; anchors.top: parent.top; height: parent.height; width: 1;
          gradient: Gradient {
            GradientStop { position: 0.0; color: Qt.lighter(Theme.rgba(MyPalette.highlightBackgroundColor, 1.0), 1.5) }
            GradientStop { position: 0.2; color: Theme.rgba(MyPalette.highlightBackgroundColor, 1.0) }
            GradientStop { position: 1.0; color: "transparent" }
          }
        }
      }
    }
  }
  Rectangle {
    z: -1
    anchors.centerIn: parent
    anchors.fill: parent
    //height: col.height
    //width: parent.width
    gradient: Gradient {
      GradientStop { position: -1.5; color: Qt.lighter(Theme.rgba(MyPalette.highlightDimmerColor, 1.0), 1.5) }
      GradientStop { position: 1.5; color: Theme.rgba(MyPalette.highlightBackgroundColor, 1.0 ) }
    }
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript

