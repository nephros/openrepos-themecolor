// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.Silica.Background 1.0
import Nemo.Thumbnailer 1.0
import ".."

SilicaItem {
    property alias fillMode: showbg.fillMode
    //property bool showbgRects: true
    width: parent.width

    ImageWallpaper {
        id: showbg
        anchors.top: parent.top
        width: parent.width
        height: parent.height
        anchors.horizontalCenter: parent.horizontalCenter

        //colorScheme: MyPalette.colorScheme
        //wallpaperFilter: MyPalette._wallpaperFilter

        smooth: false
        //cache: true
        //source: "file://" + imagepath.value
        //source: Theme._homeBackgroundImage
        //source: "image://nemoThumbnail/" + Theme._homeBackgroundImage
        //imageUrl: "image://nemoThumbnail/" + Theme._homeBackgroundImage
        imageUrl: Theme._homeBackgroundImage
        sourceSize.width: width
        sourceSize.height: height
        wallpaperFilter: "glass"
        fillMode: Image.PreserveAspectCrop

    }
    Rectangle {
        id: fillrect
        anchors.fill: showbg
        anchors.verticalCenter: showbg.verticalCenter
        color: MyPalette._wallpaperOverlayColor
        opacity: MyPalette.opacityLow
        radius: Theme.paddingLarge
    }
  /*
    Row {
        anchors.verticalCenter: showbg.verticalCenter
        anchors.horizontalCenter: showbg.horizontalCenter
        visible: showbgRects
        height: showbg.height
        property var elemwidth: showbg.width/5
        Rectangle {
            id: firstrec
            anchors.verticalCenter: parent.verticalCenter
            height: parent.height
            width: parent.elemwidth
            color: MyPalette.overlayBackgroundColor
            opacity: Theme.opacityHigh
            radius: Theme.paddingLarge
        }
        Rectangle {
            anchors.verticalCenter: firstrec.verticalCenter
            height: firstrec.height
            width: firstrec.width
            color: MyPalette.overlayBackgroundColor
            opacity: Theme.opacityLow
            radius: Theme.paddingLarge
        }
        Rectangle {
            anchors.verticalCenter: firstrec.verticalCenter
            height: firstrec.height
            width: firstrec.width
            color: MyPalette.overlayBackgroundColor
            opacity: Theme.opacityFaint
            radius: Theme.paddingLarge
        }
        Rectangle {
            anchors.verticalCenter: firstrec.verticalCenter
            height: firstrec.height
            width: firstrec.width
            color: MyPalette.highlightBackgroundColor
            opacity: Theme.highlightBackgroundOpacity
            radius: Theme.paddingLarge
        }
    }
    */
}

// vim: expandtab ts=4 softtabstop=4 filetype=javascript
