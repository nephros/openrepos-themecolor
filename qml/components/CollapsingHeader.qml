// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

BackgroundItem { id: root
  height: header.height + icon.height
  width: parent.width
  property Item target: null
  property alias text: header.text
  property color color: Theme.highlightColor
  property color collapsedColor: Theme.secondaryColor
  onClicked: if (target !== null) target.visible = !target.visible;
  Icon {
    id: icon
    anchors.verticalCenter: sep.verticalCenter
    anchors.right: parent.right
    height: Theme.iconSizeSmall
    width: Theme.iconSizeSmall
    source: "image://theme/icon-s-unfocused-down?" + (target.visible ? Theme.highlightColor : Theme.secondaryColor)
    rotation: target.visible ? 360 : 90
    Behavior on rotation { PropertyAnimation { } }
  }
  SectionHeader {
    id: header
    width: parent.width
    anchors.verticalCenter: parent.verticalCenter
    anchors.left: parent.left
    font.pixelSize: Theme.fontSizeLarge
    color: ( target.visible ) ? root.color : root.collapsedColor
    Behavior on color { ColorAnimation { } }
  }
  Separator {
    id: sep
    anchors.top: header.bottom
    width: parent.width - ( icon.width )
    height: 2
    horizontalAlignment: target.visible ? Qt.AlignRight : Qt.AlignHCenter
    color: Theme.highlightColor
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript
