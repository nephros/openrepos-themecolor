// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0

Dialog {
  id: dialog
  allowedOrientations: defaultAllowedOrientations
  property string coltext
  canAccept: field.acceptableInput
  width: parent.width
  Column {
    width: parent.width
    DialogHeader { title: qsTr("Color input") }
    Row {
      anchors.horizontalCenter: parent.horizontalCenter
      // formerly ColorIndicator2
      Icon {
        property color iconcolor: field.acceptableInput ? field.text : "transparent"
        sourceSize.height: Theme.iconSizeMedium;
        source: "image://theme/icon-m-ambience?" + iconcolor
        anchors.verticalCenter: field.verticalCenter
        opacity:  field.acceptableInput ? 1.0 : Theme.opacityLow
        Behavior on opacity { FadeAnimator {} }
      }
      ColorField {
        id: field
        width: Math.max(dialog.width - Theme.horizontalPageMargin * 4 ,  Theme.buttonWidthLarge)
        col: incolor;
        name: qsTr("Color input")
        text: outcolor
        EnterKey.enabled: acceptableInput
        EnterKey.onClicked: {
          focus = false;
          if (acceptableInput) dialog.accept();
        }
      }
    }
  }
  onDone: {
    if (result == DialogResult.Accepted) {
      var startsWith = /^#/;
      ( ! startsWith.test(field.text) ) ?  coltext = field.text.replace(/^/, "#") : coltext = field.text;
      console.debug("returned from dialog: " + field.text )
    }
  }
}
// vim: expandtab ts=4 st=4 filetype=javascript
