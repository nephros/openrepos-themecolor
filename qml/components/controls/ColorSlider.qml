// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0

Column { id: root
    property color incolor
    property color outcolor
    property color editcolor: Qt.rgba( colr.value, colg.value, colb.value, haveAlpha ? cola.value : 1 )
    property color _tmpcolor
    property bool haveAlpha: incolor.a < 1.0
    onIncolorChanged: resetOutcolor();
    onEditcolorChanged: outcolor = editcolor

    width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter
    spacing: 0

    Component.onCompleted: resetOutcolor();

    function resetOutcolor() {
        var tmp = incolor; //avoid binding loop warning
        outcolor = tmp;
        resetSliders(incolor);
    }
    function resetSliders(col) {
        _tmpcolor = col
        colr.value = col.r;
        colg.value = col.g;
        colb.value = col.b;
        if (col.a < 1) { cola.value = col.a }
    }
    function shufflePickerColors() {
        var pcols = [
            "#e60003", "#e6007c", "#e700cc", "#9d00e7",
            "#7b00e6", "#5d00e5", "#0077e7", "#01a9e7",
            "#00cce7", "#00e696", "#00e600", "#99e600",
            "#e3e601", "#e5bc00", "#e78601"];

        for(var i = 0; i < pcols.length; i++) {
            pcols[i] = Qt.tint(pcols[i], Theme.rgba( incolor,0.5));
        }
        return pcols;
    }

    Component { id: tiDialog
        ColorTextInputDialog {}
    }

    Row { id: colrow
        anchors.horizontalCenter: parent.horizontalCenter
        width: btnrow.width
        spacing: 0
        ColRect { width: parent.width / 2; height: Theme.fontSizeTiny; color: incolor }
        ColRect { width: parent.width / 2; color: root.editcolor }
    }
    Row { id: btnrow
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 0
        ValueButton { id: valbutton
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width * ( 2 / 4 )
            description: qsTr("Adjust sliders, tap to reset")
            label: haveAlpha ? "(a)RGB" : "RGB"
            value: editcolor
            valueColor: Theme.secondaryColor
            labelColor: Theme.secondaryColor
            onClicked: { resetOutcolor() }
            onValueChanged: { outcolor = value }
        }
        IconButton { id: tibutton
            anchors.verticalCenter: parent.verticalCenter
            height: valbutton.height
            width: parent.width * ( 1/ 4 )
            icon.source: "image://theme/icon-m-text-input?" + Theme.highlightColor
            onClicked: {
                var dialog = pageStack.push(tiDialog)
                dialog.accepted.connect(function() { resetSliders(dialog.coltext) })
            }
        }
        IconButton { id: pickbutton
            anchors.verticalCenter: parent.verticalCenter
            height: valbutton.height
            width: parent.width * ( 1 / 4 )
            icon.source: "image://theme/icon-m-wizard?" + Theme.highlightColor
            onClicked: {
                var dialog = pageStack.push("Sailfish.Silica.ColorPickerDialog", { colors: shufflePickerColors() })
                dialog.accepted.connect(function() { resetSliders(dialog.color) })
            }
        }
    }
    Slider { id: colr
        width: parent.width
        transform: Scale { yScale: 0.8 }
        handleVisible: true
        highlighted: false
        anchors.horizontalCenter: parent.horizontalCenter
        minimumValue: 0; maximumValue: 1.0; stepSize: 1/256
        color: Qt.rgba( value, 0, 0, 1.0)
    }
    Slider { id: colg
        width: parent.width
        transform: Scale { yScale: 0.8 }
        handleVisible: true
        highlighted: false
        anchors.horizontalCenter: parent.horizontalCenter
        minimumValue: 0 ; maximumValue: 1.0; stepSize: 1/256
        color: Qt.rgba( 0, value, 0, 1.0)
    }
    Slider { id: colb
        width: parent.width
        transform: Scale { yScale: 0.8 }
        handleVisible: true
        highlighted: false
        anchors.horizontalCenter: parent.horizontalCenter
        minimumValue: 0 ; maximumValue: 1.0; stepSize: 1/256
        color: Qt.rgba( 0, 0, value, 1.0)
    }
    Slider { id: cola
        visible: haveAlpha
        width: parent.width
        transform: Scale { yScale: 0.8 }
        handleVisible: true
        highlighted: false
        anchors.horizontalCenter: parent.horizontalCenter
        minimumValue: stepSize ; maximumValue: 1.0; stepSize: 1/256
        color: Qt.rgba( 0.5, 0.5, 0.5, value)
    }
    IconButton { id: alphabutton
        visible: !haveAlpha
        anchors.horizontalCenter: parent.horizontalCenter
        icon.source: "image://theme/icon-s-clear-opaque-background?" + "#33333333"
        height: Theme.itemSizeMedium
        width: Theme.itemSizeMedium
        Label { text: "𝛼"; font.pixelSize: Theme.fontSizeLarge; font.bold: true; anchors.centerIn: parent }
        onClicked: {
            haveAlpha = true
            cola.value = 181/256
        }
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
