// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import QtQml.Models 2.1
import Sailfish.Silica 1.0
import ".."

Column {
    width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter
    spacing: 0

    GlowIndicator {
        id: dot
        height: Theme.iconSizeExtraLarge
        width: height
        anchors.horizontalCenter: parent.horizontalCenter
    }
    Slider {
        id: glowSlider
        width: parent.width
        //width: parent.width - dot.width
        //anchors.verticalCenter: dot.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        minimumValue: 0.0
        maximumValue: 0.9999
        stepSize: 1/359
        value: MyPalette.highlightColor.hsvHueF ? MyPalette.highlightColor.hsvHueF : 0.5
        color: MyPalette.highlightColor.hsvHueF ? Theme.highlightFromColor(Qt.hsva(value, 1.0, 0.5, 0.0), MyPalette.colorScheme) : MyPalette.backgroundGlowColor 
        property real hue
        label: "Hue " + Math.round( value * 360 ) + "°"
        onValueChanged: {
          hue = value;
          glowSlider.enabled =  false;
          MyPalette.backgroundGlowColor = Theme.highlightFromColor(Qt.hsva(hue, 0.5, 0.5, 0.0), MyPalette.colorScheme);
          glowSlider.enabled =  true;
        }
    }
    Grid {
        anchors.horizontalCenter: parent.horizontalCenter
        rows: 2
        columnSpacing: Theme.paddingLarge
        rowSpacing: Theme.paddingLarge
        Component{ id: glass; GlassItem {
            height: Theme.iconSizeMedium
            width: Theme.iconSizeMedium
            radius: 0.22
            falloffRadius: 0.18
            backgroundColor: Theme.rgba(modelData, 1.0)
            BackgroundItem { anchors.fill: parent; anchors.centerIn: parent ; onClicked: {MyPalette.backgroundGlowColor = modelData }}
            Rectangle { anchors.fill: parent; anchors.centerIn: parent ; color: "transparent"; border.width: 1; border.color:  "gray" ; radius: 10 }
        }}
        // placeholder to pad to same amount the other repeater has:
        Item { height: Theme.iconSizeMedium; width: Theme.iconSizeMedium }
        Repeater {
            delegate: glass
            model: [ "white", "black", ]
        }
        // placeholder to pad to same amount the other repeater has:
        Item { height: Theme.iconSizeMedium; width: Theme.iconSizeMedium }
        Repeater {
            delegate: glass
            model: [
                MyPalette.primaryColor,
                MyPalette.secondaryColor,
                MyPalette.highlightColor,
                MyPalette.secondaryHighlightColor
            ]
        }
    }
}

// vim: expandtab ts=4 st=4 filetype=javascript
