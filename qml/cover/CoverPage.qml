// This file is part of ThemeColor.
// Copyright (c) 2021 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

/*

MIT License

Copyright (c) 2021-2024 Peter Gantner (nephros)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/


import QtQuick 2.6
import Sailfish.Silica 1.0

CoverBackground {
    id: coverPage

    property color col1: "#88ff0000"
    property color col2: "#8800ff00"
    property color col3: "#880000ff"
    property color colt: Theme.highlightColor

    function rndColors() {
      col1 = Qt.rgba((Math.random() / 2 + 0.5), Math.random(), Math.random(), col1.a);
      col2 = Qt.rgba(Math.random(), (Math.random() / 2 + 0.5), Math.random(), col2.a);
      col3 = Qt.rgba(Math.random(), Math.random(), (Math.random() / 2 + 0.5), col3.a);
      colt = Qt.rgba(Math.random(), Math.random(), Math.random(), colt.a);
    }

    onStatusChanged: rndColors()

    Label {
      id: titletext
      //text: qsTr("ThemeColor")
      text: Qt.application.name
      color: Theme.highlightFromColor(colt, Theme.colorScheme)
      anchors {
        horizontalCenter: parent.horizontalCenter
        //leftMargin: Theme.horizontalPageMargin
        //rightMargin: Theme.horizontalPageMargin
      }
      horizontalAlignment : Text.alignHCenter
      anchors.bottom: logo.top
      anchors.topMargin: Theme.paddingLarge
      anchors.bottomMargin: Theme.paddingLarge * 3
    }
    Row {
      id: logo
        anchors.centerIn: parent
        Icon { source: "image://theme/icon-m-ambience?" + col1 }
        Icon { source: "image://theme/icon-m-ambience?" + col2 }
        Icon { source: "image://theme/icon-m-ambience?" + col3 }
    }
    Label {
      text: "v" + Qt.application.version
      color: titletext.color
      font.pixelSize: Theme.fontSizeSmall
      anchors {
        horizontalCenter: parent.horizontalCenter
      }
      horizontalAlignment : Text.alignHCenter
      anchors.top: logo.bottom
      anchors.topMargin: Theme.paddingLarge * 3
      anchors.bottomMargin: Theme.paddingLarge
    }
    Image {
        source: "./background.png"
        z: -1
        anchors {
            bottom: parent.bottom
            horizontalCenter: parent.horizontalCenter
        }
        sourceSize.width: parent.width
        fillMode: Image.PreserveAspectFit
        opacity: 0.2
        smooth: false
        scale: ( parent.status === Cover.Active ) ? 1.0 : 0.2
        Behavior on scale { NumberAnimation { duration: 1000; easing.type: Easing.OutElastic } }
    }
}

// vim: expandtab ts=4 st=4 filetype=javascript
