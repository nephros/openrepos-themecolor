// This file is part of ThemeColor.
// Copyright (c) 2021 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.DBus 2.0
import "cover"
import "components"

ApplicationWindow {
    id: app

    property bool colorsInitialized: false
    property bool arewesandboxed;

    allowedOrientations: defaultAllowedOrientations

    Component.onCompleted: {
        console.info("Intialized", AppInfo.displayName, "version", AppInfo.versionstring,"launched as", AppInfo.appname, "by", AppInfo.organame );
        initColors()
    }

    /*
     * generic interface to SystemD, so we can restart units
     */
    DBusInterface {
        id: systemdbus
        bus: DBus.SessionBus
        service: "org.freedesktop.systemd1"
        path: "/org/freedesktop/systemd1"
        iface: "org.freedesktop.systemd1.Manager"
        function restartAmbienced() {
            call('RestartUnit', [ 'ambienced.service', 'replace' ],
                function (result) {         console.debug('DBus call result: ' + result) },
                function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
            )
        }
        function restartLipstick() {
            call('RestartUnit', [ 'lipstick.service', 'replace' ],
                function (result) {         console.debug('DBus call result: ' + result) },
                function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
            )
        }
        function runBuilder() {
            //call('RestartUnit', [ 'openrepos-themecolor-makerpm.service', 'replace' ],
            call('RestartUnit', [ 'org.nephros.sailfish.ThemeColor.makerpm.service', 'replace' ],
                function (result) {         console.debug('DBus call result: ' + result) },
                function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
            )
        }
    }
    /*
     * DBus connection to ambienced, to save settings to ambience
     */
    DBusInterface {
        id: ambiencedbus
        bus: DBus.SessionBus
        service: "com.jolla.ambienced"
        path: "/com/jolla/ambienced"
        iface: "com.jolla.ambienced"
        signalsEnabled: true
        /*
         * from  http://www.jollausers.com/2013/12/how-to-make-ambiance-wallpapers-for-sailfish-bonus/
         *   method void com.jolla.ambienced.createAmbience(QString url)
         *      where url is a real one or a file:/// to an image
         *   method void com.jolla.ambienced.saveAttributes(int contentType, qlonglong contentId, QVariantMap args)
         */
        function saveAmbience() {
            call('saveAttributes', [ ],
                function (result) {         console.debug('DBus call result: ' + result) },
                function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
            )
        }
        function makeAmbience() {
            call('createAmbience', [ ],
                function (result) {         console.debug('DBus call result: ' + result) },
                function (error, message) { console.warn('DBus call failed: ', error, 'message: ', message) }
            );
        }
        // signal handler to detect ambience change
        function contentChanged() {
            console.debug("ambienced signalled");
            colorsInitialized = false; initColors();
        }
    }

      /*
       * the main theme saving interface
       */
    ConfigurationGroup {
        id: conf
        path: "/desktop/jolla/theme/color"
        synchronous: false
        property color primary
        property color secondary
        property color highlight
        property color secondaryHighlight
        property color highlightBackground
        property color highlightDimmer
        property color backgroundGlow
        property double opacityFaint
        property double opacityLow
        property double opacityHigh
        property double opacityOverlay
    }

    // dump all Theme colors:
    function printColors(message) {
        console.info( message +
            "    primaryColor: "             + MyPalette.primaryColor + "\n" +
            "    secondaryColor: "           + MyPalette.secondaryColor + "\n" +
            "    highlightColor: "           + MyPalette.highlightColor + "\n" +
            "    secondaryHighlightColor: "  + MyPalette.secondaryHighlightColor + "\n" +
            "    highlightBackgroundColor: " + MyPalette.highlightBackgroundColor + "\n" +
            "    highlightDimmerColor: "     + MyPalette.highlightDimmerColor + "\n" +
            "    overlayBackgroundColor: "   + MyPalette.overlayBackgroundColor + "\n" +
            "    backgroundGlowColor: "      + MyPalette.backgroundGlowColor + "\n" +
            "    _wallpaperOverlayColor: "   + MyPalette._wallpaperOverlayColor + "\n" +
            "    _coverOverlayColor: "       + MyPalette._coverOverlayColor + "\n"
        );
        console.debug( "MyPalette object:", JSON.stringify(MyPalette, null, 4));
    }
    function initColors() {
        console.debug("init colors")
        if ( !colorsInitialized ) {
            MyPalette.primaryColor            = conf.primary;
            MyPalette.secondaryColor          = conf.secondary;
            MyPalette.highlightColor          = conf.highlight;
            MyPalette.secondaryHighlightColor = conf.secondaryHighlight;
            // these will not exist at first run/load:
            if ( typeof(MyPalette.colorScheme) === "undefined" ) { MyPalette.colorScheme = Theme.colorScheme }
            //( typeof(conf.highlightBackground) === "undefined" ) ? MyPalette.highlightBackgroundColor = Theme.highlightBackgroundColor : MyPalette.highlightBackgroundColor = conf.highlightBackground;
            //( typeof(conf.highlightDimmer )    === "undefined" ) ? MyPalette.highlightDimmerColor = Theme.highlightDimmerColor         : MyPalette.highlightDimmerColor = conf.highlightDimmer;
            ( typeof(conf.secondaryHighlight)  === "undefined" ) ? MyPalette.secondaryHighlightColor  = Theme.secondaryHighlightFromColor(MyPalette.highlightColor, MyPalette.colorScheme)         : MyPalette.secondaryHighlightColor = conf.secondaryHighlight;
            ( typeof(conf.highlightBackground) === "undefined" ) ? MyPalette.highlightBackgroundColor = Theme.highlightBackgroundFromColor(MyPalette.highlightColor, MyPalette.colorScheme)        : MyPalette.highlightBackgroundColor = conf.highlightBackground;
            ( typeof(conf.highlightDimmer )    === "undefined" ) ? MyPalette.highlightDimmerColor     = Theme.highlightDimmerFromColor(MyPalette.highlightBackgroundColor, MyPalette.colorScheme)  : MyPalette.highlightDimmerColor = conf.highlightDimmer;
            ( typeof(conf.backgroundGlow)      === "undefined" ) ? MyPalette.backgroundGlowColor      = Theme.backgroundGlowColor                                                                  : MyPalette.backgroundGlowColor = conf.backgroundGlow;

            printColors("Initialized Ambience/System Theme colors:\n");
            colorsInitialized = true;
            MyPalette.paletteChanged();
        }
    }

    function reloadThemeColors() {
        MyPalette.primaryColor                = Theme.primaryColor;
        MyPalette.secondaryColor              = Theme.secondaryColor;
        MyPalette.highlightColor              = Theme.highlightColor;
        MyPalette.secondaryHighlightColor     = Theme.secondaryHighlightColor;
        MyPalette.highlightBackgroundColor    = Theme.highlightBackgroundColor;
        MyPalette.highlightDimmerColor        = Theme.highlightDimmerColor;
        MyPalette.backgroundGlowColor         = Theme.backgroundGlowColor
        printColors("Reloaded colors:\n");
    }
    function applyThemeColors() {
        printColors("Applying colors:\n");
        conf.primary              = MyPalette.primaryColor;
        conf.secondary            = MyPalette.secondaryColor;
        conf.highlight            = MyPalette.highlightColor;
        conf.secondaryHighlight   = MyPalette.secondaryHighlightColor;
        conf.highlightBackground  = MyPalette.highlightBackgroundColor;
        conf.highlightDimmer      = MyPalette.highlightDimmerColor;
        conf.backgroundGlow       = MyPalette.backgroundGlowColor;
        //conf.opacityFaint         = MyPalette.opacityFaint;
        //conf.opacityLow           = MyPalette.opacityLow;
        //conf.opacityHigh          = MyPalette.opacityHigh;
        //conf.opacityOverlay       = MyPalette.opacityOverlay;
        conf.sync();
        // these are read-only
        //Theme.opacityFaint        = MyPalette.opacityFaint;
        //Theme.opacityLow          = MyPalette.opacityLow;
        //Theme.opacityHigh         = MyPalette.opacityHigh;
        //Theme.opacityOverlay      = MyPalette.opacityOverlay;
    }
    function restartAmbienced() { systemdbus.restartAmbienced() }
    function restartLipstick() { systemdbus.restartLipstick() }
    function resetDconf() {
        conf.clear();
        conf.primary            = (Theme.colorScheme == Theme.LightOnDark) ? "#FFFFFFFF" : "#FF000000";
        conf.secondary          = (Theme.colorScheme == Theme.LightOnDark) ? "#B0FFFFFF" : "#B0000000";
        conf.highlight          = Theme.highlightColor;
        conf.secondaryHighlight = Theme.secondaryHighlightColor;
        conf.sync();
    }

    initialPage: Qt.resolvedUrl( "pages/FirstPage.qml")
    cover: Component { CoverPage {} }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
