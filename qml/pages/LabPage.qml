// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Dialog { id: labPage

    objectName: "lab" // so we can use pageStack.find()

    onStatusChanged: {
        if ( status === PageStatus.Active && pageStack.nextPage() === null ) { pageStack.pushAttached(Qt.resolvedUrl("Saver.qml")) }
    }

    allowedOrientations: defaultAllowedOrientations

    // map dropdown selection to ints
    //TODO: is there a more native type for this?
    readonly property var inputMode: QtObject {
        property int sliders: 0
        property int swapper: 1
        property int generators: 2
        property int jolla: 3
        property int defaultValue: this.sliders
    }

    SilicaFlickable { id: flick
        anchors.fill: parent
        contentHeight: col.height
        Column { id: col
            // would be nice, but will also affect the header:
            // so, set it for each of the components below:
            //width: isLandscape ? parent.width * 4/5 : parent.width
            //anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            DialogHeader { id: head ; title: qsTr("Atelier"); cancelText: qsTr("Preview"); acceptText: qsTr("Cupboards") }
            ShowRoomMini {
                width: (isLandscape ? parent.width * 4/5 : parent.width) - Theme.horizontalPageMargin
                anchors.horizontalCenter: parent.horizontalCenter
                theme: MyPalette
                title: qsTr("Current Palette")
            }
            ComboBox { id: modeSelector
                width: (isLandscape ? parent.width * 4/5 : parent.width) - Theme.horizontalPageMargin
                anchors.horizontalCenter: parent.horizontalCenter
                label: qsTr("Editor", "tool/editing mode") + ":"
                description: qsTr("Tap to switch")
                currentIndex: inputMode.defaultValue
                menu: ContextMenu {
                    // TODO: anchoring throws Type errors, but does work...
                    // we need these two because the menu otherwise gets full-width and centered in landscape view.
                    anchors.horizontalCenter: ( parent !== null ) ? parent.horizontalCenter : undefined
                    container: modeSelector; width: container.width

                    MenuItem { text: qsTr("Sliders") }
                    MenuItem { text: qsTr("Swapper/Copier") }
                    MenuItem { text: qsTr("Generators") }
                    MenuItem { text: qsTr("Jolla Original") }
                }
            }

            Column { id: editcol
                width: (isLandscape ? parent.width * 4/5 : parent.width) - Theme.horizontalPageMargin
                anchors.horizontalCenter: parent.horizontalCenter
                visible: !app.collapsed
                Label { id: editHint
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.margins: Theme.paddingMedium
                    font.pixelSize: Theme.fontSizeSmall
                    wrapMode: Text.Wrap
                    horizontalAlignment: Qt.AlignHCenter
                    color: Theme.highlightColor
                    text: {
                        switch (modeSelector.currentIndex) {
                            case inputMode.sliders:
                                return qsTr("Use the sliders to edit the colors one by one.");
                                break;
                            case inputMode.swapper:
                                return qsTr("Exchange two colors, or copy one to the other.");
                                break;
                            case inputMode.generators:
                                return qsTr("Tap a button to change the whole Palette.");
                                break;
                            case inputMode.jolla:
                                return qsTr("Slide to select a hue.");
                                break;
                        }
                    }
                }
                ColorSliders {
                    visible: modeSelector.currentIndex === inputMode.sliders
                }
                ColorSwapper {
                    visible: modeSelector.currentIndex === inputMode.swapper
                }
                ColorSwapper {
                    visible: modeSelector.currentIndex === inputMode.swapper
                    copy: true
                }
                ColorGenerators {
                    visible: modeSelector.currentIndex === inputMode.generators
                    GeneratorRemorse { id: generatorRemorse }
                }
                CollaSlider {
                    visible: modeSelector.currentIndex === inputMode.jolla
                }
            }
        }
        MainMenu{}
        RemorsePopup { id: applyRemorse }
        VerticalScrollDecorator {}
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
