// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../../components"

Page {
    allowedOrientations: defaultAllowedOrientations
    SilicaFlickable {
        id: flick
        anchors.fill: parent
        contentHeight: col.height + head.height
        PageHeader { id: head ; title: qsTr("About") }
        Column { id: col
            width: (isLandscape) ? ( parent.width * 2/3)  : parent.width - Theme.horizontalPageMargin
            spacing: (isLandscape) ? Theme.paddingMedium : Theme.paddingLarge
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: head.bottom

            DetailItem { label: qsTr("Version:");      value: AppInfo.versionstring }
            DetailItem { label: qsTr("Copyright:");    value: AppInfo.copyright;                                   BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally('mailto:' + AppInfo.email + '?' + "subject=A%20message%20from%20a%20" + Qt.application.name + "%20user&body=Hello%20nephros%2C%0A") } }
            DetailItem { label: qsTr("License:");      value: AppInfo.license + " (" + AppInfo.licenseurl + ")";   BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(AppInfo.licenseurl) } }
            DetailItem { label: qsTr("Source Code:");  value: AppInfo.source;                                      BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(AppInfo.source) } }
            SectionHeader {text: qsTr("Translations") }
            Repeater {
                model: AppInfo.translators
                // TODO: how to get localized name of langid:
                //DetailItem { label: Qt.locale(langid).name + "/" + Qt.locale(langid).nativeLanguageName ;  value: persons;                          BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(AppInfo.transurl) } }
                DetailItem { label: langname + "/" + Qt.locale(langid).nativeLanguageName ;  value: persons;                          BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(AppInfo.transurl) } }

            }
        }
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
