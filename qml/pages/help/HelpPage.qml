// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.WebView 1.0
import Sailfish.WebEngine 1.0
//import "../components"
//import "help"

WebViewPage {
    id: helppage

    property string lang: "en"
    property string langname: ""
    property bool usel10n: true
    property string docRoot: "file:///usr/share/openrepos-themecolor/doc"
    property string currentPage
    property string currentUrl: usel10n ? docRoot + "/" + lang + "/" + currentPage : docRoot + "/" + "en" + "/" + currentPage;

    onUsel10nChanged: webview.load(currentUrl)

    allowedOrientations: defaultAllowedOrientations

    Component.onCompleted: {
        WebEngineSettings.autoLoadImages = true;
        WebEngineSettings.downloadsEnabled = false;
        WebEngineSettings.javascriptEnabled = false;
        WebEngineSettings.canShowSelectionMarkers = false;
        //WebEngineSettings.pixelRatio = 3;

        WebEngineSettings.setPreference("browser.cache.disk.enable",     false, WebEngineSettings.BoolPref);
        WebEngineSettings.setPreference("browser.cache.memory.enable",   true,  WebEngineSettings.BoolPref);
        WebEngineSettings.setPreference("browser.cache.memory.capacity", 4096,  WebEngineSettings.IntPref);

        lang = getL10NLanguage();
        langname = Qt.locale().nativeLanguageName;
        console.debug("L10N language determined to be:", lang);
    }

    function getL10NLanguage() {
        var llang = Qt.locale().name;
        var slang = llang.substr(0,2);
        console.debug("L10N languages:", llang, " ", slang);
        // supported = [ de, en, es, fr, ru, sv, nb_NO, zh_CN, en_GB,  ];
        if ( (slang == "de") || (slang == "es") || ( slang == "fr" ) || ( slang == "ru") || ( slang == "sv" ) ) {
            return slang;
        } else if ( (llang == "en_GB") || ( llang == "nb_NO" ) || ( llang == "zh_ZN" ) ) {
            return llang;
        }
        // else
        return "en";
    }

    SilicaFlickable {
        id: flick
        anchors.fill: parent
        anchors.horizontalCenter: parent.horizontalCenter
        contentHeight: head.height + webview.height
        PageHeader {
            id: head
            //: User Manual, Application Manual, Handbook, Howto
            title: qsTr("Handbook", "Help Index")
            property alias currentIndex: box.currentIndex
            ComboBox { id: box
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.extraContent.horizontalCenter
                width: parent.extraContent.width
                label: currentIndex >= 0 ? qsTr("Chapter %1:", "Help Index Chapter Prefix").arg(currentIndex + 1) : qsTr("Select a chapter")
                description: currentIndex >= 0 ? qsTr("Tap to switch") : qsTr("Tap to select")
                menu: helpIndex
                currentIndex: -1
                onCurrentIndexChanged: currentPage = currentItem.path
            }
            ContextMenu {
                id: helpIndex
                MenuItem{ text: qsTr("General Information"       , "Help Index") ; property string path: "HelpGeneral.html" }
                MenuItem{ text: qsTr("About the UI"              , "Help Index") ; property string path: "HelpUIMain.html" }
                MenuItem{ text: qsTr("About Colors"              , "Help Index") ; property string path: "HelpColors.html" }
                MenuItem{ text: qsTr("Exporting Ambiences"       , "Help Index") ; property string path: "HelpExport.html" }
                MenuItem{ text: qsTr("Watcher Daemon"            , "Help Index") ; property string path: "HelpDaemon.html" }
                MenuItem{ text: qsTr("How it works"              , "Help Index") ; property string path: "HelpHow.html" }
                MenuItem{ text: qsTr("Limitations , Tips etc."   , "Help Index") ; property string path: "HelpTips.html" }
            }
        }

        WebView {
            id: webview
            visible: head.currentIndex >= 0
            url: currentUrl
            privateMode: true
            //width:  viewportWidth
            //height: viewportHeight
            //viewportWidth: parent.width - Theme.horizontalPageMargin
            //viewportHeight: Screen.height - head.height - Theme.paddingMedium
            width: parent.width - Theme.horizontalPageMargin
            height: Screen.height - head.height - Theme.paddingMedium
            anchors {
                top: head.bottom
                horizontalCenter: parent.horizontalCenter
            }
        }

        ViewPlaceholder {
            anchors.fill: webview
            enabled: head.currentIndex < 0
            text: qsTr("No Chapter selected")
            hintText: qsTr("Tap the header to select a chapter")
        }

        PullDownMenu {
            MenuItem { text: qsTr("Credits"); onClicked: pageStack.replace(Qt.resolvedUrl("HelpCredits.qml"))  }
            MenuItem { text: qsTr("View %1", "User Manual display language").arg(usel10n ? qsTr("English") : langname );
                onClicked: usel10n = !usel10n
            }
        }
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
