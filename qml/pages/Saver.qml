// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import "../components"
import "../components/saver"
import "savefunctions.js" as Saver

Dialog { id: saver

        allowedOrientations: defaultAllowedOrientations

        property alias slots: store.slots
        property alias lootboxtapped: store.lootboxtapped

        ConfigurationGroup { id: store
                path: "/org/nephros/openrepos-themecolor/storage"
                property int slots: 4
                property int lootboxtapped: 0
        }

        function loadTheme(i) {
                MyPalette.primaryColor             = store.value("store" + i + "_p")
                MyPalette.secondaryColor           = store.value("store" + i + "_s")
                MyPalette.highlightColor           = store.value("store" + i + "_h")
                MyPalette.secondaryHighlightColor  = store.value("store" + i + "_sh")
                MyPalette.highlightBackgroundColor = store.value("store" + i + "_hbg")
                MyPalette.backgroundGlowColor      = store.value("store" + i + "_bgg")
                MyPalette.highlightDimmerColor     = store.value("store" + i + "_hdc")
                //pageStack.navigateBack()
                var gotoPage = pageStack.find(function(page) {
                    return page.objectName === "lab"
                })
                pageStack.pop(gotoPage)
        }
        function saveTheme(i) {
                store.setValue("store" + i + "_p",    MyPalette.primaryColor);
                store.setValue("store" + i + "_s",    MyPalette.secondaryColor);
                store.setValue("store" + i + "_h",    MyPalette.highlightColor);
                store.setValue("store" + i + "_sh",   MyPalette.secondaryHighlightColor);
                store.setValue("store" + i + "_hbg",  MyPalette.highlightBackgroundColor);
                store.setValue("store" + i + "_bgg",  MyPalette.backgroundGlowColor);
                store.setValue("store" + i + "_hdc",  MyPalette.highlightDimmerColor);
                store.sync();
        }
        function setThemeProp(i, prop, val) {
                if ( typeof(val) === "undefined" ) {
                        //console.warn("trying to save uninitialized value, saving gray instead");
                        //store.setValue("store" + i + "_" + prop, "'dimgray'");
                        //console.warn("trying to save uninitialized value");
                        return;
                }
                store.setValue("store" + i + "_" + prop, val);
        }
        function getThemeProp(i, prop) {
                var c =  store.value("store" + i + "_" + prop);
                if ( typeof(c) === "undefined") {
                        //console.debug("returning gray instead of uninitialized value for " + prop);
                        return "dimgray";
                } else if (c === "dimgray") {
                        //console.debug("correcting saved dimgray value for " + prop);
                        store.setValue("store" + i + "_" + prop, '');
                        return "dimgray";
                } else {
                        return c;
                }
        }
        Component.onCompleted: {
                if ( lootboxtapped >= 3 ) {
                        slots += 2;
                        lootboxtapped = 0;
                }
        }

        onStatusChanged: {
                // TODO: don't do this every time it becomes visible...
                // SaverPlus depends on being Attached! so it can pop()
                if ( status === PageStatus.Active  && pageStack.nextPage() === null ) { 
                        pageStack.pushAttached( Qt.resolvedUrl("SaverPlus.qml")) ;
                }
        }

        SilicaFlickable { id: flick
                anchors.fill: parent
                contentHeight: view.height + Theme.paddingLarge
                VerticalScrollDecorator {}
                SilicaListView { id: view
                        //anchors.fill: parent
                        //anchors.centerIn: parent
                        //anchors.top: head.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: parent.width
                        height: flick.height //- (head.height + sep.height )
                        clip: true
                        currentIndex: -1
                        model: slots
                        header: Item {
                                width: parent.width
                                height: head.height +  sep.height
                                DialogHeader { id: head ; width: parent.width; title: qsTr("Global Cupboard")
                                        cancelText: qsTr("Atelier"); acceptText: qsTr("Ambience");
                                }
                                Separator { id: sep
                                        anchors.horizontalCenter: parent.horizontalCenter
                                        anchors.bottom: parent.bottom
                                        width: head.width
                                        color: Theme.secondaryColor
                                }
                        }
                        delegate: Loader {
                                property int modelIndex: index
                                property double viewWidth: ListView.view.width
                                sourceComponent: Component {
                                        SaveSlot {
                                                idx: modelIndex
                                                width: viewWidth
                                        }
                                }
                        }
                        footer: LootBoxItem { id: loot
                                width: ListView.view.width
                                numtapped: lootboxtapped
                                onNumtappedChanged: { lootboxtapped = numtapped; }
                        }
                        pullDownMenu: pull
                }
                RemorsePopup { id: remorse }
                PullDownMenu { id: pull
                        MenuItem { text: qsTr("Clean out this cupboard"); onClicked: remorse.execute( ( qsTr("Spring Clean") + "…"),
                                function() {
                                        var tmp = slots;
                                        store.clear();
                                        store.setValue("slots", tmp);
                                        pageStack.navigateBack();
                                } ,6000) }
                }
        }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
