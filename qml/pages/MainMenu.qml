// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0

PullDownMenu {
    MenuItem { text: qsTr("Handbook", "User Manual, Application Manual, Handbook, Howto");
        onClicked: { pageStack.push(Qt.resolvedUrl("help/HelpPage.qml")) }
    }
    MenuItem { text: qsTr("Advanced…");
        onClicked: { pageStack.push(Qt.resolvedUrl("AdvancedPage.qml"), { isAdvanced: true }) }
    }
    MenuItem { text: qsTr("Export to Ambience package");
        onClicked: { pageStack.push(Qt.resolvedUrl("advanced/SaveAmbience.qml")) }
    }
    MenuItem { text: qsTr("Apply Colors to System");
        onClicked: { applyRemorse.execute(qsTr("Applying") + "…", function () { applyThemeColors() } ) }
    }
    MenuItem { text: qsTr("Reload Colors from System");
        onClicked: {
            reloadThemeColors();
            colorsInitialized = false;
            initColors()
        }
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
