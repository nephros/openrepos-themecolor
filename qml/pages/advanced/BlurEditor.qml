// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import "../../components"

Dialog {
  id: blurpage

  allowedOrientations: defaultAllowedOrientations

  onOpened: {
      newIterations = iterations.value
      newDeviation  = deviation.value
      newKernel = kernel.value
  }
  //onDone: {
  //}
  onAccepted: {
      iterations.value = newIterations
      deviation.value  = newDeviation
      kernel.value     = newKernel
  }
  onRejected: {
      MyPalette.secondaryColor            = Theme.rgba(MyPalette.secondaryHighlightColor, MyPalette.s_alpha)
      MyPalette.secondaryHighlightColor   = Theme.rgba(MyPalette.secondaryHighlightColor, MyPalette.shl_alpha)
      //printValues("Colors on Cancel: ") 
  }
  /*
   "SampleSize5": 0,
   "SampleSize9": 1,
   "SampleSize13": 2,
   "SampleSize17": 3,
   "SampleSize21": 4,
   "SampleSize25": 5,
   "SampleSize29": 6,
   "SampleSize33": 7
  */
  property int newIterations
  property double newDeviation
  property int newKernel
  ConfigurationValue{ id: iterations; key: "/desktop/lipstick-jolla-home/blur_iterations" ; defaultValue: 2 }
  ConfigurationValue{ id: deviation;  key: "/desktop/lipstick-jolla-home/blur_deviation";   defaultValue: 5 }
  ConfigurationValue{ id: kernel;     key: "/desktop/lipstick-jolla-home/blur_kernel";      defaultValue: 3 }
  SilicaFlickable {
    id: flick
    anchors.fill: parent
    contentHeight: col.height + head.height
    VerticalScrollDecorator {}
    DialogHeader { id: head ; title: qsTr("Edit Blur Parameters") }
    Column {
      id: col
      width: parent.width - Theme.horizontalPageMargin
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.top: head.bottom
      spacing: Theme.paddingLarge
        Label {
            width: parent.width
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("These are some values found in Lipstick. They are exposed here for your experimentation, although it is unclear where exactly they are applied, and changing them seems to have very little effect. They are also not part of Ambiences.")
            wrapMode: Text.Wrap
            font.pixelSize: Theme.fontSizeSmall
        }
        SectionHeader { text: qsTr("Blur Iterations") }
        Slider {
              width: parent.width
              value: blurpage.newIterations
              minimumValue: 0
              maximumValue: 5
              valueText: Math.floor(sliderValue)
              stepSize: 1
              onDownChanged: if (!down) blurpage.newIterations = Math.floor(sliderValue)
      }
      SectionHeader { text: qsTr("Blur Deviation") }
      Slider {
              width: parent.width
              value: blurpage.newDeviation
              minimumValue: 0
              maximumValue: 500
              valueText: Math.floor(sliderValue)
              stepSize: 1
              onDownChanged: if (!down) blurpage.newDeviation = Math.floor(sliderValue)
      }
      SectionHeader { text: qsTr("Blur Kernel") }
      Slider {
              width: parent.width
              value: blurpage.newKernel
              minimumValue: 0
              maximumValue: samples.length
              stepSize: 1
              valueText: samples[Math.floor(sliderValue)]
              onDownChanged: if (!down) blurpage.newKernel = Math.floor(sliderValue)
              property var samples: [ 
                      "SampleSize5",
                      "SampleSize9",
                      "SampleSize13",
                      "SampleSize17",
                      "SampleSize21",
                      "SampleSize25",
                      "SampleSize29",
                      "SampleSize33"
              ]
      }
     }
  }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
