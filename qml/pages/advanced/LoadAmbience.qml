// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.Pickers 1.0
import "../../components"

Dialog {
  id: dialogpage

  canAccept: false

  allowedOrientations: defaultAllowedOrientations

  readonly property string defaultPath: StandardPaths.documents
  property string selectedFile: ""
  property string selectedName: ""
  property string selectedUrl: ""
  property var fileData
  property alias myprimaryColor                 : filePalette.primaryColor
  property alias mysecondaryColor               : filePalette.secondaryColor
  property alias myhighlightColor               : filePalette.highlightColor
  property alias mysecondaryHighlightColor      : filePalette.secondaryHighlightColor
  property alias myhighlightBackgroundColor     : filePalette.highlightBackgroundColor
  property alias myhighlightDimmerColor         : filePalette.highlightDimmerColor
  property alias mycoverOverlayColor            : filePalette.coverOverlayColor

  MyPaletteProto {
    id: filePalette
  }

  states: [
    State { name: "loaded" },
    State { name: "selected" ; PropertyChanges { target: button; text: qsTr("Load") } },
    State { name: "failed" }
  ]

  function handleData(j) {
    fileData = JSON.stringify(j, null, 2);
    console.debug("request done. got data: " + fileData);
    myprimaryColor = j.primaryColor;
    mysecondaryColor = j.secondaryColor;
    myhighlightColor = j.highlightColor;
    mysecondaryHighlightColor = j.secondaryHighlightColor;
    myhighlightBackgroundColor = j.highlightBackgroundColor;
    myhighlightDimmerColor = j.highlightDimmerColor;
    //mycoverOverlayColor = j._coverOverlayColor;
    srm.theme = filePalette;
    srm.opacity = 1.0;
    dialogpage.state = "loaded";
    dialogpage.canAccept = true;
  }
  function getData() {
    var fileUrl = dialogpage.selectedUrl;

    var r = new XMLHttpRequest()
    r.open('GET', fileUrl);
    r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    r.send();

    r.onreadystatechange = function(event) {
      if (r.readyState == XMLHttpRequest.DONE) {
        var j = JSON.parse(r.response);
        handleData(j);
      }
    }
  }
  onDone: {
    if (result == DialogResult.Accepted) {
      for (var p in MyPalette)
        MyPalette[p] = filePalette[p];
    }
  }

  Component {
    id: filePickerpage
    FilePickerPage {
      nameFilters: [ '*.json', '*.ambience', '*.js', '*.json.txt' ]
      //title: "Select document"
      //showSystemFiles: false
      onSelectedContentPropertiesChanged: {
        dialogpage.selectedFile = selectedContentProperties.filePath;
        dialogpage.selectedName = selectedContentProperties.fileName;
        dialogpage.selectedUrl = selectedContentProperties.url;
        dialogpage.state = "loaded"
        dialogpage.getData();
      }
    }
  }
  SilicaFlickable {
    id: flick
    contentHeight: col.height + header.height
    anchors.fill: parent

    DialogHeader {id: header ; width: dialogpage.width; title: qsTr("Load Theme"); acceptText: qsTr("Take to Atelier")}
    Column {
      id: col
      width: parent.width - Theme.horizontalPageMargin
      spacing: Theme.paddingSmall
      anchors.top: header.bottom
      anchors.horizontalCenter: parent.horizontalCenter
      //dialogpageHeader { title: qsTr("Loading Functions") }
      Label {
        width: parent.width
        anchors {
          horizontalCenter: parent.horizontalCenter
          leftMargin: Theme.horizontalPageMargin
          rightMargin: Theme.horizontalPageMargin
        }
        font.pixelSize: Theme.fontSizeSmall
        horizontalAlignment: Text.AlignJustify
        wrapMode: Text.WordWrap
        text: qsTr("Here you can load a .ambience (JSON) file.")
      }
      ShowRoomMini {
        theme: MyPalette
        title: qsTr("current")
      }
      ShowRoomMini {
        id: srm
        opacity: 0.0
        theme: dialogpage.palette
        title: qsTr("file data")
      }
      ValueButton {
        id: button
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        label: qsTr("File name ")
        value: dialogpage.selectedName
        description: qsTr("Click to select")

        onClicked: {
          if ( dialogpage.state != "selected" ) {
            pageStack.push(filePickerpage);
          }
        }
      }      SectionHeader { text: qsTr("File content") }
      TextArea {
        width: parent.width - Theme.paddingLarge * 2
        height: implicitHeight > 0 ? Math.min(implicitHeight, Theme.itemSizeLarge * 6) : Theme.itemSizeLarge * 2
        backgroundStyle: TextEditor.FilledBackground
        background: Component { Rectangle { color: Theme.highlightBackgroundFromColor(Theme.highlightBackgroundColor, Theme.colorScheme); anchors.fill: parent} }
        color: Theme.secondaryColor
        //description: dialogpage.selectedName
        font.pixelSize: Theme.fontSizeSmall
        placeholderText: qsTr("No file loaded")
        readOnly: true
        text: (dialogpage.state == "loaded") ? dialogpage.fileData : ""
        verticalAlignment: TextEdit.AlignVCenter
      }
    }
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript
