// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.DBus 2.0
import "../../components" // should load singleton

Page {
  id: page
  allowedOrientations: defaultAllowedOrientations

  property alias serviceState: systemd.serviceState

  Timer {
    id: checkTimer
    interval: 750
    repeat: true
    onTriggered: {
      systemd.checkService();
      if (typeof(serviceState) !== "undefined") { checkTimer.stop() };
      interval = 1200
    }
  }

  Component.onCompleted: {
    systemd.checkService();
    checkTimer.start();
  }

  ConfigurationGroup {
    id: conf
    path: "/org/nephros/openrepos-themecolor/daemon"
    // use all lowercase and no underscore values, because that's what GConf allows which we use from python:
    property bool autoopen: false
    property bool autoapply: false
    property bool autoapplynight: false
    property int nighthour: 19
    property int nightminute: 25
    property int dayhour: 7
    property int dayminute: 17
  }

  DBusInterface {
    id: systemd

    bus: DBus.SessionBus
    service: "org.freedesktop.systemd1"
    path: "/org/freedesktop/systemd1"
    iface: "org.freedesktop.systemd1.Manager"

    readonly property string unitname: Qt.application.organization + "." + Qt.application.name + "." + "watcher.service"
    property var serviceState

    function checkService() {
      systemd.typedCall("GetUnitFileState", [
        { "type": "s", "value":  [ systemd.unitname ]}
      ], function (result) { console.info('Service is: ' + result); systemd.serviceState = result },
        function (error, message) { console.error('Check Service call failed: ', error, 'message: ', message) }
      )
    }
    function enableService() {
      systemd.typedCall("EnableUnitFiles", [
        { "type": "as", "value":  [ systemd.unitname ]},      // array of service names
        { "type": "b", "value": "false"},                     // session only?
        { "type": "b", "value": "false"}                      // force?
      ],
        function (result) { console.debug('Service enabled: ' + result) },
        function (error, message) { console.error('Disable Service call failed: ', error, 'message: ', message) }
      )
    }
    function disableService() {
      systemd.typedCall("DisableUnitFiles", [
        { "type": "as", "value":  [ systemd.unitname ]},      // array of service names
        { "type": "b", "value": "false"}                      // session only?
      ], function (result) { console.debug('Service disabled: ' + result) },
        function (error, message) { console.error('Disable Service call failed: ', error, 'message: ', message) }
      )
    }
    function startService() {
      systemd.typedCall("StartUnit", [{ "type": "s", "value": systemd.unitname },
        { "type": "s", "value": "fail" }
      ], function (result) { console.debug('Service start successful: ' + result) },
        function (error, message) { console.error('Systemd call failed: ', error, 'message: ', message) }
      )
    }
    function stopService() {
      systemd.typedCall("StopUnit", [{ "type": "s", "value": systemd.unitname },
        { "type": "s", "value": "fail" }
      ], function (result) { console.debug('Service stop successful: ' + result) },
        function (error, message) { console.error('Systemd call failed: ', error, 'message: ', message) }
      )
    }
  }

  SilicaFlickable {
    id: flick
    anchors.fill: parent
    contentHeight: column.height

    Column {
      id: column
      anchors.horizontalCenter: parent.horizontalCenter
      width: parent.width - Theme.horizontalPageMargin

      PageHeader {
        title: qsTr("Daemon Settings")
      }
      Label {
        width: parent.width;
        color: Theme.highlightColor
        text: qsTr("Watches for %1 changes and applies actions defined below.").arg(qsTr("Ambience"))
        wrapMode : Text.WordWrap
        anchors { leftMargin: Theme.horizontalPageMargin; rightMargin: Theme.horizontalPageMargin }
      }
      IconTextSwitch {
        text: qsTr("Enable Watcher daemon")
        description: qsTr("Daemon is ") + ( checked ? qsTr("active") : qsTr("inactive"))
        icon.source: checked ? "image://theme/icon-splus-show-password" : "image://theme/icon-splus-hide-password"
        enabled: (typeof(page.serviceState) !== "undefined");
        busy: !enabled
        checked: (page.serviceState === "enabled");
        Behavior on enabled { FadeAnimation { duration: 200 } }
        onClicked: {
          if (checked) {
            systemd.enableService();
            systemd.startService()
          } else {
            systemd.stopService();
            systemd.disableService();
          }
          checkTimer.start();
        }
      }
      SectionHeader {
        text: qsTr("Actions")
      }
      Label {
        width: parent.width;
        color: Theme.highlightColor
        font.pixelSize: Theme.fontSizeSmall
        text: qsTr("Select what happens when an %1 change is detected").arg(qsTr("Ambience"))
        wrapMode : Text.WordWrap
        anchors { leftMargin: Theme.horizontalPageMargin; rightMargin: Theme.horizontalPageMargin }
      }
      TextSwitch {
        text: qsTr("launch %1").arg(AppInfo.displayName)
        description: qsTr("just opens the app")

        checked: conf.autoopen
        onClicked: conf.autoopen = checked
      }
      Label {
        width: parent.width
        color: Theme.secondaryHighlightColor
        text: Theme.highlightText(
          "⚠" + " "
          +  qsTr("Warning:") + " "
          + qsTr("The features below are experimental and do not work reliably.")
          , qsTr("Warning:"), Theme.presenceColor(Theme.PresenceAway)
        )
        wrapMode : Text.WordWrap
        font.pixelSize: Theme.fontSizeSmall
        anchors { leftMargin: Theme.horizontalPageMargin; rightMargin: Theme.horizontalPageMargin }
      }
      TextSwitch {
        text: qsTr("apply Top theme")
        description: qsTr("applies theme from the top %1 shelf").arg(qsTr("Ambience"))

        checked: conf.autoapply
        onClicked: conf.autoapply = checked;
      }
      TextSwitch {
        enabled: conf.autoapply // only enabled if the above is. watcher logic relies on this!
        text: qsTr("apply Night theme")
        description: qsTr("taken from the second %1 shelf").arg(qsTr("Ambience"))

        checked: conf.autoapplynight
        onClicked: conf.autoapplynight = checked;
      }

      Column {
        width: parent.width - Theme.itemSizeExtraSmall // TextSwitch toggle size
        x: Theme.itemSizeExtraSmall // TextSwitch toggle size
        enabled: conf.autoapplynight
        ValueButton {
          id: timeButtonStart
          enabled: parent.enabled
          label: qsTr("Night begins:")
          value: conf.nighthour + ":" + conf.nightminute
          onClicked: {
            var dialog = pageStack.push("Sailfish.Silica.TimePickerDialog", { hour: conf.nighthour, minute: conf.nightminute } )
            dialog.accepted.connect(function(){
              timeButtonStart.value = dialog.timeText;
              conf.setValue("nighthour", dialog.hour);
              conf.setValue("nightminute", dialog.minute);
            })
          }
        }
        ValueButton {
          id: timeButtonEnd
          enabled: parent.enabled
          label: qsTr("Night ends:")
          value: conf.dayhour + ":" + conf.dayminute
          onClicked: {
            var dialog = pageStack.push("Sailfish.Silica.TimePickerDialog", { hour: conf.dayhour, minute: conf.dayminute } )
            dialog.accepted.connect(function(){
              timeButtonEnd.value = dialog.timeText;
              conf.setValue("dayhour", dialog.hour);
              conf.setValue("dayminute", dialog.minute);
            })
          }
        }
      }

    } //Column
    VerticalScrollDecorator {}
  } //Flickable
} //Page

// vim: expandtab ts=4 st=4 filetype=javascript
