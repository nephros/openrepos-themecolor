// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.DBus 2.0
//import Sailfish.Share 1.0
import "../../components"

Page {
    id: page

    allowedOrientations: defaultAllowedOrientations

    readonly property string defaultPath: StandardPaths.documents
    property string exportPath : defaultPath
    property string cleanedUserName
    property string contentid
    /*
     * for the RPM builder to pick up
     */
    ConfigurationGroup {
        id: rpmparam
        path: "/org/nephros/openrepos-themecolor/rpmparam"
        synchronous: false
        property string ambname
        property string imgname
        property string ambfilename
        property string tcver

    }

    /*
    ShareAction {
        id: installer
        mimeType: "application/x-rpm"
        resources: []
        title: qsTr("Ambience Package")
    }
    */

    SilicaFlickable {
        id: flick
        contentHeight: col.height
        anchors.fill: parent
        Column {
            id: col
            width: parent.width - Theme.horizontalPageMargin
            spacing: Theme.paddingSmall
            anchors.horizontalCenter: parent.horizontalCenter
            PageHeader { title: qsTr("Export Functions") }
            Label {
                width: parent.width
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeSmall
                horizontalAlignment: Text.AlignJustify
                wrapMode: Text.WordWrap
                textFormat: Text.StyledText
                text: "<p>"
                    + qsTr("Here you can export your creation to a .ambience (JSON) file, create an installable package from it, and optionally install that on your device.")
                    + "</p><p>"
                    + qsTr("First, enter a name for your %1. You can edit the file by clicking the name, and generate a package from it using the Builder Button.").arg(qsTr("Ambience"))
                    + "<br />"
                    + qsTr("The .ambience file as well as any built packages will be stored in the %1 folder. (%2)", "first argument is the short name of the location, second argument is the full path").arg(qsTr("Documents")).arg(defaultPath)
                    + "</p>"
            }
            SectionHeader { text: qsTr("Disclaimer") }
            Label {
                width: parent.width
                anchors {
                    horizontalCenter: parent.horizontalCenter
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
                font.pixelSize: Theme.fontSizeSmall
                horizontalAlignment: Text.AlignJustify
                wrapMode: Text.WordWrap
                color: Theme.secondaryHighlightColor
                text: qsTr("If you decide to share or otherwise distribute the package make sure you respect copy- and other rights for any content included.")
            }

            SectionHeader { text: qsTr("Export to File") }
            TextField {
                id: username
                anchors.horizontalCenter: parent.horizontalCenter
                width: Math.max(implicitWidth, parent.width - Theme.itemSizeMedium)
                label: qsTr("Ambience Name")
                labelVisible: true
                placeholderText: qsTr("A cool Ambience Name")
                font.pixelSize: Theme.fontSizeLarge
                focus: true
                property string lasttext
                EnterKey.enabled: text.length > 0
                EnterKey.onClicked: { focus = false }
                onFocusChanged: { if (!focus) handleInput(); }
                function handleInput() {
                    if ( (text.length <= 0) || (text === placeholderText) || (text === lasttext) ) return
                    cleanedUserName = text.replace(/[^\.\-_a-zA-Z0-9]/g, "_");
                    contentid = Qt.md5(Date.now() + text);
                    prepareExportData();
                    lasttext = text;
                }
                function prepareExportData() {
                    busytimer.start();
                    /*
                     * prepare data
                     */
                    function colToHexStr(color) {
                        // stupid but we need to construct a correct string
                        const str = color.toString();
                        if (str.length === 9) { return str }; // we got alpha
                        if (str.length === 7) { str = str.substr(1,6) } else { console.warn("got a weird color...") }
                        const alpha = "";
                        if (color.a === NaN || (color.a == 1)) { alpha = "ff"; } else
                            if (color.a == 0)  { alpha = "00"; }
                        else { console.warn("got a weird alpha value...")}
                        return "#" + alpha + str;
                    }
                    var data = {};
                    // ambience expects full argb, 8(9) character color strings, but Theme.rgba squishes alpha if 1
                    data.primaryColor             = colToHexStr(MyPalette.primaryColor);
                    data.secondaryColor           = colToHexStr(MyPalette.secondaryColor);
                    data.highlightColor           = colToHexStr(MyPalette.highlightColor);
                    data.secondaryHighlightColor  = colToHexStr(MyPalette.secondaryHighlightColor);

                    data.highlightBackgroundColor = colToHexStr(MyPalette.highlightBackgroundColor);
                    data.highlightDimmerColor     = colToHexStr(MyPalette.highlightDimmerColor);
                    //data.overlayBackgroundColor   = MyPalette.overlayBackgroundColor.toString();
                    data.backgroundGlowColor      = colToHexStr(MyPalette.backgroundGlowColor);
                    //data._wallpaperOverlayColor   = MyPalette._wallpaperOverlayColor.toString();
                    //data._coverOverlayColor       = MyPalette._coverOverlayColor.toString();

                    data.colorScheme           = (MyPalette.colorScheme == Theme.LightOnDark) ? "lightondark" : "darkonlight" ;
                    data.displayName           = username.text;
                    data.version               = 3;
                    data.wallpaper             = Theme._homeBackgroundImage.toString()
                    //data.wallpaper             = imagepath.value;
                    //data.ringerVolume          = -1;
                    //data.translationCatalog    = username.text;
                    //data.favorite              = "true";

                    //data.contentId           = contentid;
                    console.debug("constructed data:\n" , JSON.stringify(data, null, 4) );

                    /*
                     * determine output file path and name
                     */
                    var filename = cleanedUserName + "_" + contentid.substr(0,8) + ".ambience";
                    var filePath = "/" + exportPath + "/" + filename;
                    var fileUrl = "file://" + filePath;

                    /*
                     * contruct upload request
                     */
                    var r = new XMLHttpRequest()
                    r.open('PUT', fileUrl);
                    var rdata = JSON.stringify(data, null, 4);
                    r.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
                    r.setRequestHeader('Content-length', rdata.length);
                    r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                    r.send(rdata);

                    r.onreadystatechange = function(event) {
                        if (r.readyState == XMLHttpRequest.DONE) {
                            //console.debug("request done.");
                            console.info(Qt.application.name, "wrote", fileUrl);
                            busytimer.stop();
                        }
                    }

                    rpmparam.ambname = cleanedUserName;
                    //rpmparam.imgname = imagepath.value;
                    // Theme._homeBackgroundImage is a file:// url, we need a path in the script
                    rpmparam.imgname = Theme._homeBackgroundImage.toString().replace("file://", "")
                    rpmparam.ambfilename = filePath;
                    rpmparam.tcver = AppInfo.versionstring;
                    rpmparam.sync();
                    //buttons.enabled = true;
                }
            }
            ValueButton { id: exportbutton
                anchors.horizontalCenter: parent.horizontalCenter
                width: Math.max(implicitWidth, username.width)
                description: qsTr("Click to Open")
                label: qsTr("File Name")
                value: (cleanedUserName ? cleanedUserName + "_" + contentid.substr(0,8) + ".ambience" : "" )
                enabled: ( username.text.length > 0 && username.text !== username.placeholderText && username.focus != true )
                onClicked: { Qt.openUrlExternally("file://" + rpmparam.ambfilename )}
            }
            Row { id: buttons
                property bool enabled: exportbutton.enabled
                spacing: Theme.paddingSmall
                anchors.horizontalCenter: parent.horizontalCenter
                //Button { enabled: parent.enabled; text: qsTr("Open File");      icon.source: "image://theme/icon-s-edit?" + (pressed ? Theme.highlightColor : Theme.primaryColor);      onClicked: { Qt.openUrlExternally("file://" + rpmparam.ambfilename )} }
                Button { enabled: parent.enabled; text: qsTr("Build Package"); icon.source: "image://theme/icon-s-developer?" + (pressed ? Theme.highlightColor : Theme.primaryColor); onClicked: { enabled=false; busytimer.start(); systemdbus.runBuilder("file://" + "/usr/share/applications/openrepos-themecolor-makeambience.desktop" ) } }
            }
            SilicaItem {
                width: parent.width
                height: Theme.itemSizeMedium
            }
        }
        VerticalScrollDecorator {}
    }
    PageBusyIndicator {
        running: busytimer.running
        anchors.centerIn: page
    }
    Timer {
        id: busytimer
        interval: 1600
        repeat: false
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
