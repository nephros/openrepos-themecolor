// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0

Page {
  id: page
  // we may be called from Jolla-settings, or from the app
  // show different things depending on which
  property bool isAdvanced: false
  allowedOrientations: defaultAllowedOrientations
  states: [
          State { name: "advanced"; PropertyChanges { page.isAdvanced: true} },
          State { name: "settings"; PropertyChanges { page.isAdvanced: false} }
  ]
  SilicaFlickable {
    id: flick
    anchors.fill: parent
    contentHeight: flw.height + head.height
    PageHeader { id: head ; title: qsTr("Advanced Options") }
    Flow {
      id: flw
      spacing: Theme.paddingMedium
      width: parent.width - Theme.horizontalPageMargin
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.top: head.bottom
      Column {
        width: (isLandscape) ? ( parent.width / 2)  - Theme.horizontalPageMargin : parent.width
        SectionHeader { text: qsTr("System Functions"); visible: page.isAdvanced}
        ValueButton {
          visible: page.isAdvanced
          label: qsTr("Restart %1", "restart an application, as in 'restart service foo'").arg("Lipstick");
          description: qsTr("Restart the %1 service. This will close all apps and relaunch the %2", "arguments are 'Lipstick' and 'Home Screen', one id the application name, one is the commonly used name for the same.").arg(qsTr("Lipstick")).arg(qsTr("Home Screen"));
          onClicked: { applyRemorse.execute( qsTr("Restarting") + "…", function () { restartLipstick() } ) }
        }
        ValueButton {
          visible: page.isAdvanced
          label: qsTr("Restart %1", "restart an application, as in 'restart service foo'").arg("ambienced");
          description: qsTr("Restart the %1 service. This sometimes fixes color problems").arg(qsTr("Ambience"))
          onClicked: { applyRemorse.execute( qsTr("Restarting") + "…", function () { restartAmbienced() } ) }
        }
        /*
         * Does not work when sandboxed...
         *
        ValueButton {
          visible: page.isAdvanced
          label: qsTr("Ambience Settings");
          description: qsTr("Open Ambience System Settings");
          onClicked: { settings.open() }
        }
        */
        SectionHeader { text: qsTr("Config Management") }
        ValueButton {
          label: qsTr("Reset Config");
          description: qsTr("Reset all color-related configuration values");
          onClicked: { applyRemorse.execute( qsTr("Resetting") + "…", function () { resetDconf(); } ) }
        }
        ValueButton {
          label: qsTr("Reset Config and Restart");
          description: qsTr("Reset all color-related configuration values and restart %1").arg(qsTr("Lipstick"));
          onClicked: { applyRemorse.execute( qsTr("Resetting") + "…", function () { resetDconf(); restartLipstick(); } ) }
        }
        Separator { horizontalAlignment: Qt.AlignHCenter ; color: Theme.secondaryHighlightColor }
      }
      Column {
        width: (isLandscape) ? ( parent.width / 2)  - Theme.horizontalPageMargin : parent.width
        SectionHeader { text: qsTr("Advanced Editors") }
        ValueButton {
          label: qsTr("Edit Global Blur");
          description: qsTr("Edit blur parameters")
          onClicked: { pageStack.push(Qt.resolvedUrl("advanced/BlurEditor.qml")) }
        }
        /*
        ValueButton {
          label: qsTr("Edit Transparency");
          onClicked: { pageStack.push(Qt.resolvedUrl("advanced/TransparencyEditor.qml")) }
        }
        */
        SectionHeader { text: qsTr("Background Service") }
        ValueButton {
          label: qsTr("Set up daemon");
          description: qsTr("Background service which watches for %1 changes and applies actions.").arg(qsTr("Ambience"))
          onClicked: { pageStack.push(Qt.resolvedUrl("advanced/DaemonSettings.qml")) }
        }
        SectionHeader { text: qsTr("Import/Export"); visible: page.isAdvanced }
        ValueButton {
          visible: page.isAdvanced
          label: qsTr("Load Ambience File");
          description: qsTr("Load a color scheme from a .ambience file.")
          onClicked: { var dialog = pageStack.replace(Qt.resolvedUrl("advanced/LoadAmbience.qml")) }
        }
      }
    }
    VerticalScrollDecorator {}
  }
  RemorsePopup { id: applyRemorse }
}

// vim: expandtab ts=4 st=4 filetype=javascript
