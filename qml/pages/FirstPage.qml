// This file is part of ThemeColor.
// Copyright (c) 2021-2023 Peter Gantner (nephros)
// SPDX-License-Identifier: MIT

import QtQuick 2.6
import Sailfish.Silica 1.0
import Sailfish.Silica.Background 1.0
import "../components/showroom"

Dialog { id: firstPage

    allowedOrientations: defaultAllowedOrientations

    // populate the pageStack
    backNavigation: false // we are the bottom of the stack
    onStatusChanged: {
        if ( status === PageStatus.Active && pageStack.nextPage() === null ) {
            pageStack.pushAttached(Qt.resolvedUrl("LabPage.qml"))
        }
    }

    background: ThemeImageWallpaper { }

    SilicaFlickable { id: flick
        anchors.fill: parent
        contentHeight: col.height //+ Theme.itemSizeMedium*3 // contextmenu isn't counted
        Column { id: col
            width: parent.width
            DialogHeader { id: head; title: qsTr("Showroom"); acceptText: qsTr("Atelier"); }
            //ShowRoomBG{ width: parent.width }
            ShowRoomText{ width: parent.width - Theme.horizontalPageMargin}
            ShowRoomUI  { width: parent.width - Theme.horizontalPageMargin}
        }
        MainMenu{}
        RemorsePopup { id: applyRemorse }
        VerticalScrollDecorator {}
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
