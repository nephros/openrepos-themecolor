<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>AdvancedPage</name>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="22"/>
        <source>Advanced Options</source>
        <translation>Avancerade alternativ</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="31"/>
        <source>System Functions</source>
        <translation type="unfinished">System Functions</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="34"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="40"/>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation>Starta om %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="62"/>
        <source>Lipstick</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <source>Home Screen</source>
        <translation>Hemskärm</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <source>Restart the %1 service. This will close all apps and relaunch the %2</source>
        <comment>arguments are &apos;Lipstick&apos; and &apos;Home Screen&apos;, one id the application name, one is the commonly used name for the same.</comment>
        <translation>Starta om %1-tjänsten. Detta stänger alla appar och startar om %2.</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="36"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="42"/>
        <source>Restarting</source>
        <translation>Startar om...</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="41"/>
        <source>Restart the %1 service. This sometimes fixes color problems</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="41"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="84"/>
        <source>Ambience</source>
        <translation>Atmosfär</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="54"/>
        <source>Config Management</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="56"/>
        <source>Reset Config</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="57"/>
        <source>Reset all color-related configuration values</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="58"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="63"/>
        <source>Resetting</source>
        <translation>Återställer</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="61"/>
        <source>Reset Config and Restart</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="62"/>
        <source>Reset all color-related configuration values and restart %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="69"/>
        <source>Advanced Editors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="71"/>
        <source>Edit Global Blur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="72"/>
        <source>Edit blur parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="81"/>
        <source>Background Service</source>
        <translation>Bakgrundstjänst</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="83"/>
        <source>Set up daemon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="84"/>
        <source>Background service which watches for %1 changes and applies actions.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="87"/>
        <source>Import/Export</source>
        <translation>Importera/Exportera</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="90"/>
        <source>Load Ambience File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="91"/>
        <source>Load a color scheme from a .ambience file.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AppInfoSingleton</name>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="25"/>
        <source>ThemeColor</source>
        <comment>this will also be used for the app icon</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="26"/>
        <source>ThemeColor® RPM Builder™</source>
        <comment>this will also be used for the app icon</comment>
        <translation>ThemeColor® RPM-byggare™</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="38"/>
        <source>English</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Engelsk</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="35"/>
        <source>German</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Tysk</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="48"/>
        <source>Norwegian Bokmål</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Norska Bokmål</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="41"/>
        <source>Spanish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Spanska</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="45"/>
        <source>French</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Franska</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="51"/>
        <source>Russian</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="54"/>
        <source>Swedish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="57"/>
        <source>Simplified Chinese</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Förenklad kinesiska</translation>
    </message>
</context>
<context>
    <name>BlurEditor</name>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="53"/>
        <source>Edit Blur Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="63"/>
        <source>These are some values found in Lipstick. They are exposed here for your experimentation, although it is unclear where exactly they are applied, and changing them seems to have very little effect. They are also not part of Ambiences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="67"/>
        <source>Blur Iterations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="77"/>
        <source>Blur Deviation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="87"/>
        <source>Blur Kernel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorField</name>
    <message>
        <source>specify RGB or aRGB value, e.g.</source>
        <translation type="vanished">ange RGB- eller aRGB-värde, t.ex.</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="13"/>
        <source>an %1 value</source>
        <comment>parameter is either RGB or aRGB</comment>
        <translation>%1-värde</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="14"/>
        <source>(the # is optional)</source>
        <translation>(# är valfritt)</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="14"/>
        <source>e.g.</source>
        <translation>t.ex.</translation>
    </message>
</context>
<context>
    <name>ColorFilters</name>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="21"/>
        <source>Red</source>
        <translation>Röd</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="21"/>
        <source>Tint %1</source>
        <translation>Tonfärg %1</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="29"/>
        <source>Darken</source>
        <translation>Mörkare</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="37"/>
        <source>Brighten</source>
        <translation>Lysa upp</translation>
    </message>
</context>
<context>
    <name>ColorGenerators</name>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="14"/>
        <source>Randomizer</source>
        <translation>Slumpmässig</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="16"/>
        <source>Filters</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="18"/>
        <source>Scheme Generators</source>
        <translation>System generatorer</translation>
    </message>
</context>
<context>
    <name>ColorRandomizer</name>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="20"/>
        <source>Random</source>
        <translation>Slumpmässig</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="25"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="39"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="49"/>
        <source>Generated</source>
        <translation>Genererad</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="30"/>
        <source>Bright</source>
        <translation>Ljus</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="30"/>
        <source>Dark</source>
        <translation>Mörk</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="44"/>
        <source>Gray</source>
        <translation>Grå</translation>
    </message>
</context>
<context>
    <name>ColorSchemeGenerator</name>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="17"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="32"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="39"/>
        <source>%1 Theme</source>
        <comment>parameter is name for a theme, e.g. night, day, random... you might just use the parameter and not translate theme at all.</comment>
        <translation>%1 Tema</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="17"/>
        <source>Solarize</source>
        <comment>as in &apos;solarized&apos; color scheme</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="24"/>
        <source>Generate</source>
        <translation>Generera</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="24"/>
        <source>from</source>
        <translation>från</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="24"/>
        <source>Highlight</source>
        <translation>Färgmarkering</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="32"/>
        <source>Night</source>
        <translation>Natt</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="52"/>
        <source>%1 Color Blindness</source>
        <comment>%1 is replaced with the variant of color blindness, as in &apos;red-green color-blindness&apos;</comment>
        <translation>%1 Full färgblindhet</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="52"/>
        <source>R/G</source>
        <comment>abbreviation for red-green color blindness</comment>
        <translation>R/G</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="52"/>
        <source>#%1/%2</source>
        <comment>for lists, as in &apos;number one of four&apos; is #1/4</comment>
        <translation>#%1/%2</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="39"/>
        <source>Summer</source>
        <translation>Sommar</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="27"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="35"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="42"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="58"/>
        <source>Applying</source>
        <translation>Tillämpar</translation>
    </message>
</context>
<context>
    <name>ColorSlider</name>
    <message>
        <location filename="../qml/components/controls/ColorSlider.qml" line="66"/>
        <source>Adjust sliders, tap to reset</source>
        <translation>Justera med skjutreglage, tryck för att återställa</translation>
    </message>
</context>
<context>
    <name>ColorSliders</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="15"/>
        <source>Primary Color</source>
        <translation>Primär färg</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="21"/>
        <source>Secondary Color</source>
        <translation>Sekundär färg</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="28"/>
        <source>Highlight Color</source>
        <translation>Markeringsfärg</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="35"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundär markeringsfärg</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="42"/>
        <source>Background Highlight Color</source>
        <translation>Markeringsbakgrundsfärg</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="49"/>
        <source>Dim Highlight Color</source>
        <translation>Färgton för tonad markering</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Markeringsbakgrundsfärg</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="56"/>
        <source>Background Glow Color</source>
        <translation>Bakgrundsfärg för glöd</translation>
    </message>
</context>
<context>
    <name>ColorSwapper</name>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="70"/>
        <source>Copy</source>
        <translation>Kopiera</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="70"/>
        <source>Swap</source>
        <translation>Ändra</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="91"/>
        <location filename="../qml/components/ColorSwapper.qml" line="118"/>
        <source>Primary Color</source>
        <translation>Primär färg</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="92"/>
        <location filename="../qml/components/ColorSwapper.qml" line="119"/>
        <source>Secondary Color</source>
        <translation>Sekundär färg</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="93"/>
        <location filename="../qml/components/ColorSwapper.qml" line="120"/>
        <source>Highlight Color</source>
        <translation>Markeringsfärg</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="94"/>
        <location filename="../qml/components/ColorSwapper.qml" line="121"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundär markeringsfärg</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="95"/>
        <location filename="../qml/components/ColorSwapper.qml" line="122"/>
        <source>Background Highlight Color</source>
        <translation>Markeringsbakgrundsfärg</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="96"/>
        <location filename="../qml/components/ColorSwapper.qml" line="123"/>
        <source>Dim Highlight Color</source>
        <translation>Färgton för tonad markering</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Markeringsbakgrundsfärg</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Colour</translation>
    </message>
</context>
<context>
    <name>ColorTextInput</name>
    <message>
        <source>Input value, tap to reset</source>
        <translation type="vanished">Indatavärde, tryck för att återställa</translation>
    </message>
</context>
<context>
    <name>ColorTextInputDialog</name>
    <message>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="16"/>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="32"/>
        <source>Color input</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ColorTextInputs</name>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primär färg</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Sekundär färg</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Markeringsfärg</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Sekundär markeringsfärg</translation>
    </message>
    <message>
        <source>Background Highlight Color</source>
        <translation type="vanished">Markera bakgrundsfärg</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">Färgton för tonad markering</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Markera bakgrundsfärg</translation>
    </message>
    <message>
        <source>Background Glow Color</source>
        <translation type="vanished">Färg för bakgrundsglöd</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">ThemeColor</translation>
    </message>
</context>
<context>
    <name>DaemonSettings</name>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="109"/>
        <source>Daemon Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="119"/>
        <source>Enable Watcher daemon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="114"/>
        <source>Watches for %1 changes and applies actions defined below.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="120"/>
        <source>Daemon is </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="120"/>
        <source>active</source>
        <translation>aktiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="120"/>
        <source>inactive</source>
        <translation>inaktiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="138"/>
        <source>Actions</source>
        <translation>Åtgärder</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="144"/>
        <source>Select what happens when an %1 change is detected</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="149"/>
        <source>launch %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="150"/>
        <source>just opens the app</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="160"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="162"/>
        <source>Warning:</source>
        <translation>Varning:</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="161"/>
        <source>The features below are experimental and do not work reliably.</source>
        <translation>Funktionerna nedan är experimentella och fungerar inte tillförlitligt.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="169"/>
        <source>apply Top theme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="170"/>
        <source>applies theme from the top %1 shelf</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="177"/>
        <source>apply Night theme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="114"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="144"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="170"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="178"/>
        <source>Ambience</source>
        <translation>Atmosfär</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="178"/>
        <source>taken from the second %1 shelf</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="191"/>
        <source>Night begins:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="205"/>
        <source>Night ends:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <source>Adjust Theme Colors</source>
        <translation type="vanished">Justera temafärger</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="29"/>
        <source>Showroom</source>
        <translation>Utställningsrum</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="29"/>
        <source>Atelier</source>
        <translation type="unfinished">Laboratorium</translation>
    </message>
    <message>
        <source>Input Mode:</source>
        <translation type="vanished">Input Mode:</translation>
    </message>
    <message>
        <source>Tap to switch</source>
        <translation type="vanished">Tryck för att växla</translation>
    </message>
    <message>
        <source>Swapper/Copier</source>
        <translation type="vanished">Utbytare/Kopierare</translation>
    </message>
    <message>
        <source>Applying</source>
        <translation type="vanished">Tillämpning av</translation>
    </message>
    <message>
        <source>Apply Colors to System</source>
        <translation type="vanished">Tillämpa färger på systemet</translation>
    </message>
    <message>
        <source>Advanced…</source>
        <translation type="vanished">Avancerat…</translation>
    </message>
    <message>
        <source>Reload Colors from System</source>
        <translation type="vanished">Reload Colours from System</translation>
    </message>
    <message>
        <source>Generators</source>
        <translation type="vanished">Generator</translation>
    </message>
    <message>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation type="vanished">Handbok</translation>
    </message>
    <message>
        <source>Reload Colors from current Theme</source>
        <translation type="vanished">Ladda om färger från aktuellt tema</translation>
    </message>
    <message>
        <source>Reload Colors from System Config</source>
        <translation type="vanished">Ladda om färger från systemkonfigurationen</translation>
    </message>
    <message>
        <source>Experimental or dangerous actions</source>
        <translation type="vanished">Experimentella eller farliga åtgärder</translation>
    </message>
    <message>
        <source>Export to Ambience package</source>
        <translation type="vanished">Exportera till Ambience-paketet</translation>
    </message>
    <message>
        <source>Edit Transparency</source>
        <translation type="vanished">Redigera transparens</translation>
    </message>
    <message>
        <source>Resetting</source>
        <translation type="vanished">Återställer</translation>
    </message>
    <message>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation type="vanished">Starta om %1</translation>
    </message>
    <message>
        <source>Restarting</source>
        <translation type="vanished">Startar om...</translation>
    </message>
    <message>
        <source>Reset all values and restart</source>
        <translation type="vanished">Återställ alla värden och starta om</translation>
    </message>
    <message>
        <source>Reset nonstandard values</source>
        <translation type="vanished">Återställ icke-standardiserade värden</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Hjälp</translation>
    </message>
    <message>
        <source>Sliders</source>
        <translation type="vanished">Reglage</translation>
    </message>
    <message>
        <source>Swapper</source>
        <translation type="vanished">Swapper</translation>
    </message>
    <message>
        <source>Jolla Original</source>
        <translation type="vanished">Jolla Original</translation>
    </message>
    <message>
        <source>Open Ambience Settings</source>
        <translation type="vanished">Öppna ambiensinställningar</translation>
    </message>
</context>
<context>
    <name>HelpColors</name>
    <message>
        <source>The four basic Colors</source>
        <translation type="vanished">De fyra grundläggande färgerna</translation>
    </message>
    <message>
        <source>The four basic Colors</source>
        <comment>Help Section</comment>
        <translation type="vanished">De fyra grundläggande färgerna</translation>
    </message>
    <message>
        <source>Additional Colors</source>
        <comment>Help Section</comment>
        <translation type="vanished">Ytterligare färger</translation>
    </message>
</context>
<context>
    <name>HelpCredits</name>
    <message>
        <source>About</source>
        <translation type="obsolete">Om</translation>
    </message>
    <message>
        <source>About</source>
        <comment>Help Section</comment>
        <translation type="vanished">Om</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">Version:</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation type="vanished">Copyright:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Licens:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation type="vanished">Källkod:</translation>
    </message>
    <message>
        <source>Translations</source>
        <translation type="vanished">Översättningar</translation>
    </message>
</context>
<context>
    <name>HelpDaemon</name>
    <message>
        <source>Ambience</source>
        <translation type="vanished">Atmosfär</translation>
    </message>
</context>
<context>
    <name>HelpExport</name>
    <message>
        <source>Exporting Ambiences</source>
        <translation type="vanished">Exportera Ambiences</translation>
    </message>
    <message>
        <source>Exporting Ambiences</source>
        <comment>Help Section</comment>
        <translation type="vanished">Exportera Ambiences</translation>
    </message>
    <message>
        <source>Export to Ambience package</source>
        <translation type="vanished">Exportera till Ambience-paketet</translation>
    </message>
    <message>
        <source>The Builder</source>
        <comment>Help Section</comment>
        <translation type="vanished">Byggmästaren</translation>
    </message>
    <message>
        <source>Notes</source>
        <comment>Help Section</comment>
        <extracomment>as in something to add, not a small piece of paper</extracomment>
        <translation type="vanished">Anteckningar</translation>
    </message>
    <message>
        <source>The Builder</source>
        <translation type="vanished">Byggmästaren</translation>
    </message>
    <message>
        <source>Notes</source>
        <comment>as in something to add, not a small piece of paper </comment>
        <translation type="vanished">Anteckningar</translation>
    </message>
</context>
<context>
    <name>HelpGeneral</name>
    <message>
        <source>General</source>
        <translation type="vanished">Allmän</translation>
    </message>
    <message>
        <source>General</source>
        <comment>Help Section</comment>
        <translation type="vanished">Allmän</translation>
    </message>
    <message>
        <source>Basic Workflow</source>
        <comment>Help Section</comment>
        <translation type="vanished">Grundläggande arbetsflöde</translation>
    </message>
    <message>
        <source>Basic Workflow</source>
        <translation type="vanished">Grundläggande arbetsflöde</translation>
    </message>
</context>
<context>
    <name>HelpHow</name>
    <message>
        <source>App Configuration and State</source>
        <translation type="vanished">Appkonfiguration och -tillstånd</translation>
    </message>
    <message>
        <source>App Configuration and State</source>
        <comment>Help Section</comment>
        <translation type="vanished">Appkonfiguration och -tillstånd</translation>
    </message>
    <message>
        <source>The Builder</source>
        <comment>Help Section</comment>
        <translation type="vanished">Byggmästaren</translation>
    </message>
    <message>
        <source>The Builder</source>
        <translation type="vanished">Byggmästaren</translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <source>Use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            When satisfied, tap the area above the slider to set the color.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</source>
        <translation type="vanished">Use the Sliders in the lower section to adjust the colours.&lt;br /&gt;            When satisfied, tap the area above the slider to set the colour.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</translation>
    </message>
    <message>
        <source>How to Use</source>
        <translation type="vanished">Hur du använder</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="vanished">Allmänt</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colors can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor, as these are the ones saved by the system.          There are other colors in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colours can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor (sic!), as these are the ones saved by the system.          There are other colours in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only some colors can be edited.          There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited. There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <translation type="vanished">Utställningsrummet</translation>
    </message>
    <message>
        <source>The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colors that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </source>
        <translation type="vanished">The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colours that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </translation>
    </message>
    <message>
        <source>The Atelier</source>
        <translation type="vanished">Laboratoriet</translation>
    </message>
    <message>
        <source>In Slider Input Mode, use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            In Text Input Mode, you can enter color values directly.&lt;br /&gt;
            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.
          </source>
        <translation type="vanished">In Slider Input Mode, use the Sliders in the lower section to adjust the colours.&lt;br /&gt;            In Text Input Mode, you can enter colour values directly.&lt;br /&gt;
            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.
          </translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.            In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know.&lt;br /&gt;             Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullDown Menu.
          </source>
        <translation type="vanished">In Slider input mode, use the Sliders in the lower section to adjust the colours. In Text input mode, you can enter colour values directly. Randomizer does what it sais, and Jolla Original you already know.&lt;br /&gt;Check what your theme will look like in the Showroom display.&lt;br /&gt; &lt;br /&gt; When you&apos;re done, use the PullDown Menu.
          </translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <translation type="vanished">Skåpet</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.             There is one global Cupboard, and one specific for the current ambience.&lt;br /&gt;             Note that only system-wide Ambiences have a name, custon ones will show as anonymous (for now)</source>
        <translation type="vanished">This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt; Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</translation>
    </message>
    <message>
        <source>Tips and Caveats</source>
        <translation type="vanished">Tips och varningar</translation>
    </message>
    <message>
        <source>It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
          It&apos;s a good idea to store a known-good color sceme in the Cupboard so you can go restore easily.&lt;br /&gt;          &lt;br /&gt;          If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;          &lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;          &lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;          and repeat for all the other colors stored there. &lt;br /&gt;          Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;&lt;br /&gt;If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;and repeat for all the other colours stored there. &lt;br /&gt;Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, 
nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited.
There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited.
There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.  In Text input mode, you can enter color values directly. Randomizer
does what it sais, and Jolla Original you already know.  Swapper lets you change color definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="vanished">In Slider input mode, use the Sliders in the lower section to adjust the colours. In Text input mode, you can enter colour values directly. Randomizer
does what it sais, and Jolla Original you already know. Swapper lets you change colour definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colours to the current session.</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="vanished">This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, 
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. 
This means once applied through the app, they will always stay the same until you change them again in the App. 
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard.
If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla Ambiences only define four colours, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added,
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colours than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there.
This means once applied through the app, they will always stay the same until you change them again in the App.
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colours when switching editing modes, applying colours to system, or taking palettes from the Cupboard.
If that happens, try reloading the colours from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colours somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colours stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Licens:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation type="vanished">Källkod:</translation>
    </message>
    <message>
        <source>Translations:</source>
        <translation type="vanished">Översättningar:</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App.  You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla Ambiences only define four colours, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colours than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colours when switching editing modes, applying colours to system, or taking palettes from the Cupboard. If that happens, try reloading the colours from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colours somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colours stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">Om</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">Version: </translation>
    </message>
    <message>
        <source>Copyright: </source>
        <translation type="vanished">Copyright: </translation>
    </message>
    <message>
        <source>License: </source>
        <translation type="vanished">Licence: </translation>
    </message>
    <message>
        <source>Source Code: </source>
        <translation type="vanished">Source Code: </translation>
    </message>
    <message>
        <source>General Information</source>
        <translation type="vanished">Generell information</translation>
    </message>
    <message>
        <source>About the UI</source>
        <translation type="vanished">Om användargränssnittet</translation>
    </message>
    <message>
        <source>About Colors</source>
        <translation type="vanished">Om färger</translation>
    </message>
    <message>
        <source>Exporting Ambiences</source>
        <translation type="vanished">Exportera Ambiences</translation>
    </message>
    <message>
        <source>How it works</source>
        <translation type="vanished">Hur det fungerar</translation>
    </message>
    <message>
        <source>Limitations, Tips etc.</source>
        <translation type="vanished">Begränsningar, tips etc.</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="vanished">Krediter</translation>
    </message>
    <message>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation type="vanished">Handbok</translation>
    </message>
    <message>
        <source>Chapter %1:</source>
        <comment>chapter in the handbook. argument is a number</comment>
        <translation type="vanished">Kapitel %1:</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="65"/>
        <source>Select a chapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="66"/>
        <source>Tap to switch</source>
        <translation type="unfinished">Tryck för att växla</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="73"/>
        <source>General Information</source>
        <comment>Help Index</comment>
        <translation>Generell information</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="74"/>
        <source>About the UI</source>
        <comment>Help Index</comment>
        <translation>Om användargränssnittet</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="75"/>
        <source>About Colors</source>
        <comment>Help Index</comment>
        <translation>Om färger</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="76"/>
        <source>Exporting Ambiences</source>
        <comment>Help Index</comment>
        <translation>Exportera Ambiences</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="77"/>
        <source>Watcher Daemon</source>
        <comment>Help Index</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="78"/>
        <source>How it works</source>
        <comment>Help Index</comment>
        <translation>Hur det fungerar</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="79"/>
        <source>Limitations , Tips etc.</source>
        <comment>Help Index</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="102"/>
        <source>View %1</source>
        <comment>User Manual display language</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="102"/>
        <source>English</source>
        <translation type="unfinished">Engelsk</translation>
    </message>
    <message>
        <source>Credits</source>
        <comment>Help Index</comment>
        <translation type="vanished">Krediter</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="59"/>
        <source>Handbook</source>
        <comment>Help Index</comment>
        <extracomment>User Manual, Application Manual, Handbook, Howto</extracomment>
        <translation>Handbok</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="65"/>
        <source>Chapter %1:</source>
        <comment>Help Index Chapter Prefix</comment>
        <translation>Kapitel %1:</translation>
    </message>
</context>
<context>
    <name>HelpTips</name>
    <message>
        <source>Tips and Caveats</source>
        <translation type="vanished">Tips och varningar</translation>
    </message>
    <message>
        <source>Tips and Caveats</source>
        <comment>Help Section</comment>
        <translation type="vanished">Tips och varningar</translation>
    </message>
</context>
<context>
    <name>HelpUIMain</name>
    <message>
        <source>&lt;p&gt;The main UI consists of the following:
       &lt;ul&gt;
       &lt;li&gt;The Showroom&lt;/li&gt;
       &lt;li&gt;The Atelier&lt;/li&gt;
       &lt;li&gt;The Cupboards&lt;/li&gt;
       &lt;li&gt;PullDown and PushUp Menus&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;Det huvudsakliga användargränssnittet består av följande:
       &lt;ul&gt;
       &lt;li&gt;Utställningsrummet&lt;/li&gt;
       &lt;li&gt;Laboratoriet&lt;/li&gt;
       &lt;li&gt;Skåp&lt;/li&gt;
       &lt;li&gt;PullDown- och PushUp-menyer&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <translation type="vanished">Utställningsrummet</translation>
    </message>
    <message>
        <source>The Atelier</source>
        <translation type="vanished">Laboratoriet</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <comment>Help Section</comment>
        <translation type="vanished">Utställningsrummet</translation>
    </message>
    <message>
        <source>The Atelier</source>
        <comment>Help Section</comment>
        <translation type="vanished">Laboratoriet</translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <comment>Help Section</comment>
        <translation type="vanished">Skåpet</translation>
    </message>
    <message>
        <source>Put on Shelf</source>
        <translation type="vanished">Lägg på hyllan</translation>
    </message>
    <message>
        <source>Take to Lab</source>
        <translation type="vanished">Ta till Lab</translation>
    </message>
    <message>
        <source>Menus</source>
        <comment>Help Section</comment>
        <translation type="vanished">Menyer</translation>
    </message>
    <message>
        <source>Menus</source>
        <translation type="vanished">Menyer</translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <translation type="vanished">Skåpet</translation>
    </message>
</context>
<context>
    <name>LabPage</name>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="38"/>
        <source>Atelier</source>
        <translation type="unfinished">Laboratorium</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="38"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="38"/>
        <source>Cupboards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="43"/>
        <source>Current Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="48"/>
        <source>Editor</source>
        <comment>tool/editing mode</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="49"/>
        <source>Tap to switch</source>
        <translation type="unfinished">Tryck för att växla</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="57"/>
        <source>Sliders</source>
        <translation type="unfinished">Reglage</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="58"/>
        <source>Swapper/Copier</source>
        <translation type="unfinished">Utbytare/Kopierare</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="59"/>
        <source>Generators</source>
        <translation type="unfinished">Generator</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="60"/>
        <source>Jolla Original</source>
        <translation type="unfinished">Jolla Original</translation>
    </message>
</context>
<context>
    <name>LoadAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="36"/>
        <source>Load</source>
        <translation>Läs in</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="97"/>
        <source>Load Theme</source>
        <translation type="unfinished">Läs in tema</translation>
    </message>
    <message>
        <source>Take to Lab</source>
        <translation type="vanished">Ta till Lab</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="97"/>
        <source>Take to Atelier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="115"/>
        <source>Here you can load a .ambience (JSON) file.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="119"/>
        <source>current</source>
        <translation>Aktuell</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="125"/>
        <source>file data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="131"/>
        <source>File name </source>
        <translation>Filnamn </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="133"/>
        <source>Click to select</source>
        <translation>klicka för att välja</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="140"/>
        <source>File content</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="149"/>
        <source>No file loaded</source>
        <translation>Inga filer inlästa</translation>
    </message>
</context>
<context>
    <name>LootBoxItem</name>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="27"/>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="32"/>
        <source>ThemeColor</source>
        <translation>ThemeColor</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="31"/>
        <source>A Lootbox was delivered!</source>
        <translation>En lootbox har levererats!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="32"/>
        <source>now has more shelves!</source>
        <translation>har nu fler hyllor!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="32"/>
        <source>Your persistence has been rewarded.</source>
        <translation>Din uthållighet har belönats.</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="33"/>
        <source>Your persistence has been rewarded!</source>
        <translation>Din uthållighet har belönats!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="43"/>
        <source>Purchase Options</source>
        <translation>Köpoptioner</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="52"/>
        <source>Payment* received!</source>
        <translation>Betalning* mottagen!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="52"/>
        <source>Buy more shelves</source>
        <translation>Köp fler hyllor</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="57"/>
        <source>Using Jolla Shop credentials to buy Storage Lootbox</source>
        <translation>Använder Jolla Shop-referenser för att köpa Storage Lootbox</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="74"/>
        <source>Thank you for your purchase!&lt;br /&gt;Your extra shelves will be delivered in the next update!</source>
        <translation>Tack för ditt köp!&lt;br /&gt;Dina extra hyllor kommer att levereras i nästa uppdatering!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="88"/>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;…or did it?</source>
        <translation type="unfinished">*) Nej, det finns inga köp i appen eller lootboxar i Jolla Shop. Det skulle vara löjligt.&lt;br /&gt;Ingen pengar överfördes.&lt;br /&gt; Det hände faktiskt ingenting just nu.&lt;br /&gt;...eller gjorde det?</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="9"/>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation type="unfinished">Handbok</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="12"/>
        <source>Advanced…</source>
        <translation type="unfinished">Avancerat…</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="15"/>
        <source>Export to Ambience package</source>
        <translation type="unfinished">Exportera till Ambience-paketet</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="18"/>
        <source>Apply Colors to System</source>
        <translation type="unfinished">Tillämpa färger på systemet</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="19"/>
        <source>Applying</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="21"/>
        <source>Reload Colors from System</source>
        <translation type="unfinished">Reload Colours from System</translation>
    </message>
</context>
<context>
    <name>SaveAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="53"/>
        <source>Export Functions</source>
        <translation>Exportfunktioner</translation>
    </message>
    <message>
        <source>Here you can export your creation to an .ambience (JSON) file.&lt;br /&gt;
                      This file can be picked up by the ThemeColor® RPM Builder™ companion tool, which will package the whole thing as an RPM you can then install.
                     </source>
        <translation type="vanished">Här kan du exportera din skapelse till en .ambience-fil (JSON).&lt;br /&gt;
 Denna fil kan hämtas av det kompletterande verktyget ThemeColor® RPM Builder™, som paketerar det hela som en RPM, som du sedan kan installera.
                     </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="67"/>
        <source>Here you can export your creation to a .ambience (JSON) file, create an installable package from it, and optionally install that on your device.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="69"/>
        <source>Ambience</source>
        <translation>Atmosfär</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="69"/>
        <source>First, enter a name for your %1. You can edit the file by clicking the name, and generate a package from it using the Builder Button.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="71"/>
        <source>The .ambience file as well as any built packages will be stored in the %1 folder. (%2)</source>
        <comment>first argument is the short name of the location, second argument is the full path</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="89"/>
        <source>Export to File</source>
        <translation>Exportera till Fil</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="94"/>
        <source>Ambience Name</source>
        <translation>Ambiente Namn</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="96"/>
        <source>A cool Ambience Name</source>
        <translation>Ett häftigt atmosfärnamn</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="191"/>
        <source>Click to Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="202"/>
        <source>Build Package</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="192"/>
        <source>File Name</source>
        <translation>Filnamn</translation>
    </message>
    <message>
        <source>Open File</source>
        <translation type="vanished">Öppna fil</translation>
    </message>
    <message>
        <source>Launch Builder</source>
        <translation type="vanished">Lanseringsbyggare</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="74"/>
        <source>Disclaimer</source>
        <translation>Ansvarsfriskrivning</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="71"/>
        <source>Documents</source>
        <translation>Dokument</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="86"/>
        <source>If you decide to share or otherwise distribute the package make sure you respect copy- and other rights for any content included.</source>
        <translation></translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Hjälp</translation>
    </message>
</context>
<context>
    <name>SaveSlot</name>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="45"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="46"/>
        <source>Ambience</source>
        <translation>Atmosfär</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="45"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="46"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="48"/>
        <source>Shelf</source>
        <translation>Hylla</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <source>default</source>
        <translation>standard</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="45"/>
        <source>night mode</source>
        <translation>Nattläge</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="75"/>
        <source>Take to Atelier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Take to Lab</source>
        <translation type="vanished">Ta till Lab</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="76"/>
        <source>Put on Shelf</source>
        <translation>Lägg på hyllan</translation>
    </message>
</context>
<context>
    <name>Saver</name>
    <message>
        <location filename="../qml/pages/Saver.qml" line="103"/>
        <source>Global Cupboard</source>
        <translation>Globala skåp</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="104"/>
        <source>Atelier</source>
        <translation type="unfinished">Laboratorium</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="104"/>
        <source>Ambience</source>
        <translation type="unfinished">Atmosfär</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="132"/>
        <source>Clean out this cupboard</source>
        <translation>Rensa skåpet</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="132"/>
        <source>Spring Clean</source>
        <translation>Vårstädning</translation>
    </message>
</context>
<context>
    <name>SaverPlus</name>
    <message>
        <source>anonymous</source>
        <translation type="vanished">Anonym</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="20"/>
        <source>User Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="110"/>
        <source>System Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="120"/>
        <source>Ambience Cupboard</source>
        <translation>Ambient Skåp</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="121"/>
        <source>Global Cupboard</source>
        <translation type="unfinished">Globala skåp</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="171"/>
        <source>Clean out this cupboard</source>
        <translation>Rensa skåpet</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="171"/>
        <source>Spring Clean</source>
        <translation>Vårstädning</translation>
    </message>
</context>
<context>
    <name>ShowRoom</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="obsolete">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="obsolete">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="obsolete">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="obsolete">Secondary Highlight Colour</translation>
    </message>
    <message>
        <source>Text Elements</source>
        <translation type="vanished">Textelement</translation>
    </message>
    <message>
        <source>UI Elements</source>
        <translation type="vanished">UI-element</translation>
    </message>
</context>
<context>
    <name>ShowRoomMini</name>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="25"/>
        <source>Mini</source>
        <comment>small showroom</comment>
        <translation>Mini</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="25"/>
        <source>Showroom</source>
        <translation>Utställningsrum</translation>
    </message>
</context>
<context>
    <name>ShowRoomText</name>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primär färg</translation>
    </message>
    <message>
        <source>A very long line showing Text in</source>
        <translation type="vanished">En mycket lång rad som visar text in</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Sekundär färg</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Markeringsfärg</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Sekundär markeringsfärg</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Text</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Markera bakgrundsfärg</translation>
    </message>
    <message>
        <source>Overlay Background Color</source>
        <translation type="vanished">Bakgrundsfärg för överlagring</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">Färgton för tonad markering</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="15"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="22"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="37"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="44"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="51"/>
        <source>%1 text</source>
        <comment>%1 is the name of a color</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="15"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="37"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="51"/>
        <source>Primary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="16"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="23"/>
        <source>No Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="22"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="44"/>
        <source>Secondary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="30"/>
        <source>Highlight Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="38"/>
        <source>Overlay Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="45"/>
        <source>Cover Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="52"/>
        <source>Wallpaper Overlay</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowRoomUI</name>
    <message>
        <source>Progress Bar Demo</source>
        <translation type="vanished">Progress Bar Demo</translation>
    </message>
    <message>
        <source>Remorse Item Demo</source>
        <translation type="vanished">Demo av Remorse Item</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="132"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="133"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="134"/>
        <source>MenuItem</source>
        <translation>MenyItem</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="134"/>
        <source>selected</source>
        <translation>valt</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="133"/>
        <source>disabled</source>
        <translation>inaktiverad</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="20"/>
        <source>Waiting…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="28"/>
        <source>Feeling Remorse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="108"/>
        <source>App</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="109"/>
        <source>Clicked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="110"/>
        <source>Dead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="154"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="155"/>
        <source>Button</source>
        <translation>Knapp</translation>
    </message>
</context>
<context>
    <name>TransparencyEditor</name>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="45"/>
        <source>Edit Transparency</source>
        <translation>Redigera transparens</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="53"/>
        <source>This will edit the alpha channel of the color. Note that in the Sailfish UI many elements use their own transparency values and are not affected by the alpha channel of the color. Remorse Timers are one such example.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="82"/>
        <source>Highlight Background Opacity</source>
        <translation>Framhäva bakgrundens opacitet</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="83"/>
        <source>This is used e.g. for Pulley Menu background.</source>
        <translation>Detta används t.ex. för bakgrunden till Pulley Menu.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="85"/>
        <source>Wallpaper Overlay Opacity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="87"/>
        <source>Cover Overlay Opacity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="88"/>
        <source>Transparency of application covers.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="94"/>
        <source>Color Alpha Channel</source>
        <translation>Färg Alpha-kanal</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="95"/>
        <source>Here you can edit Alpha channels for colors that have one.</source>
        <translation>Här kan du redigera Alpha-kanaler för färger som har en.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="96"/>
        <source>Secondary Color</source>
        <translation>Sekundär färg</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="obsolete">Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="98"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundär markeringsfärg</translation>
    </message>
</context>
</TS>
