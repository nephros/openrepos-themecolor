<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>AdvancedPage</name>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="22"/>
        <source>Advanced Options</source>
        <translation>Avanserte valg</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="31"/>
        <source>System Functions</source>
        <translation type="unfinished">Systemfunksjoner</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="34"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="40"/>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation type="unfinished">Start %1 på ny</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="62"/>
        <source>Lipstick</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <source>Home Screen</source>
        <translation type="unfinished">Hjemmeskjerm</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <source>Restart the %1 service. This will close all apps and relaunch the %2</source>
        <comment>arguments are &apos;Lipstick&apos; and &apos;Home Screen&apos;, one id the application name, one is the commonly used name for the same.</comment>
        <translation>Start %1-tjenesten på ny. Dette vil lukke alle programmer og starte %2 på nytt.</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="36"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="42"/>
        <source>Restarting</source>
        <translation type="unfinished">Starter på ny</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="41"/>
        <source>Restart the %1 service. This sometimes fixes color problems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="41"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="84"/>
        <source>Ambience</source>
        <translation>Omgivelse</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="54"/>
        <source>Config Management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="56"/>
        <source>Reset Config</source>
        <translation type="unfinished">Tilbakestill konfigurasjon</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="57"/>
        <source>Reset all color-related configuration values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="58"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="63"/>
        <source>Resetting</source>
        <translation type="unfinished">Tilbakestiller</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="61"/>
        <source>Reset Config and Restart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="62"/>
        <source>Reset all color-related configuration values and restart %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="69"/>
        <source>Advanced Editors</source>
        <translation type="unfinished">Avanserte redaktører</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="71"/>
        <source>Edit Global Blur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="72"/>
        <source>Edit blur parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="81"/>
        <source>Background Service</source>
        <translation>Bakgrunnstjeneste</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="83"/>
        <source>Set up daemon</source>
        <translation>Sett opp daemon</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="84"/>
        <source>Background service which watches for %1 changes and applies actions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="87"/>
        <source>Import/Export</source>
        <translation>Importer/eksporter</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="90"/>
        <source>Load Ambience File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="91"/>
        <source>Load a color scheme from a .ambience file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppInfoSingleton</name>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">DraktFarge</translation>
    </message>
    <message>
        <source>ThemeColor® RPM Builder™</source>
        <translation type="vanished">ThemeColor® RPM-bygger™</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="25"/>
        <source>ThemeColor</source>
        <comment>this will also be used for the app icon</comment>
        <translation>DraktFarge</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="26"/>
        <source>ThemeColor® RPM Builder™</source>
        <comment>this will also be used for the app icon</comment>
        <translation>DraktFarge® RPM-bygger™</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="38"/>
        <source>English</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Engelsk</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="35"/>
        <source>German</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Tysk</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="48"/>
        <source>Norwegian Bokmål</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Norwegian Bokmål</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="41"/>
        <source>Spanish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Spansk</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="45"/>
        <source>French</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Fransk</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="51"/>
        <source>Russian</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Russisk</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="54"/>
        <source>Swedish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Svensk</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="57"/>
        <source>Simplified Chinese</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Forenklet kinesisk</translation>
    </message>
</context>
<context>
    <name>BlurEditor</name>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="53"/>
        <source>Edit Blur Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="63"/>
        <source>These are some values found in Lipstick. They are exposed here for your experimentation, although it is unclear where exactly they are applied, and changing them seems to have very little effect. They are also not part of Ambiences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="67"/>
        <source>Blur Iterations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="77"/>
        <source>Blur Deviation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="87"/>
        <source>Blur Kernel</source>
        <translation>Kernel</translation>
    </message>
</context>
<context>
    <name>ColorField</name>
    <message>
        <source>specify RGB or aRGB value, e.g.</source>
        <translation type="vanished">angi en RGB- eller aRGB-verdi, f.eks.</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="13"/>
        <source>an %1 value</source>
        <comment>parameter is either RGB or aRGB</comment>
        <translation>en %1-verdi</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="14"/>
        <source>(the # is optional)</source>
        <translation>(# er valgfritt)</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="14"/>
        <source>e.g.</source>
        <translation>g.eks.</translation>
    </message>
</context>
<context>
    <name>ColorFilters</name>
    <message>
        <source>Tint</source>
        <translation type="obsolete">Fargelegging</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="21"/>
        <source>Red</source>
        <translation>Rød</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="21"/>
        <source>Tint %1</source>
        <translation>Fargetone %1</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="29"/>
        <source>Darken</source>
        <translation>Mørkne</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="37"/>
        <source>Brighten</source>
        <translation>Lysne</translation>
    </message>
</context>
<context>
    <name>ColorGenerators</name>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="14"/>
        <source>Randomizer</source>
        <translation>Tilfeldighetsgenerator</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="16"/>
        <source>Filters</source>
        <translation>Filtre</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="18"/>
        <source>Scheme Generators</source>
        <translation>Draktgeneratorer</translation>
    </message>
</context>
<context>
    <name>ColorRandomizer</name>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="20"/>
        <source>Random</source>
        <translation>Tilfeldig</translation>
    </message>
    <message>
        <source>Colors</source>
        <translation type="vanished">Farger</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="25"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="39"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="49"/>
        <source>Generated</source>
        <translation>Generert</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="30"/>
        <source>Bright</source>
        <translation>Lyst</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="30"/>
        <source>Dark</source>
        <translation>Mørk</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="44"/>
        <source>Gray</source>
        <translation>Grå</translation>
    </message>
</context>
<context>
    <name>ColorSchemeGenerator</name>
    <message>
        <source>Solarize</source>
        <translation type="vanished">Solarisert</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="17"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="32"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="39"/>
        <source>%1 Theme</source>
        <comment>parameter is name for a theme, e.g. night, day, random... you might just use the parameter and not translate theme at all.</comment>
        <translation>%1 Theme</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="17"/>
        <source>Solarize</source>
        <comment>as in &apos;solarized&apos; color scheme</comment>
        <translation>Solarisert</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="24"/>
        <source>Generate</source>
        <translation>Generer</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="24"/>
        <source>from</source>
        <translation>fra</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="24"/>
        <source>Highlight</source>
        <translation>Framhev</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="32"/>
        <source>Night</source>
        <translation>Natt</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="52"/>
        <source>%1 Color Blindness</source>
        <comment>%1 is replaced with the variant of color blindness, as in &apos;red-green color-blindness&apos;</comment>
        <translation>%1-fargeblindhet</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="52"/>
        <source>R/G</source>
        <comment>abbreviation for red-green color blindness</comment>
        <translation>R/G</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="52"/>
        <source>#%1/%2</source>
        <comment>for lists, as in &apos;number one of four&apos; is #1/4</comment>
        <translation>#%1/%2</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">Drakt</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="39"/>
        <source>Summer</source>
        <translation>Sommer</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="27"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="35"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="42"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="58"/>
        <source>Applying</source>
        <translation>Legger til</translation>
    </message>
</context>
<context>
    <name>ColorSelectors</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Secondary Highlight Colour</translation>
    </message>
</context>
<context>
    <name>ColorSlider</name>
    <message>
        <location filename="../qml/components/controls/ColorSlider.qml" line="66"/>
        <source>Adjust sliders, tap to reset</source>
        <translation>Juster glidebrytere, trykk for å tilbakestille</translation>
    </message>
</context>
<context>
    <name>ColorSliders</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="15"/>
        <source>Primary Color</source>
        <translation>Primærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="21"/>
        <source>Secondary Color</source>
        <translation>Sekundærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="28"/>
        <source>Highlight Color</source>
        <translation>Framhevingsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="35"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundær framhevingsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="42"/>
        <source>Background Highlight Color</source>
        <translation>Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="49"/>
        <source>Dim Highlight Color</source>
        <translation>Diffus framhevelsesfarge</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <source>Highlight Dimmer Color</source>
        <translation type="vanished">Framhev fordunklingsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="56"/>
        <source>Background Glow Color</source>
        <translation>Bakgrunnsglødefarge</translation>
    </message>
</context>
<context>
    <name>ColorSwapper</name>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="70"/>
        <source>Copy</source>
        <translation>Kopier</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="70"/>
        <source>Swap</source>
        <translation>Veksle</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="91"/>
        <location filename="../qml/components/ColorSwapper.qml" line="118"/>
        <source>Primary Color</source>
        <translation>Primærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="92"/>
        <location filename="../qml/components/ColorSwapper.qml" line="119"/>
        <source>Secondary Color</source>
        <translation>Sekundærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="93"/>
        <location filename="../qml/components/ColorSwapper.qml" line="120"/>
        <source>Highlight Color</source>
        <translation>Framhevelsesfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="94"/>
        <location filename="../qml/components/ColorSwapper.qml" line="121"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundær framhevelsesfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="95"/>
        <location filename="../qml/components/ColorSwapper.qml" line="122"/>
        <source>Background Highlight Color</source>
        <translation>Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="96"/>
        <location filename="../qml/components/ColorSwapper.qml" line="123"/>
        <source>Dim Highlight Color</source>
        <translation>Diffus framhevelsesfarge</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <source>Highlight Dimmer Color</source>
        <translation type="vanished">Framhev fordunklingsfarge</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Colour</translation>
    </message>
</context>
<context>
    <name>ColorTextInput</name>
    <message>
        <source>Color input</source>
        <translation type="vanished">Fargeinndata</translation>
    </message>
    <message>
        <source>Input value, tap to reset</source>
        <translation type="vanished">Inndataverdi, trykk for å tilbakestille</translation>
    </message>
</context>
<context>
    <name>ColorTextInputDialog</name>
    <message>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="16"/>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="32"/>
        <source>Color input</source>
        <translation>Fargeinndata</translation>
    </message>
</context>
<context>
    <name>ColorTextInputs</name>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primærfarge</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Sekundærfarge</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Framhevelsesfarge</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Sekundær framhevelsesfarge</translation>
    </message>
    <message>
        <source>Background Highlight Color</source>
        <translation type="vanished">Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">Diffus framhevelsesfarge</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <source>Highlight Dimmer Color</source>
        <translation type="vanished">Framhev fordunklingsfarge</translation>
    </message>
    <message>
        <source>Background Glow Color</source>
        <translation type="vanished">Bakgrunnsglødefarge</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">DraktFarge</translation>
    </message>
</context>
<context>
    <name>DaemonSettings</name>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="109"/>
        <source>Daemon Settings</source>
        <translation>Nisse-innstillinger</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="119"/>
        <source>Enable Watcher daemon</source>
        <translation>Skru på oppsynsnisse</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="114"/>
        <source>Watches for %1 changes and applies actions defined below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="120"/>
        <source>Daemon is </source>
        <translation>Nissen er </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="120"/>
        <source>active</source>
        <translation>påslått</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="120"/>
        <source>inactive</source>
        <translation>inaktiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="138"/>
        <source>Actions</source>
        <translation>Handlinger</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="144"/>
        <source>Select what happens when an %1 change is detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="149"/>
        <source>launch %1</source>
        <translation>start %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="150"/>
        <source>just opens the app</source>
        <translation>åpner kun programmet</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="160"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="162"/>
        <source>Warning:</source>
        <translation>Advarsel:</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="161"/>
        <source>The features below are experimental and do not work reliably.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="169"/>
        <source>apply Top theme</source>
        <translation>ifør topp-drakt</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="170"/>
        <source>applies theme from the top %1 shelf</source>
        <translation>ifører drakt fra øverste %1-hylle</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="177"/>
        <source>apply Night theme</source>
        <translation>ifør nattdrakt</translation>
    </message>
    <message>
        <source>ThemeColor</source>
        <translation type="obsolete">DraktFarge</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="114"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="144"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="170"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="178"/>
        <source>Ambience</source>
        <translation>Omgivelse</translation>
    </message>
    <message>
        <source>The features below are a developer preview and do not work.</source>
        <translation type="vanished">Funksjonene nedenfor er en utviklerforhåndsvisning og vil ikke virke.</translation>
    </message>
    <message>
        <source>You can configure them here, but the daemon does not support them yet.</source>
        <translation type="vanished">Du kan sette dem opp her, men nissen støtter dem ikke enda.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="178"/>
        <source>taken from the second %1 shelf</source>
        <translation>tatt fra andre %1-hylle</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="191"/>
        <source>Night begins:</source>
        <translation>Natt begynner:</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="205"/>
        <source>Night ends:</source>
        <translation>Natt slutter:</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <source>Adjust Theme Colors</source>
        <translation type="vanished">Juster draktfarger</translation>
    </message>
    <message>
        <source>Current Color Model:</source>
        <translation type="vanished">Current Colour Model:</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Secondary Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="29"/>
        <source>Showroom</source>
        <translation>Visningsrom</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="29"/>
        <source>Atelier</source>
        <translation type="unfinished">Laboratorium</translation>
    </message>
    <message>
        <source>Input Mode:</source>
        <translation type="vanished">Inndatamodus:</translation>
    </message>
    <message>
        <source>Tap to switch</source>
        <translation type="vanished">Trykk for å bytte</translation>
    </message>
    <message>
        <source>Advanced…</source>
        <translation type="vanished">Avansert…</translation>
    </message>
    <message>
        <source>gray</source>
        <translation type="vanished">grey</translation>
    </message>
    <message>
        <source>Apply colors to system</source>
        <translation type="vanished">Apply colours to system</translation>
    </message>
    <message>
        <source>Reload colors from system</source>
        <translation type="vanished">Reload colors from system</translation>
    </message>
    <message>
        <source>Swapper/Copier</source>
        <translation type="vanished">Veksler/kopierer</translation>
    </message>
    <message>
        <source>Compute all Colors from Highlight</source>
        <translation type="vanished">Regn ut alle farger fra framheving</translation>
    </message>
    <message>
        <source>Applying</source>
        <translation type="vanished">Legger til</translation>
    </message>
    <message>
        <source>Apply Colors to System</source>
        <translation type="vanished">Bruk farger for systemet</translation>
    </message>
    <message>
        <source>Set up daemon</source>
        <translation type="vanished">Sett opp nisse</translation>
    </message>
    <message>
        <source>Reload Colors from System</source>
        <translation type="vanished">Reload Colours from System</translation>
    </message>
    <message>
        <source>Generators</source>
        <translation type="vanished">Generatorer</translation>
    </message>
    <message>
        <source>Reload Colors from current Theme</source>
        <translation type="vanished">Gjeninnlast farger fra nåværende drakt</translation>
    </message>
    <message>
        <source>Reload Colors from System Config</source>
        <translation type="vanished">Gjeninnlast farger fra systemoppsett</translation>
    </message>
    <message>
        <source>Experimental or dangerous actions</source>
        <translation type="vanished">Eksperimentelle eller farlige handlinger</translation>
    </message>
    <message>
        <source>Export to Ambience file</source>
        <translation type="vanished">Eksporter til omgivelsesfil</translation>
    </message>
    <message>
        <source>Export to Ambience package</source>
        <translation type="vanished">Exporter til Ambience-pakke</translation>
    </message>
    <message>
        <source>Edit Transparency</source>
        <translation type="vanished">Rediger gjennomsiktighet</translation>
    </message>
    <message>
        <source>(not implemented)</source>
        <translation type="vanished">(ikke implementert)</translation>
    </message>
    <message>
        <source>Save Theme to current Ambience</source>
        <translation type="vanished">Lagre drakt til nåværende omgivelse</translation>
    </message>
    <message>
        <source>Saving</source>
        <translation type="vanished">Lagrer</translation>
    </message>
    <message>
        <source>Resetting</source>
        <translation type="vanished">Tilbakestiller</translation>
    </message>
    <message>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation type="vanished">Start %1 på ny</translation>
    </message>
    <message>
        <source>Restarting</source>
        <translation type="vanished">Starter på ny</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation type="vanished">Saving…</translation>
    </message>
    <message>
        <source>Reset all values and restart</source>
        <translation type="vanished">Tilbakestill alle verdier og start på ny</translation>
    </message>
    <message>
        <source>Reset nonstandard values</source>
        <translation type="vanished">Tilbakestill alle ikke-standardverdier</translation>
    </message>
    <message>
        <source>Restart Lipstick</source>
        <translation type="vanished">Start Lipstick på ny</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Hjelp</translation>
    </message>
    <message>
        <source>Sliders</source>
        <translation type="vanished">Glidebrytere</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Tekst</translation>
    </message>
    <message>
        <source>Swapper</source>
        <translation type="vanished">Swapper</translation>
    </message>
    <message>
        <source>Randomizer</source>
        <translation type="vanished">Tilfeldighetsgenerator</translation>
    </message>
    <message>
        <source>Jolla Original</source>
        <translation type="vanished">Opprinnelig Jolla</translation>
    </message>
    <message>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation type="vanished">Håndbok</translation>
    </message>
    <message>
        <source>Open Ambience Settings</source>
        <translation type="vanished">Åpne omgivelsesinnstillinger</translation>
    </message>
    <message>
        <source>Apply Colors to current Theme</source>
        <translation type="vanished">Apply colours to current Theme</translation>
    </message>
    <message>
        <source>Reload Colors</source>
        <translation type="vanished">Reload Colours</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Reset Colors</source>
        <translation type="vanished">Reset Colours</translation>
    </message>
</context>
<context>
    <name>HelpColors</name>
    <message>
        <source>The four basic Colors</source>
        <translation type="vanished">De fire grunnleggende fargene</translation>
    </message>
    <message>
        <source>The four basic Colors</source>
        <comment>Help Section</comment>
        <translation type="vanished">De fire grunnleggende fargene</translation>
    </message>
    <message>
        <source>secondary highlight, derived through some magic from the highlight color.</source>
        <translation type="vanished">sekundær framheving, utledet via litt magi fra framhevingsfargen.</translation>
    </message>
    <message>
        <source>Additional Colors</source>
        <comment>Help Section</comment>
        <translation type="vanished">Ekstra farger</translation>
    </message>
    <message>
        <source>Additional Colors</source>
        <translation type="vanished">Ekstra farger</translation>
    </message>
</context>
<context>
    <name>HelpCredits</name>
    <message>
        <source>About</source>
        <translation type="vanished">Om</translation>
    </message>
    <message>
        <source>About</source>
        <comment>Help Section</comment>
        <translation type="vanished">Om</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">Versjon:</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation type="vanished">Opphavsrett:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Lisens:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation type="vanished">Kildekode:</translation>
    </message>
    <message>
        <source>Translations</source>
        <translation type="vanished">Oversettelser</translation>
    </message>
</context>
<context>
    <name>HelpDaemon</name>
    <message>
        <source>Ambience</source>
        <translation type="vanished">Omgivelse</translation>
    </message>
    <message>
        <source>&lt;p&gt;The Watcher Daemon is a background service which can take care of some tasks while %1 is not running. Its main purpose is to apply color schemes after the %2 has changed.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Oppsynsnissen er en bakgrunnstjeneste som håndterer noen oppgaver mens %1 ikke kjører. Hovedformålet er å ta i bruk fargedrakter etter at %2 har endret seg.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;It can be configured by tapping the &lt;i&gt;Daemon Settings&lt;/i&gt; button on the Advanced page.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Kan settes opp ved å trykke &lt;i&gt;Nisse-innstillinger&lt;/i&gt;-knappen på «Avansert»-siden.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;This is an ever-evolving (and experimental) feature with more functions added each release.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Dette er en eksperimentell funksjon i konstant utvikling der flere funksjoner legges til ved hver utgivelse.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;One upcoming feature is to apply &amp;quot;Night Mode&amp;quot; color schemes at the appropriate time.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;En kommende funksjon er at &amp;quot;Nattmodus&amp;quot; iføres til rett tid.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>HelpExport</name>
    <message>
        <source>Exporting Ambiences</source>
        <translation type="vanished">Eksportere atmosfærer</translation>
    </message>
    <message>
        <source>Exporting Ambiences</source>
        <comment>Help Section</comment>
        <translation type="vanished">Eksportere atmosfærer</translation>
    </message>
    <message>
        <source>Export to Ambience package</source>
        <translation type="vanished">Exporter til Ambience-pakke</translation>
    </message>
    <message>
        <source>The Builder</source>
        <comment>Help Section</comment>
        <translation type="vanished">Byggmesteren</translation>
    </message>
    <message>
        <source>Notes</source>
        <comment>Help Section</comment>
        <extracomment>as in something to add, not a small piece of paper</extracomment>
        <translation type="vanished">Merknader</translation>
    </message>
    <message>
        <source>The Builder</source>
        <translation type="obsolete">Byggmesteren</translation>
    </message>
</context>
<context>
    <name>HelpGeneral</name>
    <message>
        <source>General</source>
        <translation type="vanished">Generelt</translation>
    </message>
    <message>
        <source>General</source>
        <comment>Help Section</comment>
        <translation type="vanished">Generelt</translation>
    </message>
    <message>
        <source>Basic Workflow</source>
        <comment>Help Section</comment>
        <translation type="vanished">Grunnleggende arbeidsflyt</translation>
    </message>
    <message>
        <source>Basic Workflow</source>
        <translation type="vanished">Grunnleggende arbeidsflyt</translation>
    </message>
</context>
<context>
    <name>HelpHow</name>
    <message>
        <source>Loading and Applying Colors</source>
        <translation type="vanished">Laste inn og bruke farger</translation>
    </message>
    <message>
        <source>App Configuration and State</source>
        <translation type="vanished">Appkonfigurasjon og tilstand</translation>
    </message>
    <message>
        <source>Loading and Applying Colors</source>
        <comment>Help Section</comment>
        <translation type="vanished">Laste inn og bruke farger</translation>
    </message>
    <message>
        <source>App Configuration and State</source>
        <comment>Help Section</comment>
        <translation type="vanished">Appkonfigurasjon og tilstand</translation>
    </message>
    <message>
        <source>The Builder</source>
        <comment>Help Section</comment>
        <translation type="vanished">Byggmesteren</translation>
    </message>
    <message>
        <source>The Builder</source>
        <translation type="vanished">Byggmesteren</translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <source>Use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            When satisfied, tap the area above the slider to set the color.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</source>
        <translation type="vanished">Use the Sliders in the lower section to adjust the colours.&lt;br /&gt;            When satisfied, tap the area above the slider to set the colour.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</translation>
    </message>
    <message>
        <source>How to Use</source>
        <translation type="vanished">Anvendelse</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="vanished">Generelt</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colors can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor, as these are the ones saved by the system.          There are other colors in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colours can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor (sic!), as these are the ones saved by the system.          There are other colours in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only some colors can be edited.          There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited. There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <translation type="vanished">Visningsrommet</translation>
    </message>
    <message>
        <source>The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colors that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </source>
        <translation type="vanished">The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colours that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </translation>
    </message>
    <message>
        <source>The Atelier</source>
        <translation type="vanished">Laboratoriet</translation>
    </message>
    <message>
        <source>In Slider Input Mode, use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            In Text Input Mode, you can enter color values directly.&lt;br /&gt;
            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.
          </source>
        <translation type="vanished">In Slider Input Mode, use the Sliders in the lower section to adjust the colours.&lt;br /&gt;            In Text Input Mode, you can enter colour values directly.&lt;br /&gt;
            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.
          </translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.            In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know.&lt;br /&gt;             Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullDown Menu.
          </source>
        <translation type="vanished">In Slider input mode, use the Sliders in the lower section to adjust the colours. In Text input mode, you can enter colour values directly. Randomizer does what it sais, and Jolla Original you already know.&lt;br /&gt;Check what your theme will look like in the Showroom display.&lt;br /&gt; &lt;br /&gt; When you&apos;re done, use the PullDown Menu.
          </translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <translation type="vanished">Skapet</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.             There is one global Cupboard, and one specific for the current ambience.&lt;br /&gt;             Note that only system-wide Ambiences have a name, custon ones will show as anonymous (for now)</source>
        <translation type="vanished">This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt; Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</translation>
    </message>
    <message>
        <source>Tips and Caveats</source>
        <translation type="vanished">Tips og triks</translation>
    </message>
    <message>
        <source>It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
          It&apos;s a good idea to store a known-good color sceme in the Cupboard so you can go restore easily.&lt;br /&gt;          &lt;br /&gt;          If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;          &lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;          &lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;          and repeat for all the other colors stored there. &lt;br /&gt;          Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;&lt;br /&gt;If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;and repeat for all the other colours stored there. &lt;br /&gt;Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, 
nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited.
There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited.
There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>The top area on the first page (&quot;Showroom&quot;) is non-interactive and just shows the colors that are selected currently.&lt;br /&gt;Here you can preview your creation.</source>
        <translation type="vanished">Toppområdet på første side («Visningsrommet») er ikke interaktivt, og viser kun fargene som er valgt.&lt;br /&gt;Her kan du bivåne din kreasjon.</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.  In Text input mode, you can enter color values directly. Randomizer
does what it sais, and Jolla Original you already know.  Swapper lets you change color definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="vanished">In Slider input mode, use the Sliders in the lower section to adjust the colours. In Text input mode, you can enter colour values directly. Randomizer
does what it sais, and Jolla Original you already know. Swapper lets you change colour definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colours to the current session.</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="vanished">This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, 
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. 
This means once applied through the app, they will always stay the same until you change them again in the App. 
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard.
If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla Ambiences only define four colours, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added,
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colours than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there.
This means once applied through the app, they will always stay the same until you change them again in the App.
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colours when switching editing modes, applying colours to system, or taking palettes from the Cupboard.
If that happens, try reloading the colours from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colours somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colours stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited. There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;
&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="vanished">Programmet lar deg endre fargepaletten på Lipstick. Det vil ikke (enda) opprette eller endre nye omgivelser, ei heller overlever det endring av omgivelse, omstart av Lipstick, eller omstart av enheten.&lt;br /&gt;
Kun noen farger kan redigeres. Det er andre farger i bruk av systemet som automatisk utregnes fra de grunnleggende fire, og de kan ikke endres.&lt;br/&gt;
&lt;br/&gt;
Det jobbes med å overkomme disse hindrene.</translation>
    </message>
    <message>
        <source>The top area on the first page (&quot;Showroom&quot;) shows the colors that are selected currently using various UI elements.&lt;br /&gt;Here you can preview your creation.&lt;br /&gt;Tapping on either area will hide it. To unhide, tap the title header.</source>
        <translation type="vanished">Toppområdet på første side («Visningsrommet») er ikke interaktivt, og viser kun fargene som er valgt.&lt;br /&gt;Her kan du bivåne din kreasjon.&lt;br /&gt;Å trykke på et område skjuler det. For å vise det igjen kan du trykke på tittelfeltet.</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors. In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know. Swapper lets you change color definitions.&lt;br /&gt;
Check what your theme will look like in the Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="vanished">I glidebryterinngangsmodus, kan du bruke dem for å senke en del for justering av fargene. I tekstinndatamodus kan du skrive inn fargeverdier direkte. Tilfeldighetsgeneratoren gjør det den sier, og den opprinnelige Jolla-oppførselen er det den ble laget som. Veksleren lar deg endre fargedefinisjoner.&lt;br /&gt;
Sjekk hvordan din drakt vil se ut i visningsrommet.&lt;br /&gt;
&lt;br /&gt;
Når du er ferdig kan du bruke nedtrekksmenyen for å bruke fargene i nåværende økt.</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="vanished">Dette området lar deg lagre dine opprettede fargepaletter for gjenbruk senere. Det er et skap for hele systemet, og ett som er spesifikt for omgivelse.&lt;br /&gt;
Merk at kun omgivelser for hele systemet har et navn. Egendefinerte vil vises som anonyme (for øyeblikket).</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is primary  a little less opaque primary, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla-omgivelser definerer kun fire farger. «Primær» (som er svart eller hvit avhengig av omgivelsesinnstilling), «Sekundær», har litt mindre dekkevne med litt lagt til, pluss «Framhevet» og «Sekundær framhevelsesfarge» som er de valgt i omgivelsesinnstillingene.
&lt;br /&gt;
Programmet lar deg redigere andre farger enn disse fire, men de blir ikke påvirket av endringer i omgivelse eller Lipstick-omstarter, siden de lagres i dconf-databasen og blir der. Dette betyr at når de er lagt til gjennom programmet, vil de alltid forbli de samme til du endrer dem igjen i programmet. Du kan tilbakestille valget nedenfor for å bli kvitt dem hvis nødvendig.
&lt;br /&gt;
Programmet blir ofte forvirret om farger ved veksling eller redigering av modus, tillegging av farger i systemet, eller å ta fargepaletter fra skapet. Hvis det skjer kan du prøve å gjeninnlaste fargene fra systemet. Det hjelper som oftest.&lt;br /&gt;
&lt;br /&gt;
Det er mulig å skape fargepaletter som gjør deler av grensesnittet uleselig. Sjekk spesielt ikke-åpenbare områder som det virtuelle tastaturet.&lt;br /&gt;
Det er en god idé å lagre farger du vet fungerer i skapet slik at du kan gjenopprette dem enkelt.&lt;br /&gt;
&lt;br /&gt;
Hvis du har tullet til fargene på noe vis, kan du bruke dragingsmenyen for å tilbakestille alt, eller bruke kommandolinjen:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
og gjenta for alle de andre fargene lagret der.&lt;br /&gt;
Endring av omgivelse fra systeminnstillingene kan også hjelpe.</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla-omgivelser definerer kun fire farger. «Primær» (som er svart eller hvit avhengig av omgivelsesinnstilling), «Sekundær», har litt mindre dekkevne med litt lagt til, pluss «Framhevet» og «Sekundær framhevelsesfarge» som er de valgt i omgivelsesinnstillingene.
&lt;br /&gt;
Programmet lar deg redigere andre farger enn disse fire, men de blir ikke påvirket av endringer i omgivelse eller Lipstick-omstarter, siden de lagres i dconf-databasen og blir der. Dette betyr at når de er lagt til gjennom programmet, vil de alltid forbli de samme til du endrer dem igjen i programmet. Du kan tilbakestille valget nedenfor for å bli kvitt dem hvis nødvendig.
&lt;br /&gt;
Programmet blir ofte forvirret om farger ved veksling eller redigering av modus, tillegging av farger i systemet, eller å ta fargepaletter fra skapet. Hvis det skjer kan du prøve å gjeninnlaste fargene fra systemet. Det hjelper som oftest.&lt;br /&gt;
&lt;br /&gt;
Det er mulig å skape fargepaletter som gjør deler av grensesnittet uleselig. Sjekk spesielt ikke-åpenbare områder som det virtuelle tastaturet.&lt;br /&gt;
Det er en god idé å lagre farger du vet fungerer i skapet slik at du kan gjenopprette dem enkelt.&lt;br /&gt;
&lt;br /&gt;
Hvis du har tullet til fargene på noe vis, kan du bruke dragingsmenyen for å tilbakestille alt, eller bruke kommandolinjen:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
og gjenta for alle de andre fargene lagret der.&lt;br /&gt;
Endring av omgivelse fra systeminnstillingene kan også hjelpe.</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">Versjon:</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation type="vanished">Opphavsrett:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Lisens:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation type="vanished">Kildekode:</translation>
    </message>
    <message>
        <source>Translations:</source>
        <translation type="vanished">Oversettelser:</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App.  You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla-omgivelser definerer kun fire farger. «Primær» (som er svart eller hvit avhengig av omgivelsesinnstilling), «Sekundær», har litt mindre dekkevne med litt lagt til, pluss «Framhevet» og «Sekundær framhevelsesfarge» som er de valgt i omgivelsesinnstillingene.
&lt;br /&gt;
Programmet lar deg redigere andre farger enn disse fire, men de blir ikke påvirket av endringer i omgivelse eller Lipstick-omstarter, siden de lagres i dconf-databasen og blir der. Dette betyr at når de er lagt til gjennom programmet, vil de alltid forbli de samme til du endrer dem igjen i programmet. Du kan tilbakestille valget nedenfor for å bli kvitt dem hvis nødvendig.
&lt;br /&gt;
Programmet blir ofte forvirret om farger ved veksling eller redigering av modus, tillegging av farger i systemet, eller å ta fargepaletter fra skapet. Hvis det skjer kan du prøve å gjeninnlaste fargene fra systemet. Det hjelper som oftest.&lt;br /&gt;
&lt;br /&gt;
Det er mulig å skape fargepaletter som gjør deler av grensesnittet uleselig. Sjekk spesielt ikke-åpenbare områder som det virtuelle tastaturet.&lt;br /&gt;
Det er en god idé å lagre farger du vet fungerer i skapet slik at du kan gjenopprette dem enkelt.&lt;br /&gt;
&lt;br /&gt;
Hvis du har tullet til fargene på noe vis, kan du bruke dragingsmenyen for å tilbakestille alt, eller bruke kommandolinjen:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
og gjenta for alle de andre fargene lagret der.&lt;br /&gt;
Endring av omgivelse fra systeminnstillingene kan også hjelpe.</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">Om</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">Versjon: </translation>
    </message>
    <message>
        <source>Copyright: </source>
        <translation type="vanished">Opphavsrett: </translation>
    </message>
    <message>
        <source>License: </source>
        <translation type="vanished">Lisens: </translation>
    </message>
    <message>
        <source>Source Code: </source>
        <translation type="vanished">Kildekode: </translation>
    </message>
    <message>
        <source>General Information</source>
        <translation type="vanished">Generell informasjon</translation>
    </message>
    <message>
        <source>About the UI</source>
        <translation type="vanished">Om brukergrensesnittet</translation>
    </message>
    <message>
        <source>About Colors</source>
        <translation type="vanished">Om farger</translation>
    </message>
    <message>
        <source>Exporting Ambiences</source>
        <translation type="vanished">Eksportere atmosfærer</translation>
    </message>
    <message>
        <source>Limitations, Tips etc.</source>
        <translation type="vanished">Begrensninger, tips osv.</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="vanished">Bidragsytere</translation>
    </message>
    <message>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation type="vanished">Håndbok</translation>
    </message>
    <message>
        <source>Chapter %1:</source>
        <comment>chapter in the handbook. argument is a number</comment>
        <translation type="vanished">Kapittel %1:</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="65"/>
        <source>Select a chapter</source>
        <translation type="unfinished">Velg et kapittel</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="66"/>
        <source>Tap to switch</source>
        <translation type="unfinished">Trykk for å bytte</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="73"/>
        <source>General Information</source>
        <comment>Help Index</comment>
        <translation>Generell informasjon</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="74"/>
        <source>About the UI</source>
        <comment>Help Index</comment>
        <translation>Om brukergrensesnittet</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="75"/>
        <source>About Colors</source>
        <comment>Help Index</comment>
        <translation>Om farger</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="76"/>
        <source>Exporting Ambiences</source>
        <comment>Help Index</comment>
        <translation>Eksportere atmosfærer</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="77"/>
        <source>Watcher Daemon</source>
        <comment>Help Index</comment>
        <translation>Oppsynsnisse</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="78"/>
        <source>How it works</source>
        <comment>Help Index</comment>
        <translation>Hvordan det virker</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="79"/>
        <source>Limitations , Tips etc.</source>
        <comment>Help Index</comment>
        <translation>Begrensninger, tips osv.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="102"/>
        <source>View %1</source>
        <comment>User Manual display language</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="102"/>
        <source>English</source>
        <translation>Engelsk</translation>
    </message>
    <message>
        <source>Credits</source>
        <comment>Help Index</comment>
        <translation type="vanished">Bidragsytere</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="59"/>
        <source>Handbook</source>
        <comment>Help Index</comment>
        <extracomment>User Manual, Application Manual, Handbook, Howto</extracomment>
        <translation>Håndbok</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="65"/>
        <source>Chapter %1:</source>
        <comment>Help Index Chapter Prefix</comment>
        <translation>Kapittel %1:</translation>
    </message>
</context>
<context>
    <name>HelpTips</name>
    <message>
        <source>Tips and Caveats</source>
        <translation type="vanished">Tips og triks</translation>
    </message>
    <message>
        <source>Tips and Caveats</source>
        <comment>Help Section</comment>
        <translation type="vanished">Tips og triks</translation>
    </message>
</context>
<context>
    <name>HelpUIMain</name>
    <message>
        <source>The Showroom</source>
        <translation type="vanished">Visningsrommet</translation>
    </message>
    <message>
        <source>The Atelier</source>
        <translation type="vanished">Laboratoriet</translation>
    </message>
    <message>
        <source>&lt;p&gt;The main UI consists of the following:
       &lt;ul&gt;
       &lt;li&gt;The Showroom&lt;/li&gt;
       &lt;li&gt;The Atelier&lt;/li&gt;
       &lt;li&gt;The Cupboards&lt;/li&gt;
       &lt;li&gt;PullDown Menu&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Hovedgrensesnittet består av følgende:
       &lt;ul&gt;
       &lt;li&gt;Visningsrommet&lt;/li&gt;
       &lt;li&gt;Laboratoriet&lt;/li&gt;
       &lt;li&gt;Skapene&lt;/li&gt;
       &lt;li&gt;Nedtrekksmenyen&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <comment>Help Section</comment>
        <translation type="vanished">Visningsrommet</translation>
    </message>
    <message>
        <source>The Atelier</source>
        <comment>Help Section</comment>
        <translation type="vanished">Laboratoriet</translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <comment>Help Section</comment>
        <translation type="vanished">Skapet</translation>
    </message>
    <message>
        <source>Put on Shelf</source>
        <translation type="vanished">Putt på hyllen</translation>
    </message>
    <message>
        <source>Take to Lab</source>
        <translation type="vanished">Ta til laboratoriet</translation>
    </message>
    <message>
        <source>Menus</source>
        <comment>Help Section</comment>
        <translation type="vanished">Menyer</translation>
    </message>
    <message>
        <source>&lt;p&gt;
       The PullDown menu contains the most used commands.
       &lt;p&gt;
       &lt;ul&gt;
       &lt;li&gt;Apply: The central command of the app. This will apply the current color theme to the system.&lt;/li&gt;
       &lt;li&gt;Reload: Loads the theme that is currently in use in the system.&lt;/li&gt;
       &lt;li&gt;Export: opens the Export Ambience page. See the &lt;b&gt;Export&lt;/b&gt; chapter for more.&lt;/li&gt;
       &lt;li&gt;Advanced: takes you to a page containing various lesser used, experimental or destructive commands.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;
       Nedtrekksmenyen inneholder hyppigst brukte kommandoer.
       &lt;p&gt;
       &lt;ul&gt;
       &lt;li&gt;Bruk: Den sentrale kommandoen i programmet. Dette ifører fargedrakten for systemet.&lt;/li&gt;
       &lt;li&gt;Gjeninnlast. Laster inn iført drakt igjen.&lt;/li&gt;
       &lt;li&gt;Eksporter: Åpner «Eksporter omgivelse»-siden. Sjekk &lt;b&gt;Eksport&lt;/b&gt;-kapittelet for mer info.&lt;/li&gt;
       &lt;li&gt;Avansert. Tar deg til en side som inneholder mindre brukte, eksperimentelle eller destruktive kommandoer.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;</translation>
    </message>
    <message>
        <source>Menus</source>
        <translation type="vanished">Menyer</translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <translation type="vanished">Skapet</translation>
    </message>
</context>
<context>
    <name>LabPage</name>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="38"/>
        <source>Atelier</source>
        <translation>Atelier</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="38"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="38"/>
        <source>Cupboards</source>
        <translation type="unfinished">Skap</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="43"/>
        <source>Current Palette</source>
        <translation type="unfinished">Nåværende palett</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="48"/>
        <source>Editor</source>
        <comment>tool/editing mode</comment>
        <translation type="unfinished">Redaktør</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="49"/>
        <source>Tap to switch</source>
        <translation type="unfinished">Trykk for å bytte</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="57"/>
        <source>Sliders</source>
        <translation type="unfinished">Glidebrytere</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="58"/>
        <source>Swapper/Copier</source>
        <translation type="unfinished">Veksler/kopierer</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="59"/>
        <source>Generators</source>
        <translation type="unfinished">Generatorer</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="60"/>
        <source>Jolla Original</source>
        <translation type="unfinished">Opprinnelig Jolla</translation>
    </message>
</context>
<context>
    <name>LoadAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="36"/>
        <source>Load</source>
        <translation>Last inn</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="97"/>
        <source>Load Theme</source>
        <translation>Last Tema</translation>
    </message>
    <message>
        <source>Take to Lab</source>
        <translation type="vanished">Ta til laboratoriet</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="97"/>
        <source>Take to Atelier</source>
        <translation>Ta til Atelier</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="115"/>
        <source>Here you can load a .ambience (JSON) file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="119"/>
        <source>current</source>
        <translation>gjeldende</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="125"/>
        <source>file data</source>
        <translation>Fildata</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="131"/>
        <source>File name </source>
        <translation>Filnavn </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="133"/>
        <source>Click to select</source>
        <translation>Klikk for å velge</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="140"/>
        <source>File content</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="149"/>
        <source>No file loaded</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LootBoxItem</name>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="27"/>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="32"/>
        <source>ThemeColor</source>
        <translation>DraktFarge</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="31"/>
        <source>A Lootbox was delivered!</source>
        <translation>En utbyttekasse ble levert.</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="32"/>
        <source>now has more shelves!</source>
        <translation>har nå flere hyller.</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="32"/>
        <source>Your persistence has been rewarded.</source>
        <translation>Din standhaftighet har blitt belønnet.</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="33"/>
        <source>Your persistence has been rewarded!</source>
        <translation>Din standhaftighet har blitt belønnet!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="43"/>
        <source>Purchase Options</source>
        <translation>Kjøpsalternativer</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="52"/>
        <source>Payment* received!</source>
        <translation>Betaling* mottatt,</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="52"/>
        <source>Buy more shelves</source>
        <translation>Kjøp flere hyller</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="57"/>
        <source>Using Jolla Shop credentials to buy Storage Lootbox</source>
        <translation>Bruk identitetsdetaljene fra Jolla-butikken for å kjøpe lagringsutbyttekasse</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="74"/>
        <source>Thank you for your purchase!&lt;br /&gt;Your extra shelves will be delivered in the next update!</source>
        <translation>Takk for kjøpet.&lt;br /&gt;Dine ekstra hyller vil bli levert i neste oppdatering.</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="88"/>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;…or did it?</source>
        <translation>*) Neida, det er ingen kjøpselementer i dette programmet eller utbyttekasser i Jolla-butikken. Det skulle tatt seg ut.&lt;br /&gt;Ingen penger ble oveført.&lt;br /&gt;Det skjedde faktisk ingenting nå.&lt;br /&gt;…eller?</translation>
    </message>
    <message>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</source>
        <translation type="vanished">*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="9"/>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation>Håndbok</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="12"/>
        <source>Advanced…</source>
        <translation>Avansert…</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="15"/>
        <source>Export to Ambience package</source>
        <translation type="unfinished">Exporter til Ambience-pakke</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="18"/>
        <source>Apply Colors to System</source>
        <translation type="unfinished">Bruk farger for systemet</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="19"/>
        <source>Applying</source>
        <translation type="unfinished">Legger til</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="21"/>
        <source>Reload Colors from System</source>
        <translation>Gjeninnlast farger fra system</translation>
    </message>
</context>
<context>
    <name>SaveAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="53"/>
        <source>Export Functions</source>
        <translation>Eksportfunksjoner</translation>
    </message>
    <message>
        <source>Here you can export your creation to a .ambience (json) file.&lt;br /&gt;
                      This file can be picked up by the ThemeColor® RPM Builder™ companion tool which will package the whole thing as an RPM which you can then install.
                     </source>
        <translation type="vanished">Her kan du eksportere din kreasjon til en .ambience (JSON)-fil.&lt;br /&gt;
                      Denne filen vil bli plukket opp av ThemeColor® RPM-bygger™-tilleggsverktøyet, som vil pakke det hele som en RPM-pakke du kan installere.
                     </translation>
    </message>
    <message>
        <source>Here you can export your creation to an .ambience (JSON) file.&lt;br /&gt;
                      This file can be picked up by the ThemeColor® RPM Builder™ companion tool, which will package the whole thing as an RPM you can then install.
                     </source>
        <translation type="vanished">Her kan du eksportere din kreasjon til en .ambience (JSON)-fil.&lt;br /&gt;
                      Denne filen vil bli plukket opp av ThemeColor® RPM-bygger™-tilleggsverktøyet, som vil pakke det hele som en RPM-pakke du kan installere.
                     </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="67"/>
        <source>Here you can export your creation to a .ambience (JSON) file, create an installable package from it, and optionally install that on your device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="69"/>
        <source>Ambience</source>
        <translation>Omgivelse</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="69"/>
        <source>First, enter a name for your %1. You can edit the file by clicking the name, and generate a package from it using the Builder Button.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="71"/>
        <source>The .ambience file as well as any built packages will be stored in the %1 folder. (%2)</source>
        <comment>first argument is the short name of the location, second argument is the full path</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="89"/>
        <source>Export to File</source>
        <translation>Eksporter til fil</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="94"/>
        <source>Ambience Name</source>
        <translation>Ambience-navn</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="96"/>
        <source>A cool Ambience Name</source>
        <translation>Et snedig Ambience-navn</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="191"/>
        <source>Click to Open</source>
        <translation>Klikk for å åpne</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="202"/>
        <source>Build Package</source>
        <translation>Byggepakke</translation>
    </message>
    <message>
        <source>Click to export</source>
        <translation type="vanished">Klikk for å eksportere</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="192"/>
        <source>File Name</source>
        <translation>Filnavn</translation>
    </message>
    <message>
        <source>Open File</source>
        <translation type="vanished">Åpne fil</translation>
    </message>
    <message>
        <source>Launch Builder</source>
        <translation type="vanished">Start byggherre</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="74"/>
        <source>Disclaimer</source>
        <translation>Ansvarsfraskrivelse</translation>
    </message>
    <message>
        <source>Ambience Package</source>
        <translation type="obsolete">Atmosfærepakke</translation>
    </message>
    <message>
        <source>Both the .ambience file as well as any built packages will be stored in the %1 folder. (%2)</source>
        <comment>first argument is the short name of the locatin, second argument is the full path</comment>
        <translation type="vanished">Både .ambience.filen og alle innebygde pakker lagres i %1-mappen. (%2)</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="71"/>
        <source>Documents</source>
        <translation>Dokumenter</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="86"/>
        <source>If you decide to share or otherwise distribute the package make sure you respect copy- and other rights for any content included.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Hjelp</translation>
    </message>
</context>
<context>
    <name>SaveSlot</name>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="45"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="46"/>
        <source>Ambience</source>
        <translation>Omgivelse</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="45"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="46"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="48"/>
        <source>Shelf</source>
        <translation>Hylle</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <source>default</source>
        <translation>standard</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="45"/>
        <source>night mode</source>
        <translation>Nattmodus</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="75"/>
        <source>Take to Atelier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Take to Lab</source>
        <translation type="vanished">Ta til laboratoriet</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="76"/>
        <source>Put on Shelf</source>
        <translation>Putt på hyllen</translation>
    </message>
</context>
<context>
    <name>Saver</name>
    <message>
        <location filename="../qml/pages/Saver.qml" line="103"/>
        <source>Global Cupboard</source>
        <translation>Skap for hele systemet</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="104"/>
        <source>Atelier</source>
        <translation type="unfinished">Laboratorium</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="104"/>
        <source>Ambience</source>
        <translation type="unfinished">Omgivelse</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="132"/>
        <source>Clean out this cupboard</source>
        <translation>Tøm dette skapet</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="132"/>
        <source>Spring Clean</source>
        <translation>Vårrengjøring</translation>
    </message>
</context>
<context>
    <name>SaverPlus</name>
    <message>
        <source>anonymous</source>
        <translation type="vanished">anonym</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="20"/>
        <source>User Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="110"/>
        <source>System Ambience</source>
        <translation type="unfinished">System Atmosfære</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="120"/>
        <source>Ambience Cupboard</source>
        <translation>Omgivelsesskap</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="121"/>
        <source>Global Cupboard</source>
        <translation type="unfinished">Skap for hele systemet</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="171"/>
        <source>Clean out this cupboard</source>
        <translation>Tøm dette skapet</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="171"/>
        <source>Spring Clean</source>
        <translation>Vårrengjøring</translation>
    </message>
</context>
<context>
    <name>ShowRoom</name>
    <message>
        <source>A very long line showing Text in </source>
        <translation type="vanished">En veldig lang tekstlinje som viser tekst i </translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primærfarge</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Sekundærfarge</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Framhevelsesfarge</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Sekundær framhevelsesfarge</translation>
    </message>
    <message>
        <source>Error Color</source>
        <translation type="vanished">Feilfarge</translation>
    </message>
    <message>
        <source>Background Color</source>
        <translation type="vanished">Bakgrunnsfarge</translation>
    </message>
    <message>
        <source>four kinds of background overlay opacities and colors</source>
        <translation type="vanished">four kinds of background overlay opacities and colours</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Tekst</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <source>Progress Bar Demo</source>
        <translation type="vanished">Framdriftsbjelke-demo</translation>
    </message>
    <message>
        <source>Tap to restart Demos</source>
        <translation type="vanished">Trykk for å tilbakestille demoer</translation>
    </message>
    <message>
        <source>Remorse Item Demo</source>
        <translation type="vanished">Angringselement-demo</translation>
    </message>
    <message>
        <source>MenuItem</source>
        <translation type="vanished">menyelement</translation>
    </message>
    <message>
        <source>selected</source>
        <translation type="vanished">valgt</translation>
    </message>
    <message>
        <source>disabled</source>
        <translation type="vanished">avskrudd</translation>
    </message>
    <message>
        <source>Button</source>
        <translation type="vanished">Knapp</translation>
    </message>
    <message>
        <source>Text Elements</source>
        <translation type="vanished">Tekstelementer</translation>
    </message>
    <message>
        <source>UI Elements</source>
        <translation type="vanished">Grensesnittselementer</translation>
    </message>
</context>
<context>
    <name>ShowRoomMini</name>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="25"/>
        <source>Mini</source>
        <comment>small showroom</comment>
        <translation>Mini</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="25"/>
        <source>Showroom</source>
        <translation>Visningsrom</translation>
    </message>
</context>
<context>
    <name>ShowRoomText</name>
    <message>
        <source>A very long line showing Text in </source>
        <translation type="vanished">En veldig lang tekstlinje som viser tekst i </translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primærfarge</translation>
    </message>
    <message>
        <source>A very long line showing Text in</source>
        <translation type="vanished">En veldig lang tekstlinje som viser tekst i</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Sekundærfarge</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Framhevingsfarge</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Sekundær framhevingsfarge</translation>
    </message>
    <message>
        <source>Cover Background Color</source>
        <translation type="obsolete">Omslagsbakgrunnsfarge</translation>
    </message>
    <message>
        <source>Error Color</source>
        <translation type="vanished">Feilfarge</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Tekst</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <source>Overlay Background Color</source>
        <translation type="vanished">Overleggs-bakgrunnsfarge</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">Diffus framhevelsesfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="15"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="22"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="37"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="44"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="51"/>
        <source>%1 text</source>
        <comment>%1 is the name of a color</comment>
        <translation>%1 tekst</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="15"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="37"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="51"/>
        <source>Primary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="16"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="23"/>
        <source>No Background</source>
        <translation>Nei Bakgrunn</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="22"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="44"/>
        <source>Secondary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="30"/>
        <source>Highlight Background</source>
        <translation type="unfinished">Høydepunkt Bakgrunn</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="38"/>
        <source>Overlay Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="45"/>
        <source>Cover Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="52"/>
        <source>Wallpaper Overlay</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowRoomUI</name>
    <message>
        <source>Progress Bar Demo</source>
        <translation type="vanished">Framdriftsbjelke-demo</translation>
    </message>
    <message>
        <source>Remorse Item Demo</source>
        <translation type="vanished">Angringselement-demo</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="132"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="133"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="134"/>
        <source>MenuItem</source>
        <translation>menyelement</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="134"/>
        <source>selected</source>
        <translation>valgt</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="133"/>
        <source>disabled</source>
        <translation>avskrudd</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="20"/>
        <source>Waiting…</source>
        <translation>Venter…</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="28"/>
        <source>Feeling Remorse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="108"/>
        <source>App</source>
        <translation>App</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="109"/>
        <source>Clicked</source>
        <translation type="unfinished">Klikk på</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="110"/>
        <source>Dead</source>
        <translation>Døde</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="154"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="155"/>
        <source>Button</source>
        <translation>Knapp</translation>
    </message>
</context>
<context>
    <name>TransparencyEditor</name>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="45"/>
        <source>Edit Transparency</source>
        <translation>Rediger gjennomsiktighet</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="53"/>
        <source>This will edit the alpha channel of the color. Note that in the Sailfish UI many elements use their own transparency values and are not affected by the alpha channel of the color. Remorse Timers are one such example.</source>
        <translation>Dette redigerer alfakanalen tilhørende fargen. Merk at i Sailfish-grensesnittet bruker mange elementer sin egne gjennomsiktighetsverdier, og påvirkes ikke av alfakanalen til fargen. Avglemmingstidsur er ett eksempel.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="82"/>
        <source>Highlight Background Opacity</source>
        <translation>Dekkevne for bakgrunnsframhevelse</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="83"/>
        <source>This is used e.g. for Pulley Menu background.</source>
        <translation>Dette brukes f.eks. for nedtrekksmenyens bakgrunn.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="85"/>
        <source>Wallpaper Overlay Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="87"/>
        <source>Cover Overlay Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="88"/>
        <source>Transparency of application covers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="94"/>
        <source>Color Alpha Channel</source>
        <translation>Alfakanal for farge</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="95"/>
        <source>Here you can edit Alpha channels for colors that have one.</source>
        <translation>Her kan du redigere alfakanaler for farger som har det.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="96"/>
        <source>Secondary Color</source>
        <translation>Sekundærfarge</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="98"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundær framhevelsesfarge</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="obsolete">Framhevelsesbakgrunnsfarge</translation>
    </message>
</context>
</TS>
