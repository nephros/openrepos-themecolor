<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>AdvancedPage</name>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="22"/>
        <source>Advanced Options</source>
        <translation>Opciones avanzadas</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="31"/>
        <source>System Functions</source>
        <translation>Funciones del sistema</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="34"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="40"/>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation>Reiniciar %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="62"/>
        <source>Lipstick</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <source>Home Screen</source>
        <translation>Pantalla de inicio</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <source>Restart the %1 service. This will close all apps and relaunch the %2</source>
        <comment>arguments are &apos;Lipstick&apos; and &apos;Home Screen&apos;, one id the application name, one is the commonly used name for the same.</comment>
        <translation>Reinicia el servicio %1. Esto cerrará todas las aplicaciones y reiniciará el servicio %2</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="36"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="42"/>
        <source>Restarting</source>
        <translation>Reiniciando</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="41"/>
        <source>Restart the %1 service. This sometimes fixes color problems</source>
        <translation>Reinicia el servicio %1. Esto a veces soluciona los problemas de color</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="41"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="84"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="54"/>
        <source>Config Management</source>
        <translation>Gestión de la configuración</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="56"/>
        <source>Reset Config</source>
        <translation>Reiniciar la configuración</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="57"/>
        <source>Reset all color-related configuration values</source>
        <translation>Restablecer todos la configuración de los colores</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="58"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="63"/>
        <source>Resetting</source>
        <translation>Restableciendo</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="61"/>
        <source>Reset Config and Restart</source>
        <translation>Restablecer la configuración y reiniciar</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="62"/>
        <source>Reset all color-related configuration values and restart %1</source>
        <translation>Restablecer todos la configuración de los colores y reiniciar %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="69"/>
        <source>Advanced Editors</source>
        <translation>Editores avanzados</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="71"/>
        <source>Edit Global Blur</source>
        <translation>Editar el Desenfoque Global</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="72"/>
        <source>Edit blur parameters</source>
        <translation>Editar los parámetros del desenfoque</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="81"/>
        <source>Background Service</source>
        <translation>Servicio en segundo plano</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="83"/>
        <source>Set up daemon</source>
        <translation>Configurar daemon</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="84"/>
        <source>Background service which watches for %1 changes and applies actions.</source>
        <translation>Un proceso en segundo plano que monitoreas %1 los cambios y realizas acciones.</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="87"/>
        <source>Import/Export</source>
        <translation>Importar/Exportar</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="90"/>
        <source>Load Ambience File</source>
        <translation>Cargar el archivo del entorno</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="91"/>
        <source>Load a color scheme from a .ambience file.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AppInfoSingleton</name>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">ColorDelTema</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="25"/>
        <source>ThemeColor</source>
        <comment>this will also be used for the app icon</comment>
        <translation>ColorDelTema</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="26"/>
        <source>ThemeColor® RPM Builder™</source>
        <comment>this will also be used for the app icon</comment>
        <translation>ThemeColor® RPM Constructor™</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="38"/>
        <source>English</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Inglés</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="35"/>
        <source>German</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Alemán</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="48"/>
        <source>Norwegian Bokmål</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Bokmål Noruego</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="41"/>
        <source>Spanish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Español</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="45"/>
        <source>French</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Francés</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="51"/>
        <source>Russian</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Ruso</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="54"/>
        <source>Swedish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Sueco</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="57"/>
        <source>Simplified Chinese</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Chino simplificado</translation>
    </message>
</context>
<context>
    <name>BlurEditor</name>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="53"/>
        <source>Edit Blur Parameters</source>
        <translation>Editar los parámetros del desenfoque</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="63"/>
        <source>These are some values found in Lipstick. They are exposed here for your experimentation, although it is unclear where exactly they are applied, and changing them seems to have very little effect. They are also not part of Ambiences.</source>
        <translation>Estos son algunos valores que se encuentran en Lipstick. Están expuestos aquí para tu experimentación, aunque no está claro dónde se aplican exactamente, y cambiarlos parece tener muy poco efecto. Tampoco forman parte de Ambientes.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="67"/>
        <source>Blur Iterations</source>
        <translation>Iteraciones del desenfoque</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="77"/>
        <source>Blur Deviation</source>
        <translation>Desviación del desenfoque</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="87"/>
        <source>Blur Kernel</source>
        <translation>Kernel</translation>
    </message>
</context>
<context>
    <name>ColorField</name>
    <message>
        <source>specify RGB or aRGB value, e.g.</source>
        <translation type="vanished">especifica valor RGB o aRGB, p.e.</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="13"/>
        <source>an %1 value</source>
        <comment>parameter is either RGB or aRGB</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="14"/>
        <source>(the # is optional)</source>
        <translation>(# es opcional)</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="14"/>
        <source>e.g.</source>
        <translation>p.ej.</translation>
    </message>
</context>
<context>
    <name>ColorFilters</name>
    <message>
        <source>Tint</source>
        <translation type="vanished">Teñir</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="21"/>
        <source>Red</source>
        <translation>Rojo</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="21"/>
        <source>Tint %1</source>
        <translation>Tinte %1</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="29"/>
        <source>Darken</source>
        <translation>Oscurecer</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="37"/>
        <source>Brighten</source>
        <translation>Aclarar</translation>
    </message>
</context>
<context>
    <name>ColorGenerators</name>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="14"/>
        <source>Randomizer</source>
        <translation>Aleatorizar</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="16"/>
        <source>Filters</source>
        <translation>Filtros</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="18"/>
        <source>Scheme Generators</source>
        <translation>Generadores de tema</translation>
    </message>
</context>
<context>
    <name>ColorRandomizer</name>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="20"/>
        <source>Random</source>
        <translation>Aleatorio</translation>
    </message>
    <message>
        <source>Colors</source>
        <translation type="vanished">Colores</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="25"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="39"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="49"/>
        <source>Generated</source>
        <translation>Generados</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="30"/>
        <source>Bright</source>
        <translation>Brillantes</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="30"/>
        <source>Dark</source>
        <translation>Oscuros</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="44"/>
        <source>Gray</source>
        <translation>Grises</translation>
    </message>
</context>
<context>
    <name>ColorSchemeGenerator</name>
    <message>
        <source>Solarize</source>
        <translation type="vanished">Solarizar</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="17"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="32"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="39"/>
        <source>%1 Theme</source>
        <comment>parameter is name for a theme, e.g. night, day, random... you might just use the parameter and not translate theme at all.</comment>
        <translation>Tema %1</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="17"/>
        <source>Solarize</source>
        <comment>as in &apos;solarized&apos; color scheme</comment>
        <translation>Solarizado</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="24"/>
        <source>Generate</source>
        <translation>Generar</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="24"/>
        <source>from</source>
        <translation>de</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="24"/>
        <source>Highlight</source>
        <translation>Resaltar</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="32"/>
        <source>Night</source>
        <translation>Nocturno</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="52"/>
        <source>%1 Color Blindness</source>
        <comment>%1 is replaced with the variant of color blindness, as in &apos;red-green color-blindness&apos;</comment>
        <translation>Acromatopsia %1</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="52"/>
        <source>R/G</source>
        <comment>abbreviation for red-green color blindness</comment>
        <translation>R/V</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="52"/>
        <source>#%1/%2</source>
        <comment>for lists, as in &apos;number one of four&apos; is #1/4</comment>
        <translation>#%1/%2</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="39"/>
        <source>Summer</source>
        <translation>Verano</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="27"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="35"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="42"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="58"/>
        <source>Applying</source>
        <translation>Aplicando</translation>
    </message>
</context>
<context>
    <name>ColorSelectors</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Secondary Highlight Colour</translation>
    </message>
</context>
<context>
    <name>ColorSlider</name>
    <message>
        <location filename="../qml/components/controls/ColorSlider.qml" line="66"/>
        <source>Adjust sliders, tap to reset</source>
        <translation>Ajusta controles deslizantes, toca para resetear</translation>
    </message>
</context>
<context>
    <name>ColorSliders</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="15"/>
        <source>Primary Color</source>
        <translation>Color principal</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="21"/>
        <source>Secondary Color</source>
        <translation>Color secundario</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="28"/>
        <source>Highlight Color</source>
        <translation>Color de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="35"/>
        <source>Secondary Highlight Color</source>
        <translation>Color secundario de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="42"/>
        <source>Background Highlight Color</source>
        <translation>Color de fondo de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="49"/>
        <source>Dim Highlight Color</source>
        <translation>Color oscuro de realce</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Color de fondo de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="56"/>
        <source>Background Glow Color</source>
        <translation>Color de punto luminoso</translation>
    </message>
</context>
<context>
    <name>ColorSwapper</name>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="70"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="70"/>
        <source>Swap</source>
        <translation>Intercambiar</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="91"/>
        <location filename="../qml/components/ColorSwapper.qml" line="118"/>
        <source>Primary Color</source>
        <translation>Color principal</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="92"/>
        <location filename="../qml/components/ColorSwapper.qml" line="119"/>
        <source>Secondary Color</source>
        <translation>Color secundario</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="93"/>
        <location filename="../qml/components/ColorSwapper.qml" line="120"/>
        <source>Highlight Color</source>
        <translation>Color de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="94"/>
        <location filename="../qml/components/ColorSwapper.qml" line="121"/>
        <source>Secondary Highlight Color</source>
        <translation>Color secundario de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="95"/>
        <location filename="../qml/components/ColorSwapper.qml" line="122"/>
        <source>Background Highlight Color</source>
        <translation>Color de fondo de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="96"/>
        <location filename="../qml/components/ColorSwapper.qml" line="123"/>
        <source>Dim Highlight Color</source>
        <translation>Color oscuro de realce</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Color de fondo de realce</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Colour</translation>
    </message>
</context>
<context>
    <name>ColorTextInput</name>
    <message>
        <source>Color input</source>
        <translation type="vanished">Entrada de color</translation>
    </message>
    <message>
        <source>Input value, tap to reset</source>
        <translation type="vanished">Valor de entrada, toca para resetear</translation>
    </message>
</context>
<context>
    <name>ColorTextInputDialog</name>
    <message>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="16"/>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="32"/>
        <source>Color input</source>
        <translation>Entrada de color</translation>
    </message>
</context>
<context>
    <name>ColorTextInputs</name>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Color principal</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Color secundario</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Color de realce</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Color secundario de realce</translation>
    </message>
    <message>
        <source>Background Highlight Color</source>
        <translation type="vanished">Color de fondo de realce</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">Color oscuro de realce</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Color de fondo de realce</translation>
    </message>
    <message>
        <source>Background Glow Color</source>
        <translation type="vanished">Color de punto luminoso</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">ColorDelTema</translation>
    </message>
</context>
<context>
    <name>DaemonSettings</name>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="109"/>
        <source>Daemon Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="119"/>
        <source>Enable Watcher daemon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="114"/>
        <source>Watches for %1 changes and applies actions defined below.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="120"/>
        <source>Daemon is </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="120"/>
        <source>active</source>
        <translation>activo</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="120"/>
        <source>inactive</source>
        <translation>inactivas</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="138"/>
        <source>Actions</source>
        <translation>Acciones</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="144"/>
        <source>Select what happens when an %1 change is detected</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="149"/>
        <source>launch %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="150"/>
        <source>just opens the app</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="160"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="162"/>
        <source>Warning:</source>
        <translation>Atención:</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="161"/>
        <source>The features below are experimental and do not work reliably.</source>
        <translation>Las características siguientes son experimentales y no funcionan de forma fiable.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="169"/>
        <source>apply Top theme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="170"/>
        <source>applies theme from the top %1 shelf</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="177"/>
        <source>apply Night theme</source>
        <translation></translation>
    </message>
    <message>
        <source>ThemeColor</source>
        <translation type="obsolete">ColorDelTema</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="114"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="144"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="170"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="178"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="178"/>
        <source>taken from the second %1 shelf</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="191"/>
        <source>Night begins:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="205"/>
        <source>Night ends:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <source>Adjust Theme Colors</source>
        <translation type="vanished">Ajustar colores del tema</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="29"/>
        <source>Showroom</source>
        <translation>Sala de exposiciones</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="29"/>
        <source>Atelier</source>
        <translation>Atelier</translation>
    </message>
    <message>
        <source>Input Mode:</source>
        <translation type="vanished">Modo de entrada:</translation>
    </message>
    <message>
        <source>Tap to switch</source>
        <translation type="vanished">Toca para cambiar</translation>
    </message>
    <message>
        <source>Swapper/Copier</source>
        <translation type="vanished">Intercambiador/Copiadora</translation>
    </message>
    <message>
        <source>Advanced…</source>
        <translation type="vanished">Avanzado…</translation>
    </message>
    <message>
        <source>Compute all Colors from Highlight</source>
        <translation type="vanished">Calcular todos los colores de realce</translation>
    </message>
    <message>
        <source>Applying</source>
        <translation type="vanished">Aplicando</translation>
    </message>
    <message>
        <source>Apply Colors to System</source>
        <translation type="vanished">Aplicar colores al sistema</translation>
    </message>
    <message>
        <source>Reload Colors from System</source>
        <translation type="vanished">Reload Colours from System</translation>
    </message>
    <message>
        <source>Generators</source>
        <translation type="vanished">Generadores</translation>
    </message>
    <message>
        <source>Reload Colors from current Theme</source>
        <translation type="vanished">Volver a cargar colores del tema actual</translation>
    </message>
    <message>
        <source>Reload Colors from System Config</source>
        <translation type="vanished">Volver a cargar colores del sistema</translation>
    </message>
    <message>
        <source>Experimental or dangerous actions</source>
        <translation type="vanished">Acciones experimentales o peligrosas</translation>
    </message>
    <message>
        <source>Export to Ambience file</source>
        <translation type="vanished">Exportar a archivo de ambiente</translation>
    </message>
    <message>
        <source>Export to Ambience package</source>
        <translation type="vanished">Exportar a paquete de Ambiente</translation>
    </message>
    <message>
        <source>(not implemented)</source>
        <translation type="vanished">(no implementado)</translation>
    </message>
    <message>
        <source>Save Theme to current Ambience</source>
        <translation type="vanished">Guardar tema a ambiente actual</translation>
    </message>
    <message>
        <source>Saving</source>
        <translation type="vanished">Guardando</translation>
    </message>
    <message>
        <source>Resetting</source>
        <translation type="vanished">Restableciendo</translation>
    </message>
    <message>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation type="vanished">Reiniciar %1</translation>
    </message>
    <message>
        <source>Restarting</source>
        <translation type="vanished">Reiniciando</translation>
    </message>
    <message>
        <source>Reset all values and restart</source>
        <translation type="vanished">Restaurar todos los valores y reiniciar</translation>
    </message>
    <message>
        <source>Reset nonstandard values</source>
        <translation type="vanished">Restaurar a valores no estándar</translation>
    </message>
    <message>
        <source>Restart Lipstick</source>
        <translation type="vanished">Reiniciar Lipstick</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Ayuda</translation>
    </message>
    <message>
        <source>Sliders</source>
        <translation type="vanished">Controles deslizantes</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Texto</translation>
    </message>
    <message>
        <source>Swapper</source>
        <translation type="vanished">Swapper</translation>
    </message>
    <message>
        <source>Randomizer</source>
        <translation type="vanished">Aleatorizar</translation>
    </message>
    <message>
        <source>Jolla Original</source>
        <translation type="vanished">Original de Jolla</translation>
    </message>
    <message>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation type="vanished">Manual</translation>
    </message>
    <message>
        <source>Open Ambience Settings</source>
        <translation type="vanished">Abrir ajustes de ambiente</translation>
    </message>
</context>
<context>
    <name>HelpColors</name>
    <message>
        <source>The four basic Colors</source>
        <translation type="vanished">Los cuatro colores básicos</translation>
    </message>
    <message>
        <source>The four basic Colors</source>
        <comment>Help Section</comment>
        <translation type="vanished">Los cuatro colores básicos</translation>
    </message>
    <message>
        <source>primary, for most interactive elements (it is black or white depending on Ambience Scheme)</source>
        <translation type="vanished">primario, para la mayoría de los elementos interactivos (es blanco o negro según el esquema de ambiente)</translation>
    </message>
    <message>
        <source>Additional Colors</source>
        <comment>Help Section</comment>
        <translation type="vanished">Colores adicionales</translation>
    </message>
    <message>
        <source>Additional Colors</source>
        <translation type="vanished">Colores adicionales</translation>
    </message>
</context>
<context>
    <name>HelpCredits</name>
    <message>
        <source>About</source>
        <translation type="vanished">Acerca de</translation>
    </message>
    <message>
        <source>About</source>
        <comment>Help Section</comment>
        <translation type="vanished">Acerca de</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">Versión:</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation type="vanished">Derechos de autor:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Licencia:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation type="vanished">Código fuente:</translation>
    </message>
    <message>
        <source>Translations</source>
        <translation type="vanished">Traducciones</translation>
    </message>
</context>
<context>
    <name>HelpDaemon</name>
    <message>
        <source>Ambience</source>
        <translation type="vanished">Ambiente</translation>
    </message>
</context>
<context>
    <name>HelpExport</name>
    <message>
        <source>Exporting Ambiences</source>
        <translation type="vanished">Exportación de ambientes</translation>
    </message>
    <message>
        <source>Exporting Ambiences</source>
        <comment>Help Section</comment>
        <translation type="vanished">Exportación de ambientes</translation>
    </message>
    <message>
        <source>Export to Ambience package</source>
        <translation type="vanished">Exportar a paquete de Ambiente</translation>
    </message>
    <message>
        <source>The Builder</source>
        <comment>Help Section</comment>
        <translation type="vanished">El Constructor</translation>
    </message>
    <message>
        <source>Notes</source>
        <comment>Help Section</comment>
        <extracomment>as in something to add, not a small piece of paper</extracomment>
        <translation type="vanished">Notas</translation>
    </message>
    <message>
        <source>The Builder</source>
        <translation type="vanished">El Constructor</translation>
    </message>
    <message>
        <source>Notes</source>
        <comment>as in something to add, not a small piece of paper </comment>
        <translation type="vanished">Notas</translation>
    </message>
</context>
<context>
    <name>HelpGeneral</name>
    <message>
        <source>General</source>
        <translation type="vanished">General</translation>
    </message>
    <message>
        <source>General</source>
        <comment>Help Section</comment>
        <translation type="vanished">General</translation>
    </message>
    <message>
        <source>Basic Workflow</source>
        <comment>Help Section</comment>
        <translation type="vanished">Flujo de trabajo básico</translation>
    </message>
</context>
<context>
    <name>HelpHow</name>
    <message>
        <source>Loading and Applying Colors</source>
        <comment>Help Section</comment>
        <translation type="vanished">Cargar y aplicar colores</translation>
    </message>
    <message>
        <source>App Configuration and State</source>
        <comment>Help Section</comment>
        <translation type="vanished">Configuración y estado de la aplicación</translation>
    </message>
    <message>
        <source>The Builder</source>
        <comment>Help Section</comment>
        <translation type="vanished">El Constructor</translation>
    </message>
    <message>
        <source>The Builder</source>
        <translation type="vanished">El Constructor</translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <source>How to Use</source>
        <translation type="vanished">Forma de utilización</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="vanished">General</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colors can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor, as these are the ones saved by the system.          There are other colors in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colours can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor (sic!), as these are the ones saved by the system.          There are other colours in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only some colors can be edited.          There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited. There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <translation type="vanished">La sala de exposiciones</translation>
    </message>
    <message>
        <source>The Atelier</source>
        <translation type="vanished">El laboratorio</translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <translation type="vanished">El armario</translation>
    </message>
    <message>
        <source>Tips and Caveats</source>
        <translation type="vanished">Consejos y advertencias</translation>
    </message>
    <message>
        <source>It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
          It&apos;s a good idea to store a known-good color sceme in the Cupboard so you can go restore easily.&lt;br /&gt;          &lt;br /&gt;          If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;          &lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;          &lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;          and repeat for all the other colors stored there. &lt;br /&gt;          Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;&lt;br /&gt;If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;and repeat for all the other colours stored there. &lt;br /&gt;Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, 
nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited.
There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited.
There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>The top area on the first page (&quot;Showroom&quot;) is non-interactive and just shows the colors that are selected currently.&lt;br /&gt;Here you can preview your creation.</source>
        <translation type="vanished">El área superior de la primera página (&quot;Sala de exposiciones&quot;) no es interactiva y sólo muestra los colores que actualmente se han seleccionado.&lt;br/&gt;Aquí puedes obtener una vista previa de tu creación.</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.  In Text input mode, you can enter color values directly. Randomizer
does what it sais, and Jolla Original you already know.  Swapper lets you change color definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="vanished">In Slider input mode, use the Sliders in the lower section to adjust the colours. In Text input mode, you can enter colour values directly. Randomizer
does what it sais, and Jolla Original you already know. Swapper lets you change colour definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colours to the current session.</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="vanished">This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, 
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. 
This means once applied through the app, they will always stay the same until you change them again in the App. 
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard.
If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla Ambiences only define four colours, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added,
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colours than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there.
This means once applied through the app, they will always stay the same until you change them again in the App.
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colours when switching editing modes, applying colours to system, or taking palettes from the Cupboard.
If that happens, try reloading the colours from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colours somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colours stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited. There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;
&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="vanished">Con esta aplicación puedes modificar el esquema de color actual de Lipstick. Sin embargo, esta aplicación no cambiará ni creará ambientes nuevos (todavía), ni los cambios realizados continuarán cuando cambies de ambiente, reinicies Lipstick o reinicies el dispositivo.&lt;br/&gt;
Actualmente sólo pueden editarse algunos colores. Hay otros colores que usa el sistema, que se calculan automáticamente a partir de los cuatro colores básicos y no se pueden modificar.&lt;br/&gt;
&lt;br/&gt;
Estamos trabajando para superar algunas de estas limitaciones.</translation>
    </message>
    <message>
        <source>The top area on the first page (&quot;Showroom&quot;) shows the colors that are selected currently using various UI elements.&lt;br /&gt;Here you can preview your creation.&lt;br /&gt;Tapping on either area will hide it. To unhide, tap the title header.</source>
        <translation type="obsolete">El área superior de la primera página (&quot;Sala de exposiciones&quot;) no es interactiva y sólo muestra los colores que actualmente se han seleccionado.&lt;br/&gt;Aquí puedes obtener una vista previa de tu creación.</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors. In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know. Swapper lets you change color definitions.&lt;br /&gt;
Check what your theme will look like in the Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="vanished">En el modo de entrada &quot;Control deslizante&quot;, usa los controles deslizantes de la sección de abajo para ajustar los colores. En el modo de entrada &quot;Texto&quot;, puedes introducir directamente los valores del color. El modo &quot;Aleatorizador&quot; hace lo que dice, y &quot;Original de Jolla&quot;, ya lo sabes. Con el modo &quot;Intercambiador&quot; puedes cambiar las definiciones del color.&lt;br/&gt;
Comprueba cómo se verá tu tema en la sala de exposiciones.&lt;br/&gt;
&lt;br/&gt;
Cuando hayas terminado, usa el menú deslizante para aplicar los colores a la sesión actual.</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="vanished">Este área te permite almacenar las paletas que has creado para un uso posterior. Hay un armario global y uno específico para el ambiente actual.&lt;br/&gt;
Ten en cuenta que sólo los ambientes de todo el sistema tienen un nombre, los personalizados se mostrarán como anónimos (por ahora)</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">En los ambientes de Jolla sólo se definen cuatro colores, principal (que es negro o blanco según el esquema del ambiente), secundario, que es como el principal pero un poco menos opaco, de realce y de realce secundario que son los colores que se seleccionan en la configuración del ambiente.&lt;br/&gt;
Esta aplicación te permite editar otros colores además de estos cuatro, pero no se verán afectados ante cambios de ambiente o reinicio de lipstick, ya que se almacenan en la base de datos dconf y permanecen allí. Esto significa que una vez aplicados a través de la aplicación, siempre permanecerán igual hasta que los cambies nuevamente en la aplicación. Puedes usar la opción de reinicio documentada a continuación para deshacerte de ellos si es necesario.
&lt;br/&gt;
La aplicación se confunde con frecuencia con los colores al cambiar de modo de edición, aplicar colores al sistema o tomar paletas del armario. Si eso sucede, intenta volver a cargar desde el sistema, eso ayuda en la mayoría de los casos.&lt;br/&gt;
&lt;br/&gt;
Es posible crear esquemas de color que hagan que partes de la interfaz de usuario sean ilegibles. Verifica las áreas especialmente no obvias como el teclado virtual.&lt;br/&gt;
Es una buena idea almacenar una combinación de colores buena en el armario para que puedas restaurarla fácilmente.&lt;br/&gt;
&lt;br/&gt;
Si de alguna manera has estropeado los colores, usa la opción del menú superior para restablecer todo, o desde la línea de comandos: &lt;br/&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
y repite ésto para todos los demás colores almacenados allí.&lt;br/&gt;
También ayuda si cambias de ambiente desde la configuración del sistema.</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">Versión:</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation type="vanished">Derechos de autor:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Licencia:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation type="vanished">Código fuente:</translation>
    </message>
    <message>
        <source>Translations:</source>
        <translation type="vanished">Traducciones:</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App.  You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla Ambiences only define four colours, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colours than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colours when switching editing modes, applying colours to system, or taking palettes from the Cupboard. If that happens, try reloading the colours from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colours somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colours stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">Acerca de</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">Version: </translation>
    </message>
    <message>
        <source>Copyright: </source>
        <translation type="vanished">Copyright: </translation>
    </message>
    <message>
        <source>License: </source>
        <translation type="vanished">Licence: </translation>
    </message>
    <message>
        <source>Source Code: </source>
        <translation type="vanished">Source Code: </translation>
    </message>
    <message>
        <source>General Information</source>
        <translation type="vanished">Información general</translation>
    </message>
    <message>
        <source>About the UI</source>
        <translation type="vanished">Sobre el UI</translation>
    </message>
    <message>
        <source>About Colors</source>
        <translation type="vanished">Sobre Colores</translation>
    </message>
    <message>
        <source>Exporting Ambiences</source>
        <translation type="vanished">Exportación de Ambientes</translation>
    </message>
    <message>
        <source>How it works</source>
        <translation type="vanished">Cómo funciona</translation>
    </message>
    <message>
        <source>Limitations, Tips etc.</source>
        <translation type="vanished">Limitaciones, Vierte etc.</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="vanished">Créditos</translation>
    </message>
    <message>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation type="vanished">Manual</translation>
    </message>
    <message>
        <source>Chapter %1:</source>
        <comment>chapter in the handbook. argument is a number</comment>
        <translation type="vanished">Capítulo %1:</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="65"/>
        <source>Select a chapter</source>
        <translation>Seleccionar un capítulo</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="66"/>
        <source>Tap to switch</source>
        <translation>Toca para cambiar</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="73"/>
        <source>General Information</source>
        <comment>Help Index</comment>
        <translation>Información general</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="74"/>
        <source>About the UI</source>
        <comment>Help Index</comment>
        <translation>Sobre el UI</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="75"/>
        <source>About Colors</source>
        <comment>Help Index</comment>
        <translation>Sobre Colores</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="76"/>
        <source>Exporting Ambiences</source>
        <comment>Help Index</comment>
        <translation>Exportación de ambientes</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="77"/>
        <source>Watcher Daemon</source>
        <comment>Help Index</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="78"/>
        <source>How it works</source>
        <comment>Help Index</comment>
        <translation>Cómo funciona</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="79"/>
        <source>Limitations , Tips etc.</source>
        <comment>Help Index</comment>
        <translation>Limitaciones, consejos, etc.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="102"/>
        <source>View %1</source>
        <comment>User Manual display language</comment>
        <translation>Ver %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="102"/>
        <source>English</source>
        <translation>Inglés</translation>
    </message>
    <message>
        <source>Credits</source>
        <comment>Help Index</comment>
        <translation type="vanished">Créditos</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="59"/>
        <source>Handbook</source>
        <comment>Help Index</comment>
        <extracomment>User Manual, Application Manual, Handbook, Howto</extracomment>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="65"/>
        <source>Chapter %1:</source>
        <comment>Help Index Chapter Prefix</comment>
        <translation>Capítulo %1:</translation>
    </message>
</context>
<context>
    <name>HelpTips</name>
    <message>
        <source>Tips and Caveats</source>
        <translation type="vanished">Consejos y advertencias</translation>
    </message>
    <message>
        <source>Tips and Caveats</source>
        <comment>Help Section</comment>
        <translation type="vanished">Consejos y advertencias</translation>
    </message>
</context>
<context>
    <name>HelpUIMain</name>
    <message>
        <source>The main UI sections</source>
        <translation type="vanished">Las secciones principales de la IGU</translation>
    </message>
    <message>
        <source>&lt;p&gt;The main UI consists of the following:
       &lt;ul&gt;
       &lt;li&gt;The Showroom&lt;/li&gt;
       &lt;li&gt;The Atelier&lt;/li&gt;
       &lt;li&gt;The Cupboards&lt;/li&gt;
       &lt;li&gt;PullDown and PushUp Menus&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;La interfaz de usuario principal consiste en lo siguiente:
       &lt;ul&gt;
       &lt;li&gt;La sala de exposiciones&lt;/li&gt;
       &lt;li&gt;El Laboratorio&lt;/li&gt;
       &lt;li&gt;Los armarios&lt;/li&gt;
       &lt;li&gt;Menús PullDown y PushUp&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <translation type="vanished">La sala de exposiciones</translation>
    </message>
    <message>
        <source>The Atelier</source>
        <translation type="vanished">El laboratorio</translation>
    </message>
    <message>
        <source>The main UI sections</source>
        <comment>Help Section</comment>
        <translation type="vanished">Las secciones principales de la IGU</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <comment>Help Section</comment>
        <translation type="vanished">La sala de exposiciones</translation>
    </message>
    <message>
        <source>The Atelier</source>
        <comment>Help Section</comment>
        <translation type="vanished">El laboratorio</translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <comment>Help Section</comment>
        <translation type="vanished">El armario</translation>
    </message>
    <message>
        <source>Put on Shelf</source>
        <translation type="vanished">Poner en estante</translation>
    </message>
    <message>
        <source>Take to Lab</source>
        <translation type="vanished">Llevar al lab</translation>
    </message>
    <message>
        <source>Menus</source>
        <comment>Help Section</comment>
        <translation type="vanished">Menús</translation>
    </message>
    <message>
        <source>Menus</source>
        <translation type="vanished">Menús</translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <translation type="vanished">El armario</translation>
    </message>
</context>
<context>
    <name>LabPage</name>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="38"/>
        <source>Atelier</source>
        <translation>Atelier</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="38"/>
        <source>Preview</source>
        <translation>Vista previa</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="38"/>
        <source>Cupboards</source>
        <translation>Armarios</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="43"/>
        <source>Current Palette</source>
        <translation>Paleta actual</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="48"/>
        <source>Editor</source>
        <comment>tool/editing mode</comment>
        <translation>Editor</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="49"/>
        <source>Tap to switch</source>
        <translation>Toca para cambiar</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="57"/>
        <source>Sliders</source>
        <translation>Controles deslizantes</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="58"/>
        <source>Swapper/Copier</source>
        <translation>Intercambiador/Copiadora</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="59"/>
        <source>Generators</source>
        <translation>Generadores</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="60"/>
        <source>Jolla Original</source>
        <translation>Original de Jolla</translation>
    </message>
</context>
<context>
    <name>LoadAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="36"/>
        <source>Load</source>
        <translation>Cargar</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="97"/>
        <source>Load Theme</source>
        <translation>Cargar Tema</translation>
    </message>
    <message>
        <source>Take to Lab</source>
        <translation type="vanished">Llevar al lab</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="97"/>
        <source>Take to Atelier</source>
        <translation>Llevar a Atelier</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="115"/>
        <source>Here you can load a .ambience (JSON) file.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="119"/>
        <source>current</source>
        <translation>actual</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="125"/>
        <source>file data</source>
        <translation>Datos de archivo</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="131"/>
        <source>File name </source>
        <translation>Nombre del archivo: </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="133"/>
        <source>Click to select</source>
        <translation>Pulse para seleccionar</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="140"/>
        <source>File content</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="149"/>
        <source>No file loaded</source>
        <translation>¡No se cargó nada!</translation>
    </message>
</context>
<context>
    <name>LootBoxItem</name>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="27"/>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="32"/>
        <source>ThemeColor</source>
        <translation>ColorDelTema</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="31"/>
        <source>A Lootbox was delivered!</source>
        <translation>¡Se ha entregado una caja de recompensa!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="32"/>
        <source>now has more shelves!</source>
        <translation>¡Ahora tienes más estantes!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="32"/>
        <source>Your persistence has been rewarded.</source>
        <translation>Se ha premiado tu perseverancia.</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="33"/>
        <source>Your persistence has been rewarded!</source>
        <translation>¡Se ha premiado tu perseverancia!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="43"/>
        <source>Purchase Options</source>
        <translation>Opciones de compra</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="52"/>
        <source>Payment* received!</source>
        <translation>¡Pago* recibido!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="52"/>
        <source>Buy more shelves</source>
        <translation>Comprar más estantes</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="57"/>
        <source>Using Jolla Shop credentials to buy Storage Lootbox</source>
        <translation>Usando credenciales de la tienda de Jolla para comprar caja de recompensa</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="74"/>
        <source>Thank you for your purchase!&lt;br /&gt;Your extra shelves will be delivered in the next update!</source>
        <translation>¡Gracias por la compra!&lt;br /&gt;¡Tus estantes extra se entregarán en la siguiente actualización!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="88"/>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;…or did it?</source>
        <translation>*) No, en serio, no hay compras internas en esta aplicación o cajas de recompensa en la Tienda Jolla. Eso sería ridículo. &lt;br/&gt;No se han transferido fondos.&lt;br/&gt;De hecho, realmente no sucedió nada en este momento. &lt;br/&gt;...¿o sí?</translation>
    </message>
    <message>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</source>
        <translation type="vanished">*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="9"/>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="12"/>
        <source>Advanced…</source>
        <translation>Avanzado…</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="15"/>
        <source>Export to Ambience package</source>
        <translation>Exportar a paquete de Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="18"/>
        <source>Apply Colors to System</source>
        <translation>Aplicar colores al sistema</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="19"/>
        <source>Applying</source>
        <translation>Aplicando</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="21"/>
        <source>Reload Colors from System</source>
        <translation>Recargar los colores del sistema</translation>
    </message>
</context>
<context>
    <name>SaveAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="53"/>
        <source>Export Functions</source>
        <translation>Exportar funciones</translation>
    </message>
    <message>
        <source>Here you can export your creation to a .ambience (json) file.&lt;br /&gt;
                      This is not terribly useful at the moment, but you can use that file to build your own ambiences by adding an image, optionally some sounds, and package the whole thing as an RPM.
                     </source>
        <translation type="vanished">Aquí puedes exportar tu creación a un archivo .ambience (json).&lt;br /&gt;
                      Por ahora esto no es muy útil, pero puedes usar el archivo para construir tus propios ambientes añadiendo una imagen, opcionalmente algunos sonidos y empaquetarlo todo en un RPM.
                     </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="67"/>
        <source>Here you can export your creation to a .ambience (JSON) file, create an installable package from it, and optionally install that on your device.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="69"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="69"/>
        <source>First, enter a name for your %1. You can edit the file by clicking the name, and generate a package from it using the Builder Button.</source>
        <translation>Primero, introduce un nombre para tu %1. Puedes editar el archivo haciendo clic en el nombre, y generar un paquete a partir de él utilizando el Botón Constructor.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="71"/>
        <source>The .ambience file as well as any built packages will be stored in the %1 folder. (%2)</source>
        <comment>first argument is the short name of the location, second argument is the full path</comment>
        <translation>El archivo .ambience, así como los paquetes creados, se almacenarán en la carpeta %1. (%2)</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="89"/>
        <source>Export to File</source>
        <translation>Exportar a archivo</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="94"/>
        <source>Ambience Name</source>
        <translation>Nombre de ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="96"/>
        <source>A cool Ambience Name</source>
        <translation>Un nombre de ambiente bonito</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="191"/>
        <source>Click to Open</source>
        <translation>Toca para Abrir</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="202"/>
        <source>Build Package</source>
        <translation>Paquete de la compilación</translation>
    </message>
    <message>
        <source>Click to export</source>
        <translation type="vanished">Haz clic para exportar</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="192"/>
        <source>File Name</source>
        <translation>Nombre del archivo</translation>
    </message>
    <message>
        <source>Open File</source>
        <translation type="vanished">Abrir el archivo</translation>
    </message>
    <message>
        <source>Launch Builder</source>
        <translation type="vanished">Comenzar Constructor</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="74"/>
        <source>Disclaimer</source>
        <translation>Descargo de responsabilidad</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="71"/>
        <source>Documents</source>
        <translation>Documentos</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="86"/>
        <source>If you decide to share or otherwise distribute the package make sure you respect copy- and other rights for any content included.</source>
        <translation></translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Ayuda</translation>
    </message>
</context>
<context>
    <name>SaveSlot</name>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="45"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="46"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="45"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="46"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="48"/>
        <source>Shelf</source>
        <translation>Estante</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <source>default</source>
        <translation>por defecto</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="45"/>
        <source>night mode</source>
        <translation>Modo nocturno</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="75"/>
        <source>Take to Atelier</source>
        <translation>Llevar a Atelier</translation>
    </message>
    <message>
        <source>Take to Lab</source>
        <translation type="vanished">Llevar al lab</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="76"/>
        <source>Put on Shelf</source>
        <translation>Poner en estante</translation>
    </message>
</context>
<context>
    <name>Saver</name>
    <message>
        <location filename="../qml/pages/Saver.qml" line="103"/>
        <source>Global Cupboard</source>
        <translation>Armario global</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="104"/>
        <source>Atelier</source>
        <translation>Atelier</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="104"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="132"/>
        <source>Clean out this cupboard</source>
        <translation>Limpiar este armario</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="132"/>
        <source>Spring Clean</source>
        <translation>Limpieza general</translation>
    </message>
</context>
<context>
    <name>SaverPlus</name>
    <message>
        <source>anonymous</source>
        <translation type="vanished">anónimo</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="20"/>
        <source>User Ambience</source>
        <translation>Ambiente del usuario</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="110"/>
        <source>System Ambience</source>
        <translation>Ambientación del sistema</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="120"/>
        <source>Ambience Cupboard</source>
        <translation>Armario del ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="121"/>
        <source>Global Cupboard</source>
        <translation>Armario global</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="171"/>
        <source>Clean out this cupboard</source>
        <translation>Limpiar este armario</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="171"/>
        <source>Spring Clean</source>
        <translation>Limpieza general</translation>
    </message>
</context>
<context>
    <name>ShowRoom</name>
    <message>
        <source>A very long line showing Text in </source>
        <translation type="vanished">Una línea muy larga está mostrando texto en </translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Color principal</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Color secundario</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Color de realce</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Color secundario de realce</translation>
    </message>
    <message>
        <source>Error Color</source>
        <translation type="vanished">Color de error</translation>
    </message>
    <message>
        <source>Background Color</source>
        <translation type="vanished">Color de fondo</translation>
    </message>
    <message>
        <source>four kinds of background overlay opacities and colors</source>
        <translation type="vanished">four kinds of background overlay opacities and colours</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Texto</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Color de fondo de realce</translation>
    </message>
    <message>
        <source>Progress Bar Demo</source>
        <translation type="vanished">Demo de barra de progreso</translation>
    </message>
    <message>
        <source>Tap to restart Demos</source>
        <translation type="vanished">Toca para reiniciar Demos</translation>
    </message>
    <message>
        <source>Remorse Item Demo</source>
        <translation type="vanished">Demo de barra de cancelación</translation>
    </message>
    <message>
        <source>MenuItem</source>
        <translation type="vanished">Elemento del menú</translation>
    </message>
    <message>
        <source>selected</source>
        <translation type="vanished">seleccionado</translation>
    </message>
    <message>
        <source>disabled</source>
        <translation type="vanished">desactivado</translation>
    </message>
    <message>
        <source>Button</source>
        <translation type="vanished">Botón</translation>
    </message>
    <message>
        <source>Text Elements</source>
        <translation type="vanished">Elementos de texto</translation>
    </message>
    <message>
        <source>UI Elements</source>
        <translation type="vanished">Elementos de la UI</translation>
    </message>
</context>
<context>
    <name>ShowRoomMini</name>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="25"/>
        <source>Mini</source>
        <comment>small showroom</comment>
        <translation>Mini</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="25"/>
        <source>Showroom</source>
        <translation>Sala de exposiciones</translation>
    </message>
</context>
<context>
    <name>ShowRoomText</name>
    <message>
        <source>A very long line showing Text in </source>
        <translation type="vanished">Una línea muy larga está mostrando texto en </translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Color principal</translation>
    </message>
    <message>
        <source>A very long line showing Text in</source>
        <translation type="vanished">Una línea muy larga está mostrando texto en</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Color secundario</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Color de realce</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Color secundario de realce</translation>
    </message>
    <message>
        <source>Cover Background Color</source>
        <translation type="vanished">Color de fondo de la cabecera</translation>
    </message>
    <message>
        <source>Error Color</source>
        <translation type="vanished">Color de error</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Texto</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Color de fondo de realce</translation>
    </message>
    <message>
        <source>Overlay Background Color</source>
        <translation type="vanished">Color de fondo de la superposición</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">Color oscuro de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="15"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="22"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="37"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="44"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="51"/>
        <source>%1 text</source>
        <comment>%1 is the name of a color</comment>
        <translation>texto %1</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="15"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="37"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="51"/>
        <source>Primary</source>
        <translation>Primario</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="16"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="23"/>
        <source>No Background</source>
        <translation>sin antecedentes</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="22"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="44"/>
        <source>Secondary</source>
        <translation>Secundario</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="30"/>
        <source>Highlight Background</source>
        <translation>Resaltar el fondo</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="38"/>
        <source>Overlay Background</source>
        <translation>Superposición del fondo</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="45"/>
        <source>Cover Background</source>
        <translation>Fondo de la portada</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="52"/>
        <source>Wallpaper Overlay</source>
        <translation>Superposición para la imagen de fondo</translation>
    </message>
</context>
<context>
    <name>ShowRoomUI</name>
    <message>
        <source>Progress Bar Demo</source>
        <translation type="vanished">Demo de barra de progreso</translation>
    </message>
    <message>
        <source>Remorse Item Demo</source>
        <translation type="vanished">Demo de barra de cancelación</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="132"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="133"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="134"/>
        <source>MenuItem</source>
        <translation>Elemento del menú</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="134"/>
        <source>selected</source>
        <translation>seleccionado</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="133"/>
        <source>disabled</source>
        <translation>desactivado</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="20"/>
        <source>Waiting…</source>
        <translation>Esperando…</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="28"/>
        <source>Feeling Remorse</source>
        <translation>Sentir remordimiento</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="108"/>
        <source>App</source>
        <translation>Aplicación</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="109"/>
        <source>Clicked</source>
        <translation>Hizo clic</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="110"/>
        <source>Dead</source>
        <translation>Muerto</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="154"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="155"/>
        <source>Button</source>
        <translation>Botón</translation>
    </message>
</context>
<context>
    <name>TransparencyEditor</name>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="45"/>
        <source>Edit Transparency</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="53"/>
        <source>This will edit the alpha channel of the color. Note that in the Sailfish UI many elements use their own transparency values and are not affected by the alpha channel of the color. Remorse Timers are one such example.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="82"/>
        <source>Highlight Background Opacity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="83"/>
        <source>This is used e.g. for Pulley Menu background.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="85"/>
        <source>Wallpaper Overlay Opacity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="87"/>
        <source>Cover Overlay Opacity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="88"/>
        <source>Transparency of application covers.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="94"/>
        <source>Color Alpha Channel</source>
        <translation>Canal alfa de color</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="95"/>
        <source>Here you can edit Alpha channels for colors that have one.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="96"/>
        <source>Secondary Color</source>
        <translation>Color secundario</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="98"/>
        <source>Secondary Highlight Color</source>
        <translation>Color secundario de realce</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Color de fondo de realce</translation>
    </message>
</context>
</TS>
