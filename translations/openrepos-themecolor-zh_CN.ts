<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AdvancedPage</name>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="22"/>
        <source>Advanced Options</source>
        <translation type="unfinished">高级选项</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="31"/>
        <source>System Functions</source>
        <translation type="unfinished">System Functions</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="34"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="40"/>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation type="unfinished">重新启动 %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="62"/>
        <source>Lipstick</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <source>Home Screen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <source>Restart the %1 service. This will close all apps and relaunch the %2</source>
        <comment>arguments are &apos;Lipstick&apos; and &apos;Home Screen&apos;, one id the application name, one is the commonly used name for the same.</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="36"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="42"/>
        <source>Restarting</source>
        <translation type="unfinished">正在重启</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="41"/>
        <source>Restart the %1 service. This sometimes fixes color problems</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="41"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="84"/>
        <source>Ambience</source>
        <translation type="unfinished">氛围</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="54"/>
        <source>Config Management</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="56"/>
        <source>Reset Config</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="57"/>
        <source>Reset all color-related configuration values</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="58"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="63"/>
        <source>Resetting</source>
        <translation type="unfinished">重置</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="61"/>
        <source>Reset Config and Restart</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="62"/>
        <source>Reset all color-related configuration values and restart %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="69"/>
        <source>Advanced Editors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="71"/>
        <source>Edit Global Blur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="72"/>
        <source>Edit blur parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="81"/>
        <source>Background Service</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="83"/>
        <source>Set up daemon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="84"/>
        <source>Background service which watches for %1 changes and applies actions.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="87"/>
        <source>Import/Export</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="90"/>
        <source>Load Ambience File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="91"/>
        <source>Load a color scheme from a .ambience file.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AppInfoSingleton</name>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">主题颜色</translation>
    </message>
    <message>
        <source>ThemeColor® RPM Builder™</source>
        <translation type="vanished">主题颜色® RPM 构建程序™</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="25"/>
        <source>ThemeColor</source>
        <comment>this will also be used for the app icon</comment>
        <translation>主题颜色</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="26"/>
        <source>ThemeColor® RPM Builder™</source>
        <comment>this will also be used for the app icon</comment>
        <translation>主题颜色® RPM 构建程序™</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="38"/>
        <source>English</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>简体中文</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="35"/>
        <source>German</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>德文</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="48"/>
        <source>Norwegian Bokmål</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>书面挪威语 (Norsk bokmål)</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="41"/>
        <source>Spanish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>西班牙文</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="45"/>
        <source>French</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="51"/>
        <source>Russian</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="54"/>
        <source>Swedish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>瑞典语</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="57"/>
        <source>Simplified Chinese</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>简体中文</translation>
    </message>
</context>
<context>
    <name>BlurEditor</name>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="53"/>
        <source>Edit Blur Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="63"/>
        <source>These are some values found in Lipstick. They are exposed here for your experimentation, although it is unclear where exactly they are applied, and changing them seems to have very little effect. They are also not part of Ambiences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="67"/>
        <source>Blur Iterations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="77"/>
        <source>Blur Deviation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="87"/>
        <source>Blur Kernel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorField</name>
    <message>
        <source>specify RGB or aRGB value, e.g.</source>
        <translation type="vanished">定义 RGB 或 aRGB 值，例如</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="13"/>
        <source>an %1 value</source>
        <comment>parameter is either RGB or aRGB</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="14"/>
        <source>(the # is optional)</source>
        <translation>(井号 # 可选)</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="14"/>
        <source>e.g.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ColorFilters</name>
    <message>
        <source>Tint</source>
        <translation type="vanished">渲染</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="21"/>
        <source>Red</source>
        <translation>红</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="21"/>
        <source>Tint %1</source>
        <translation>色调 %1</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="29"/>
        <source>Darken</source>
        <translation>暗化</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="37"/>
        <source>Brighten</source>
        <translation>增加亮度</translation>
    </message>
</context>
<context>
    <name>ColorGenerators</name>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="14"/>
        <source>Randomizer</source>
        <translation>随机生成程序</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="16"/>
        <source>Filters</source>
        <translation>过滤器</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="18"/>
        <source>Scheme Generators</source>
        <translation>方案生成程序</translation>
    </message>
</context>
<context>
    <name>ColorRandomizer</name>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="20"/>
        <source>Random</source>
        <translation>随机</translation>
    </message>
    <message>
        <source>Colors</source>
        <translation type="vanished">颜色</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="25"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="39"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="49"/>
        <source>Generated</source>
        <translation>已生成</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="30"/>
        <source>Bright</source>
        <translation>亮色</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="30"/>
        <source>Dark</source>
        <translation>深色</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="44"/>
        <source>Gray</source>
        <translation>灰色</translation>
    </message>
</context>
<context>
    <name>ColorSchemeGenerator</name>
    <message>
        <source>Solarize</source>
        <translation type="vanished">曝光</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="17"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="32"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="39"/>
        <source>%1 Theme</source>
        <comment>parameter is name for a theme, e.g. night, day, random... you might just use the parameter and not translate theme at all.</comment>
        <translation>%1 主题</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="17"/>
        <source>Solarize</source>
        <comment>as in &apos;solarized&apos; color scheme</comment>
        <translation>曝光</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="24"/>
        <source>Generate</source>
        <translation>生成</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="24"/>
        <source>from</source>
        <translation>来自</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="24"/>
        <source>Highlight</source>
        <translation>强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="32"/>
        <source>Night</source>
        <translation>夜间模式</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="52"/>
        <source>%1 Color Blindness</source>
        <comment>%1 is replaced with the variant of color blindness, as in &apos;red-green color-blindness&apos;</comment>
        <translation>%1 色盲</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="52"/>
        <source>R/G</source>
        <comment>abbreviation for red-green color blindness</comment>
        <translation>红绿色盲</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="52"/>
        <source>#%1/%2</source>
        <comment>for lists, as in &apos;number one of four&apos; is #1/4</comment>
        <translation>#%1/%2</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="39"/>
        <source>Summer</source>
        <translation>夏日</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="27"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="35"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="42"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="58"/>
        <source>Applying</source>
        <translation>应用</translation>
    </message>
</context>
<context>
    <name>ColorSelectors</name>
    <message>
        <source>Color</source>
        <translation type="vanished">颜色</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">原色</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">间色</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">强调色</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">第二强调色</translation>
    </message>
</context>
<context>
    <name>ColorSlider</name>
    <message>
        <location filename="../qml/components/controls/ColorSlider.qml" line="66"/>
        <source>Adjust sliders, tap to reset</source>
        <translation>调整滑块，点击以重置</translation>
    </message>
</context>
<context>
    <name>ColorSliders</name>
    <message>
        <source>Color</source>
        <translation type="vanished">颜色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="15"/>
        <source>Primary Color</source>
        <translation>原色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="21"/>
        <source>Secondary Color</source>
        <translation>次色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="28"/>
        <source>Highlight Color</source>
        <translation>强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="35"/>
        <source>Secondary Highlight Color</source>
        <translation>次强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="42"/>
        <source>Background Highlight Color</source>
        <translation>背景强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="49"/>
        <source>Dim Highlight Color</source>
        <translation>昏暗强调色</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">背景强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="56"/>
        <source>Background Glow Color</source>
        <translation>背景发光色</translation>
    </message>
</context>
<context>
    <name>ColorSwapper</name>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="70"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="70"/>
        <source>Swap</source>
        <translation>交换程序</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="91"/>
        <location filename="../qml/components/ColorSwapper.qml" line="118"/>
        <source>Primary Color</source>
        <translation>原色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="92"/>
        <location filename="../qml/components/ColorSwapper.qml" line="119"/>
        <source>Secondary Color</source>
        <translation>次色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="93"/>
        <location filename="../qml/components/ColorSwapper.qml" line="120"/>
        <source>Highlight Color</source>
        <translation>强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="94"/>
        <location filename="../qml/components/ColorSwapper.qml" line="121"/>
        <source>Secondary Highlight Color</source>
        <translation>次强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="95"/>
        <location filename="../qml/components/ColorSwapper.qml" line="122"/>
        <source>Background Highlight Color</source>
        <translation>背景强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="96"/>
        <location filename="../qml/components/ColorSwapper.qml" line="123"/>
        <source>Dim Highlight Color</source>
        <translation>昏暗强调色</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">背景强调色</translation>
    </message>
</context>
<context>
    <name>ColorTextInput</name>
    <message>
        <source>Color input</source>
        <translation type="vanished">颜色输入</translation>
    </message>
    <message>
        <source>Input value, tap to reset</source>
        <translation type="vanished">输入值，点击以重置</translation>
    </message>
</context>
<context>
    <name>ColorTextInputDialog</name>
    <message>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="16"/>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="32"/>
        <source>Color input</source>
        <translation type="unfinished">颜色输入</translation>
    </message>
</context>
<context>
    <name>ColorTextInputs</name>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">原色</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">次色</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">强调色</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">次强调色</translation>
    </message>
    <message>
        <source>Background Highlight Color</source>
        <translation type="vanished">背景强调色</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">昏暗强调色</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">背景强调色</translation>
    </message>
    <message>
        <source>Background Glow Color</source>
        <translation type="vanished">背景发光色</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">主题颜色</translation>
    </message>
</context>
<context>
    <name>DaemonSettings</name>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="109"/>
        <source>Daemon Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="119"/>
        <source>Enable Watcher daemon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="114"/>
        <source>Watches for %1 changes and applies actions defined below.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="120"/>
        <source>Daemon is </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="120"/>
        <source>active</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="120"/>
        <source>inactive</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="138"/>
        <source>Actions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="144"/>
        <source>Select what happens when an %1 change is detected</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="149"/>
        <source>launch %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="150"/>
        <source>just opens the app</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="160"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="162"/>
        <source>Warning:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="161"/>
        <source>The features below are experimental and do not work reliably.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="169"/>
        <source>apply Top theme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="170"/>
        <source>applies theme from the top %1 shelf</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="177"/>
        <source>apply Night theme</source>
        <translation></translation>
    </message>
    <message>
        <source>ThemeColor</source>
        <translation type="obsolete">主题颜色</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="114"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="144"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="170"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="178"/>
        <source>Ambience</source>
        <translation type="unfinished">氛围</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="178"/>
        <source>taken from the second %1 shelf</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="191"/>
        <source>Night begins:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="205"/>
        <source>Night ends:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <source>Adjust Theme Colors</source>
        <translation type="vanished">调整主题颜色</translation>
    </message>
    <message>
        <source>Current Color Model:</source>
        <translation type="vanished">当前颜色模型:</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">原色</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">间色</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">强调色</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">第二强调色</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="29"/>
        <source>Showroom</source>
        <translation>陈列室</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="29"/>
        <source>Atelier</source>
        <translation type="unfinished">实验室</translation>
    </message>
    <message>
        <source>Input Mode:</source>
        <translation type="vanished">输入模式：</translation>
    </message>
    <message>
        <source>Tap to switch</source>
        <translation type="vanished">点击以切换</translation>
    </message>
    <message>
        <source>gray</source>
        <translation type="vanished">灰色</translation>
    </message>
    <message>
        <source>Apply colors to system</source>
        <translation type="vanished">应用颜色到系统</translation>
    </message>
    <message>
        <source>Reload colors from system</source>
        <translation type="vanished">重新加载系统颜色</translation>
    </message>
    <message>
        <source>Apply Colors to System</source>
        <translation type="vanished">应用颜色到系统</translation>
    </message>
    <message>
        <source>Reload Colors from System</source>
        <translation type="vanished">重新加载系统颜色</translation>
    </message>
    <message>
        <source>Swapper/Copier</source>
        <translation type="vanished">交换程序/复印机</translation>
    </message>
    <message>
        <source>Compute all Colors from Highlight</source>
        <translation type="vanished">根据高光计算所有颜色</translation>
    </message>
    <message>
        <source>Generators</source>
        <translation type="vanished">生成程序</translation>
    </message>
    <message>
        <source>Applying</source>
        <translation type="vanished">应用</translation>
    </message>
    <message>
        <source>Reload Colors from current Theme</source>
        <translation type="vanished">从当前主题重新加载颜色</translation>
    </message>
    <message>
        <source>Reload Colors from System Config</source>
        <translation type="vanished">从系统配置中重新加载颜色</translation>
    </message>
    <message>
        <source>Experimental or dangerous actions</source>
        <translation type="vanished">实验性及危险性操作</translation>
    </message>
    <message>
        <source>Export to Ambience file</source>
        <translation type="vanished">输出到氛围文件</translation>
    </message>
    <message>
        <source>Export to Ambience package</source>
        <translation type="vanished">导出到氛围包</translation>
    </message>
    <message>
        <source>Edit Transparency</source>
        <translation type="vanished">编辑透明度</translation>
    </message>
    <message>
        <source>(not implemented)</source>
        <translation type="vanished">(尚未实施)</translation>
    </message>
    <message>
        <source>Save Theme to current Ambience</source>
        <translation type="vanished">保存主题到当前氛围</translation>
    </message>
    <message>
        <source>Saving</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>Resetting</source>
        <translation type="vanished">重置</translation>
    </message>
    <message>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation type="vanished">重新启动 %1</translation>
    </message>
    <message>
        <source>Restarting</source>
        <translation type="vanished">正在重启</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation type="vanished">正在保存…</translation>
    </message>
    <message>
        <source>Reset all values and restart</source>
        <translation type="vanished">重置全部值并重启</translation>
    </message>
    <message>
        <source>Remove custom values from configuration</source>
        <translation type="vanished">从配置中移除自定义值</translation>
    </message>
    <message>
        <source>Resetting...</source>
        <translation type="vanished">正在重置…</translation>
    </message>
    <message>
        <source>Reset nonstandard values</source>
        <translation type="vanished">重置非标准值</translation>
    </message>
    <message>
        <source>Restart Lipstick</source>
        <translation type="vanished">重启 Lipstick</translation>
    </message>
    <message>
        <source>Restarting...</source>
        <translation type="vanished">正在重置…</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">帮助</translation>
    </message>
    <message>
        <source>Sliders</source>
        <translation type="vanished">滑块</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">文本</translation>
    </message>
    <message>
        <source>Swapper</source>
        <translation type="vanished">对换程序</translation>
    </message>
    <message>
        <source>Randomizer</source>
        <translation type="vanished">随机生成器</translation>
    </message>
    <message>
        <source>Jolla Original</source>
        <translation type="vanished">Jolla 原生</translation>
    </message>
    <message>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation type="vanished">手册</translation>
    </message>
    <message>
        <source>Open Ambience Settings</source>
        <translation type="vanished">打开氛围设置</translation>
    </message>
    <message>
        <source>Apply Colors to current Theme</source>
        <translation type="vanished">应用颜色到当前主题</translation>
    </message>
    <message>
        <source>Applying...</source>
        <translation type="vanished">正在应用…</translation>
    </message>
    <message>
        <source>Reload Colors</source>
        <translation type="vanished">重新加载颜色</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">颜色</translation>
    </message>
    <message>
        <source>Reset Colors</source>
        <translation type="vanished">重置颜色</translation>
    </message>
</context>
<context>
    <name>HelpCredits</name>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>About</source>
        <comment>Help Section</comment>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">版本：</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation type="vanished">版权 :</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">许可证：</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation type="vanished">源代码：</translation>
    </message>
    <message>
        <source>Translations</source>
        <translation type="vanished">翻译</translation>
    </message>
</context>
<context>
    <name>HelpDaemon</name>
    <message>
        <source>Ambience</source>
        <translation type="obsolete">氛围</translation>
    </message>
</context>
<context>
    <name>HelpExport</name>
    <message>
        <source>Export to Ambience package</source>
        <translation type="vanished">导出到氛围包</translation>
    </message>
    <message>
        <source>Notes</source>
        <comment>Help Section</comment>
        <extracomment>as in something to add, not a small piece of paper</extracomment>
        <translation type="vanished">笔记</translation>
    </message>
    <message>
        <source>Notes</source>
        <comment>as in something to add, not a small piece of paper </comment>
        <translation type="vanished">笔记</translation>
    </message>
</context>
<context>
    <name>HelpGeneral</name>
    <message>
        <source>General</source>
        <translation type="vanished">一般</translation>
    </message>
    <message>
        <source>General</source>
        <comment>Help Section</comment>
        <translation type="vanished">一般</translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <source>Use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            When satisfied, tap the area above the slider to set the color.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</source>
        <translation type="vanished">使用下方部分滑块以调整颜色。&lt;br /&gt;            满意之后，点击滑块上方区域以设置颜色。          请在陈列室检查主题显示效果。&lt;br /&gt;            &lt;br /&gt;            当你完成之后，请使用上拉菜单以应用你的创作。</translation>
    </message>
    <message>
        <source>How to Use</source>
        <translation type="vanished">如何使用</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="vanished">一般</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;



          Currently, only four colors can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor, as these are the ones saved by the system.          There are other colors in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;



          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">该应用程序允许你修改当前Lipstick配色方案。它不会改变或创建新氛围。当你修改氛围、Lipstick重启或设备重启之后，使用该软件进行的修改也不会保存。&lt;br /&gt;



当前只有四种颜色可以编辑：原色、间色、强调色及第二强调色。这些都是系统保存的东西。系统也会使用其它颜色。这些颜色都是由系统自动计算的，当前暂时无法修改。lt;br/&gt;&lt;br/&gt;



我们正在努力克服其中的某些限制。</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;



          Currently, only some colors can be edited.          There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;



          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">应用程序允许你修改当前Lipstick配色方案。它不会改变或创建新氛围。当你修改氛围、Lipstick重启或设备重启之后，使用该软件进行的修改也不会保存。&lt;br /&gt;



当前只有四种颜色可以编辑：原色、间色、强调色及第二强调色。这些都是系统保存的东西。系统也会使用其它颜色。这些颜色都是由系统自动计算的，当前暂时无法修改。lt;br/&gt;&lt;br/&gt;



我们正在努力克服其中的某些限制。</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;

          Currently, only some colors can be edited.          There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;

          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">应用程序允许你修改当前 Lipstick 配色方案。它不会改变或创建新氛围。当你修改氛围、Lipstick重启或设备重启之后，使用该软件进行的修改也不会保存。&lt;br /&gt;



当前只有四种颜色可以编辑：原色、间色、强调色及第二强调色。这些都是系统保存的东西。系统也会使用其它颜色。这些颜色都是由系统自动计算的，当前暂时无法修改。lt;br/&gt;&lt;br/&gt;



我们正在努力克服其中的某些限制。</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <translation type="vanished">陈列室</translation>
    </message>
    <message>
        <source>The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colors that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </source>
        <translation type="vanished">首页的顶部区域 (&quot;陈列室&quot;) 不可交互，仅用于显示当前选择的颜色。&lt;br /&gt;  你可以在此预览你的创作。    </translation>
    </message>
    <message>
        <source>The Atelier</source>
        <translation type="vanished">实验室</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.            In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know.&lt;br /&gt;             Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullDown Menu.

          </source>
        <translation type="vanished">在滑块输出模式，使用下方滑块以调整颜色。 在文本输出模式，你可以直接输入颜色值。随机生成程序名副其实，Jolla 原生就像系统设置那样。&lt;br /&gt;  在陈列室检查主题效果。&lt;br /&gt;            &lt;br /&gt; 当你完成之后，请使用下拉菜单。</translation>
    </message>
    <message>
        <source>Tips and Caveats</source>
        <translation type="vanished">建议及附加说明</translation>
    </message>
    <message>
        <source>It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;

          It&apos;s a good idea to store a known-good color sceme in the Cupboard so you can go restore easily.&lt;br /&gt;          &lt;br /&gt;          If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;          &lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;          &lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;          and repeat for all the other colors stored there. &lt;br /&gt;          Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">创建的主题颜色可能使UI的某些部分不可见，特别是不明显的区域，例如虚拟键盘。&lt;br /&gt;

推荐你保存已知良好的颜色方案于壁橱，这样你以后就可以轻松恢复。&lt;br /&gt;          &lt;br /&gt; 如果你完全搞乱了颜色，请使用下拉菜单以重置全部，或使用命令行。 &lt;br /&gt;          &lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;          &lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;          并且重复储存在此的其它全部颜色。 &lt;br /&gt;  从系统设置修改氛围可能也有用。</translation>
    </message>
    <message>
        <source>The top area on the first page (&quot;Showroom&quot;) is non-interactive and just shows the colors that are selected currently.&lt;br /&gt;Here you can preview your creation.</source>
        <translation type="vanished">首页顶部区域（陈列室）不可交互，仅用于显示当前选取的颜色。&lt;br /&gt;你可以在此预览你的创作。</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited. There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;
&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="vanished">该软件允许你修改当前 Lipstick 配色方案。它不会改变或创建新氛围。当你修改氛围、 Lipstick 重启或设备重启之后，使用该软件进行的修改也不会保存。&lt;br/&gt;
当前只有四种颜色可以编辑：原色、间色、强调色及第二强调色。这些都是系统保存的东西。系统也会使用其它颜色。这些颜色都是由系统自动计算的，当前暂时无法修改。&lt;br/&gt;
我们正在努力克服其中的某些限制。</translation>
    </message>
    <message>
        <source>The top area on the first page (&quot;Showroom&quot;) shows the colors that are selected currently using various UI elements.&lt;br /&gt;Here you can preview your creation.&lt;br /&gt;Tapping on either area will hide it. To unhide, tap the title header.</source>
        <translation type="vanished">首页顶部区域（陈列室）显示当前选择用于各种UI元素的颜色。&lt;br /&gt;你可以在此预览你的创作。&lt;br /&gt;点击任意区域将会隐藏。如果想要取消隐藏，请点击标题标头。</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors. In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know. Swapper lets you change color definitions.&lt;br /&gt;
Check what your theme will look like in the Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="vanished">在滑块输出模式，使用下方滑块以调整颜色。 在文本输出模式，你可以直接输入颜色值。随机生成程序名副其实，Jolla 原生就像系统设置那样。&lt;br /&gt;在陈列室检查主题效果。&lt;br /&gt;&lt;br /&gt; 当你完成之后，请使用下拉菜单。</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="vanished">该区域允许你储存你创建的调色板以便之后重复使用。 这里有个全局壁橱，还有一个特别针对当前氛围。&lt;br /&gt; 请注意只有系统氛围拥有名字。自定义氛围目前会以匿名形式显示。</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla 氛围只定义了四种颜色：原色（根据氛围方案是深色还是亮色）、次色（主色不透明一点）、加高光和次高光。这些都是在氛围设置中选择的颜色。
该软件允许你编辑这四种颜色以外的其它颜色，但它们不会受到氛围修改和 Lipstick 重启的影响，因为它们存储在 dconf 数据库中并保持不变。这意味着一旦通过软件应用，它们将始终保持不变，直到你在软件中再次更改它们。如果有必要，你可以使用下面文档中的重置选项来摆脱它们。
&lt;br /&gt;
当切换编辑模式、将颜色应用到系统或从壁橱中提取调色板时，该软件经常会对颜色感到困惑。如果发生这种情况，请尝试从系统中重新加载，这在大多数情况下是有帮助的。&lt;br /&gt;
&lt;br /&gt;
创建颜色方案可能会使部分交换界面无法阅读。特别是对于一些不明显的区域，比如虚拟键盘。&lt;br /&gt;
在橱柜里存放一个已知好的配色方案，这样你就可以很方便地去还原。
&lt;br /&gt;
如果你把颜色弄乱了，请使用上拉菜单选项来重置一切，或者使用命令行：&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt; 。
并重复应用于存储的所有其它颜色。&lt;br /&gt;
从 “系统设置 ”中更换氛围也可能会有用。</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">版本：</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation type="vanished">版权：</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">许可证 :</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation type="vanished">源代码：</translation>
    </message>
    <message>
        <source>Translations:</source>
        <translation type="vanished">翻译：</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本：</translation>
    </message>
    <message>
        <source>Copyright: </source>
        <translation type="vanished">版权：</translation>
    </message>
    <message>
        <source>License: </source>
        <translation type="vanished">许可证：</translation>
    </message>
    <message>
        <source>Source Code: </source>
        <translation type="vanished">源代码：</translation>
    </message>
    <message>
        <source>In Slider Input Mode, use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            In Text Input Mode, you can enter color values directly.&lt;br /&gt;



            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.



          </source>
        <translation type="vanished">在滑块输出模式，使用下方滑块以调整颜色。&lt;br /&gt;            在文本模式，你可以直接输入颜色值。&lt;br /&gt;



            在陈列室检查你的方案效果。&lt;br /&gt;            &lt;br /&gt;  当你完成之后，你可以通过上拉菜单应用你的当前创作。          



          </translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <translation type="vanished">壁橱</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.             There is one global Cupboard, and one specific for the current ambience.&lt;br /&gt;             Note that only system-wide Ambiences have a name, custon ones will show as anonymous (for now)</source>
        <translation type="vanished">该区域允许你储存你创建的调色板以便之后重复使用。 这里有个全局壁橱，特别针对当前氛围。&lt;br /&gt; 请注意只有系统氛围拥有名字。自定义氛围目前会以匿名形式显示。</translation>
    </message>
    <message>
        <source>General Information</source>
        <translation type="vanished">一般信息</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="vanished">贷方</translation>
    </message>
    <message>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation type="obsolete">手册</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="65"/>
        <source>Select a chapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="66"/>
        <source>Tap to switch</source>
        <translation type="unfinished">点击以切换</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="73"/>
        <source>General Information</source>
        <comment>Help Index</comment>
        <translation>一般信息</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="74"/>
        <source>About the UI</source>
        <comment>Help Index</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="75"/>
        <source>About Colors</source>
        <comment>Help Index</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="76"/>
        <source>Exporting Ambiences</source>
        <comment>Help Index</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="77"/>
        <source>Watcher Daemon</source>
        <comment>Help Index</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="78"/>
        <source>How it works</source>
        <comment>Help Index</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="79"/>
        <source>Limitations , Tips etc.</source>
        <comment>Help Index</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="102"/>
        <source>View %1</source>
        <comment>User Manual display language</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="102"/>
        <source>English</source>
        <translation type="unfinished">简体中文</translation>
    </message>
    <message>
        <source>Credits</source>
        <comment>Help Index</comment>
        <translation type="vanished">贷方</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="59"/>
        <source>Handbook</source>
        <comment>Help Index</comment>
        <extracomment>User Manual, Application Manual, Handbook, Howto</extracomment>
        <translation>手册</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="65"/>
        <source>Chapter %1:</source>
        <comment>Help Index Chapter Prefix</comment>
        <translation></translation>
    </message>
</context>
<context>
    <name>HelpTips</name>
    <message>
        <source>Tips and Caveats</source>
        <translation type="vanished">建议及附加说明</translation>
    </message>
    <message>
        <source>Tips and Caveats</source>
        <comment>Help Section</comment>
        <translation type="vanished">建议及附加说明</translation>
    </message>
</context>
<context>
    <name>HelpUIMain</name>
    <message>
        <source>The Showroom</source>
        <translation type="vanished">陈列室</translation>
    </message>
    <message>
        <source>The Atelier</source>
        <translation type="vanished">实验室</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <comment>Help Section</comment>
        <translation type="vanished">陈列室</translation>
    </message>
    <message>
        <source>The Atelier</source>
        <comment>Help Section</comment>
        <translation type="vanished">实验室</translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <comment>Help Section</comment>
        <translation type="vanished">壁橱</translation>
    </message>
    <message>
        <source>Put on Shelf</source>
        <translation type="vanished">放上架子</translation>
    </message>
    <message>
        <source>Take to Lab</source>
        <translation type="vanished">放入实验室</translation>
    </message>
    <message>
        <source>Menus</source>
        <comment>Help Section</comment>
        <translation type="vanished">菜单</translation>
    </message>
    <message>
        <source>Menus</source>
        <translation type="vanished">菜单</translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <translation type="vanished">壁橱</translation>
    </message>
</context>
<context>
    <name>LabPage</name>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="38"/>
        <source>Atelier</source>
        <translation type="unfinished">实验室</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="38"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="38"/>
        <source>Cupboards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="43"/>
        <source>Current Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="48"/>
        <source>Editor</source>
        <comment>tool/editing mode</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="49"/>
        <source>Tap to switch</source>
        <translation type="unfinished">点击以切换</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="57"/>
        <source>Sliders</source>
        <translation type="unfinished">滑块</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="58"/>
        <source>Swapper/Copier</source>
        <translation type="unfinished">交换程序/复印机</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="59"/>
        <source>Generators</source>
        <translation type="unfinished">生成程序</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="60"/>
        <source>Jolla Original</source>
        <translation type="unfinished">Jolla 原生</translation>
    </message>
</context>
<context>
    <name>LoadAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="36"/>
        <source>Load</source>
        <translation type="unfinished">读取</translation>
    </message>
    <message>
        <source>Load File</source>
        <translation type="vanished">加载文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="97"/>
        <source>Load Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Take to Lab</source>
        <translation type="obsolete">放入实验室</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="97"/>
        <source>Take to Atelier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="115"/>
        <source>Here you can load a .ambience (JSON) file.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="119"/>
        <source>current</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="125"/>
        <source>file data</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="131"/>
        <source>File name </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="133"/>
        <source>Click to select</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="140"/>
        <source>File content</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="149"/>
        <source>No file loaded</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LootBoxItem</name>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="27"/>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="32"/>
        <source>ThemeColor</source>
        <translation>主题颜色</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="31"/>
        <source>A Lootbox was delivered!</source>
        <translation>已送出 Lootbox！</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="32"/>
        <source>now has more shelves!</source>
        <translation>现在拥有很多架子！</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="32"/>
        <source>Your persistence has been rewarded.</source>
        <translation>你的坚持不懈得到了回报。</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="33"/>
        <source>Your persistence has been rewarded!</source>
        <translation>你的坚持不懈得到了回报！</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="43"/>
        <source>Purchase Options</source>
        <translation>购买选项</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="52"/>
        <source>Payment* received!</source>
        <translation>已收到支付！</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="52"/>
        <source>Buy more shelves</source>
        <translation>购买更多架子</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="57"/>
        <source>Using Jolla Shop credentials to buy Storage Lootbox</source>
        <translation>使用 Jolla 商店凭证以购买储存Lootbox。</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="74"/>
        <source>Thank you for your purchase!&lt;br /&gt;Your extra shelves will be delivered in the next update!</source>
        <translation>感谢你的购买！你的额外架子会在下次更新送出！</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="88"/>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;…or did it?</source>
        <translation>*) 该软件没有内购服务，Jolla 商店也没有 Lootbox 。这只是个玩笑。&lt;br /&gt;你并没有捐赠软件。&lt;br /&gt;实际上什么也没有发生，不是吗？</translation>
    </message>
    <message>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</source>
        <translation type="vanished">*) 该软件没有内购服务，Jolla 商店也没有 Lootbox 。这只是个玩笑。&lt;br /&gt;你没有捐赠软件。&lt;br /&gt;实际上什么也没有发生 不是吗？</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="9"/>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation type="unfinished">手册</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="12"/>
        <source>Advanced…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="15"/>
        <source>Export to Ambience package</source>
        <translation type="unfinished">导出到氛围包</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="18"/>
        <source>Apply Colors to System</source>
        <translation type="unfinished">应用颜色到系统</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="19"/>
        <source>Applying</source>
        <translation type="unfinished">应用</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="21"/>
        <source>Reload Colors from System</source>
        <translation type="unfinished">重新加载系统颜色</translation>
    </message>
</context>
<context>
    <name>SaveAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="53"/>
        <source>Export Functions</source>
        <translation>导出功能</translation>
    </message>
    <message>
        <source>Here you can export your creation to a .ambience (json) file.&lt;br /&gt;
                      This is not terribly useful at the moment, but you can use that file to build your own ambiences by adding an image, optionally some sounds, and package the whole thing as an RPM.
                     </source>
        <translation type="vanished">你可以导出你的创作到.ambience（json）文件。&lt;br /&gt;
虽然该功能目前还不是很有用，但你可以使用该文件以构建你自己的氛围，通过添加壁纸、及某些声音操作，然后打包所有东西为一个RPM文件。
                     </translation>
    </message>
    <message>
        <source>Here you can export your creation to a .ambience (json) file.&lt;br /&gt;
                      This file can be picked up by the ThemeColor® RPM Builder™ companion tool which will package the whole thing as an RPM which you can then install.
                     </source>
        <translation type="vanished">你可以在此导出你的作品到一个.ambience(json)文件。&lt;br /&gt;
该文件能够被主题颜色®RPM 构建程序™选取。它是可以打包所有东西为可以安装的 RPM 安装包的伴随工具。
                     </translation>
    </message>
    <message>
        <source>Here you can export your creation to an .ambience (JSON) file.&lt;br /&gt;
                      This file can be picked up by the ThemeColor® RPM Builder™ companion tool, which will package the whole thing as an RPM you can then install.
                     </source>
        <translation type="vanished">你可以在此导出你的创作到一个.ambience（JSON）文件。&lt;br /&gt;
该文件可以被主题颜色®RPM 构建程序™选取。
伴随工具会打包全部东西为一个可安装的 RPM 文件。
                     </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="67"/>
        <source>Here you can export your creation to a .ambience (JSON) file, create an installable package from it, and optionally install that on your device.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="69"/>
        <source>Ambience</source>
        <translation type="unfinished">氛围</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="69"/>
        <source>First, enter a name for your %1. You can edit the file by clicking the name, and generate a package from it using the Builder Button.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="71"/>
        <source>The .ambience file as well as any built packages will be stored in the %1 folder. (%2)</source>
        <comment>first argument is the short name of the location, second argument is the full path</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="89"/>
        <source>Export to File</source>
        <translation>导出到文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="94"/>
        <source>Ambience Name</source>
        <translation>氛围名称</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="96"/>
        <source>A cool Ambience Name</source>
        <translation>给你的氛围取个好名字</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="191"/>
        <source>Click to Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="202"/>
        <source>Build Package</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click to export</source>
        <translation type="vanished">点击以导出</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="192"/>
        <source>File Name</source>
        <translation>文件名称</translation>
    </message>
    <message>
        <source>Open File</source>
        <translation type="vanished">打开文件</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="74"/>
        <source>Disclaimer</source>
        <translation>免责声明</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="71"/>
        <source>Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="86"/>
        <source>If you decide to share or otherwise distribute the package make sure you respect copy- and other rights for any content included.</source>
        <translation></translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">帮助</translation>
    </message>
</context>
<context>
    <name>SaveSlot</name>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="45"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="46"/>
        <source>Ambience</source>
        <translation>氛围</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="45"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="46"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="48"/>
        <source>Shelf</source>
        <translation>架子</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <source>default</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="45"/>
        <source>night mode</source>
        <translation type="unfinished">夜间模式</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="75"/>
        <source>Take to Atelier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Take to Lab</source>
        <translation type="vanished">放入实验室</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="76"/>
        <source>Put on Shelf</source>
        <translation>放上架子</translation>
    </message>
</context>
<context>
    <name>Saver</name>
    <message>
        <location filename="../qml/pages/Saver.qml" line="103"/>
        <source>Global Cupboard</source>
        <translation>全部壁橱</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="104"/>
        <source>Atelier</source>
        <translation type="unfinished">实验室</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="104"/>
        <source>Ambience</source>
        <translation type="unfinished">氛围</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="132"/>
        <source>Clean out this cupboard</source>
        <translation>清空该壁橱</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="132"/>
        <source>Spring Clean</source>
        <translation>春季扫除</translation>
    </message>
    <message>
        <source>Spring Clean...</source>
        <translation type="vanished">春季清洁…</translation>
    </message>
</context>
<context>
    <name>SaverPlus</name>
    <message>
        <source>anonymous</source>
        <translation type="vanished">匿名</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="20"/>
        <source>User Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="110"/>
        <source>System Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="120"/>
        <source>Ambience Cupboard</source>
        <translation>氛围壁橱</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="121"/>
        <source>Global Cupboard</source>
        <translation type="unfinished">全部壁橱</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="171"/>
        <source>Clean out this cupboard</source>
        <translation>清空该壁橱</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="171"/>
        <source>Spring Clean</source>
        <translation>春季扫除</translation>
    </message>
    <message>
        <source>Spring Clean...</source>
        <translation type="vanished">春季清除…</translation>
    </message>
</context>
<context>
    <name>ShowRoom</name>
    <message>
        <source>A very long line showing Text in </source>
        <translation type="vanished">显示文本的长区域</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">颜色</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">原色</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">次色</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">强调色</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">次强调色</translation>
    </message>
    <message>
        <source>Error Color</source>
        <translation type="vanished">错误颜色</translation>
    </message>
    <message>
        <source>Background Color</source>
        <translation type="vanished">背景色</translation>
    </message>
    <message>
        <source>four kinds of background overlay opacities and colors</source>
        <translation type="vanished">四种背景涂层不透明度及颜色</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">文本</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">背景强调色</translation>
    </message>
    <message>
        <source>Progress Bar Demo</source>
        <translation type="vanished">进度条演示</translation>
    </message>
    <message>
        <source>Tap to restart Demos</source>
        <translation type="vanished">点击以重启演示</translation>
    </message>
    <message>
        <source>Remorse Item Demo</source>
        <translation type="vanished">擦除项目演示</translation>
    </message>
    <message>
        <source>MenuItem</source>
        <translation type="vanished">菜单项</translation>
    </message>
    <message>
        <source>selected</source>
        <translation type="vanished">已选择</translation>
    </message>
    <message>
        <source>disabled</source>
        <translation type="vanished">已禁用</translation>
    </message>
    <message>
        <source>Button</source>
        <translation type="vanished">按钮</translation>
    </message>
    <message>
        <source>Text Elements</source>
        <translation type="vanished">文本元素</translation>
    </message>
    <message>
        <source>UI Elements</source>
        <translation type="vanished">UI元素</translation>
    </message>
</context>
<context>
    <name>ShowRoomMini</name>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="25"/>
        <source>Mini</source>
        <comment>small showroom</comment>
        <translation>微型</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="25"/>
        <source>Showroom</source>
        <translation>陈列室</translation>
    </message>
</context>
<context>
    <name>ShowRoomText</name>
    <message>
        <source>A very long line showing Text in </source>
        <translation type="vanished">显示文本的长区域</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">原色</translation>
    </message>
    <message>
        <source>A very long line showing Text in</source>
        <translation type="vanished">显示文本的长区域</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">间色</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">强调色</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">第二强调色</translation>
    </message>
    <message>
        <source>Error Color</source>
        <translation type="vanished">错误颜色</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">文本</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">背景强调色</translation>
    </message>
    <message>
        <source>Overlay Background Color</source>
        <translation type="vanished">涂层背景色</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">昏暗强调色</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="15"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="22"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="37"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="44"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="51"/>
        <source>%1 text</source>
        <comment>%1 is the name of a color</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="15"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="37"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="51"/>
        <source>Primary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="16"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="23"/>
        <source>No Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="22"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="44"/>
        <source>Secondary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="30"/>
        <source>Highlight Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="38"/>
        <source>Overlay Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="45"/>
        <source>Cover Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="52"/>
        <source>Wallpaper Overlay</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowRoomUI</name>
    <message>
        <source>Progress Bar Demo</source>
        <translation type="vanished">进度条演示</translation>
    </message>
    <message>
        <source>Remorse Item Demo</source>
        <translation type="vanished">擦除项目演示</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="132"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="133"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="134"/>
        <source>MenuItem</source>
        <translation>菜单项</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="134"/>
        <source>selected</source>
        <translation>已选择</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="133"/>
        <source>disabled</source>
        <translation>已禁用</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="20"/>
        <source>Waiting…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="28"/>
        <source>Feeling Remorse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="108"/>
        <source>App</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="109"/>
        <source>Clicked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="110"/>
        <source>Dead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="154"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="155"/>
        <source>Button</source>
        <translation>按钮</translation>
    </message>
</context>
<context>
    <name>TransparencyEditor</name>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="45"/>
        <source>Edit Transparency</source>
        <translation>编辑透明度</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="53"/>
        <source>This will edit the alpha channel of the color. Note that in the Sailfish UI many elements use their own transparency values and are not affected by the alpha channel of the color. Remorse Timers are one such example.</source>
        <translation>此操作编辑颜色的 alpha 通道。请注意，在旗鱼 UI 中，许多元素使用它们各自的透明度值，不受颜色的 alpha 通道的影响。后悔计时器就是一个例子。</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="82"/>
        <source>Highlight Background Opacity</source>
        <translation>强调背景不透明度</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="83"/>
        <source>This is used e.g. for Pulley Menu background.</source>
        <translation>例如用于滑轮菜单背景。</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="85"/>
        <source>Wallpaper Overlay Opacity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="87"/>
        <source>Cover Overlay Opacity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="88"/>
        <source>Transparency of application covers.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="94"/>
        <source>Color Alpha Channel</source>
        <translation>颜色 Alpha 通道</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="95"/>
        <source>Here you can edit Alpha channels for colors that have one.</source>
        <translation>你可以编辑颜色的 Alpha 通道</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="96"/>
        <source>Secondary Color</source>
        <translation>次色</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="obsolete">强调色</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="98"/>
        <source>Secondary Highlight Color</source>
        <translation>次强调色</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="obsolete">背景强调色</translation>
    </message>
</context>
</TS>
