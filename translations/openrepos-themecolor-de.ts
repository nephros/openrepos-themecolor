<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>AdvancedPage</name>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="22"/>
        <source>Advanced Options</source>
        <translation>Erweiterte Optionen</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="31"/>
        <source>System Functions</source>
        <translation>Systemfunktionen</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="34"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="40"/>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation>%1 neu starten</translation>
    </message>
    <message>
        <source>Restart the %1 service. This will close all apps and relaunch the %2</source>
        <translation type="vanished">%1-Service neu starten. Alle Apps werden geschlossen und %2 neu gestartet</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="62"/>
        <source>Lipstick</source>
        <translation>Lipstick</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <source>Home Screen</source>
        <translation>Startbildschirm</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <source>Restart the %1 service. This will close all apps and relaunch the %2</source>
        <comment>arguments are &apos;Lipstick&apos; and &apos;Home Screen&apos;, one id the application name, one is the commonly used name for the same.</comment>
        <translation>%1-Service neu starten. Alle Apps werden geschlossen und %2 neu gestartet</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="36"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="42"/>
        <source>Restarting</source>
        <translation>Starte neu</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="41"/>
        <source>Restart the %1 service. This sometimes fixes color problems</source>
        <translation>Das %1-Service neu starten. Behebt manchmal Probleme mit den Farben</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="41"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="84"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <source>Ambience Settings</source>
        <translation type="vanished">Ambiente-Einstellungen</translation>
    </message>
    <message>
        <source>Open Ambience System Settings</source>
        <translation type="vanished">Öffnet die Ambiente-Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="54"/>
        <source>Config Management</source>
        <translation>Konfigurationsverwaltung</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="56"/>
        <source>Reset Config</source>
        <translation>Konfiguration zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="57"/>
        <source>Reset all color-related configuration values</source>
        <translation>Alle farbbezogenen Konfigurationswerte zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="58"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="63"/>
        <source>Resetting</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="61"/>
        <source>Reset Config and Restart</source>
        <translation>Konfiguration zurücksetzen und App neu starten</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="62"/>
        <source>Reset all color-related configuration values and restart %1</source>
        <translation>Alle farbbezogenen Konfigurationswerte zurücksetzen und %1 neu starten</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="69"/>
        <source>Advanced Editors</source>
        <translation>Fortgeschrittene Editoren</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="71"/>
        <source>Edit Global Blur</source>
        <translation>Unschärfe ändern</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="72"/>
        <source>Edit blur parameters</source>
        <translation>Schärfeparameter ändern</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="81"/>
        <source>Background Service</source>
        <translation>Hintergrunddienst</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="83"/>
        <source>Set up daemon</source>
        <translation>Daemoneinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="84"/>
        <source>Background service which watches for %1 changes and applies actions.</source>
        <translation>Hintergrunddienst, der auf %1-Änderungen achte und Aktionen anwendet.</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="87"/>
        <source>Import/Export</source>
        <translation>Import/Export</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="90"/>
        <source>Load Ambience File</source>
        <translation>Ambientedatei laden</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="91"/>
        <source>Load a color scheme from a .ambience file.</source>
        <translation>Lade ein Farbschema aus einer .ambience-Datei.</translation>
    </message>
</context>
<context>
    <name>AppInfoSingleton</name>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="25"/>
        <source>ThemeColor</source>
        <comment>this will also be used for the app icon</comment>
        <translation>ThemeColor</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="26"/>
        <source>ThemeColor® RPM Builder™</source>
        <comment>this will also be used for the app icon</comment>
        <translation>ThemeColor® RPM Builder™</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="38"/>
        <source>English</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="35"/>
        <source>German</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Deutsch</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="48"/>
        <source>Norwegian Bokmål</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Norwegisch (Bokmål)</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="41"/>
        <source>Spanish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Spanisch</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="45"/>
        <source>French</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Französisch</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="51"/>
        <source>Russian</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Russisch</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="54"/>
        <source>Swedish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Schwedisch</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="57"/>
        <source>Simplified Chinese</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Mandarin (vereinfachte Schrift)</translation>
    </message>
</context>
<context>
    <name>BlurEditor</name>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="53"/>
        <source>Edit Blur Parameters</source>
        <translation>(Un-)Schärfeparameter ändern</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="63"/>
        <source>These are some values found in Lipstick. They are exposed here for your experimentation, although it is unclear where exactly they are applied, and changing them seems to have very little effect. They are also not part of Ambiences.</source>
        <translation>Das sind ein paar Schalter die in Lipstick vorkommen. Sie werden hier für deine Experimente zur Verfügung gestellt, es ist aber nicht klar wo genau sie greifen, und das Verändern scheint recht wenig Effekt zu haben. Sie sind ausserdem nicht Teil von Ambienten.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="67"/>
        <source>Blur Iterations</source>
        <translation>Iterationen</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="77"/>
        <source>Blur Deviation</source>
        <translation>Deviation</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/BlurEditor.qml" line="87"/>
        <source>Blur Kernel</source>
        <translation>Kernel</translation>
    </message>
</context>
<context>
    <name>ColorField</name>
    <message>
        <source>specify RGB or aRGB value, e.g.</source>
        <translation type="vanished">gib einen RGB oder aRGB- Wert ein, z.B.</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="13"/>
        <source>an %1 value</source>
        <comment>parameter is either RGB or aRGB</comment>
        <translation>ein %1-Wert</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="14"/>
        <source>(the # is optional)</source>
        <translation>(das # ist optional)</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="14"/>
        <source>e.g.</source>
        <translation>z.B.</translation>
    </message>
</context>
<context>
    <name>ColorFilters</name>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="21"/>
        <source>Red</source>
        <translation>Rot</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="21"/>
        <source>Tint %1</source>
        <translation>%1 tönen</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="29"/>
        <source>Darken</source>
        <translation>Abdunkeln</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="37"/>
        <source>Brighten</source>
        <translation>Aufhellen</translation>
    </message>
</context>
<context>
    <name>ColorGenerators</name>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="14"/>
        <source>Randomizer</source>
        <translation>Zufallsgenerator</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="16"/>
        <source>Filters</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="18"/>
        <source>Scheme Generators</source>
        <translation>Schemageneratoren</translation>
    </message>
</context>
<context>
    <name>ColorRandomizer</name>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="20"/>
        <source>Random</source>
        <translation>Zufällig</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="25"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="39"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="49"/>
        <source>Generated</source>
        <translation>Erzeugte</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="30"/>
        <source>Bright</source>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="30"/>
        <source>Dark</source>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="44"/>
        <source>Gray</source>
        <translation>Grau</translation>
    </message>
</context>
<context>
    <name>ColorSchemeGenerator</name>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="17"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="32"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="39"/>
        <source>%1 Theme</source>
        <comment>parameter is name for a theme, e.g. night, day, random... you might just use the parameter and not translate theme at all.</comment>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="17"/>
        <source>Solarize</source>
        <comment>as in &apos;solarized&apos; color scheme</comment>
        <translation>Solarisiert</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="24"/>
        <source>Generate</source>
        <translation>Berechnen</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="24"/>
        <source>from</source>
        <translation>aus der</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="24"/>
        <source>Highlight</source>
        <translation>Betonungsfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="32"/>
        <source>Night</source>
        <translation>Nacht</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="52"/>
        <source>#%1/%2</source>
        <comment>for lists, as in &apos;number one of four&apos; is #1/4</comment>
        <translation>No. %1/%2</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="39"/>
        <source>Summer</source>
        <translation>Sommer</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="52"/>
        <source>%1 Color Blindness</source>
        <comment>%1 is replaced with the variant of color blindness, as in &apos;red-green color-blindness&apos;</comment>
        <translation>%1 Farbenfehlsichtigkeit</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="52"/>
        <source>R/G</source>
        <comment>abbreviation for red-green color blindness</comment>
        <translation>R/G</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="27"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="35"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="42"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="58"/>
        <source>Applying</source>
        <translation>Anwenden</translation>
    </message>
</context>
<context>
    <name>ColorSlider</name>
    <message>
        <location filename="../qml/components/controls/ColorSlider.qml" line="66"/>
        <source>Adjust sliders, tap to reset</source>
        <translation>Anpassen mit den Schiebern. Tippen zum Zurücksetzen</translation>
    </message>
</context>
<context>
    <name>ColorSliders</name>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="15"/>
        <source>Primary Color</source>
        <translation>Hauptfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="21"/>
        <source>Secondary Color</source>
        <translation>Zweitfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="28"/>
        <source>Highlight Color</source>
        <translation>Farbe für Betonungen</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="35"/>
        <source>Secondary Highlight Color</source>
        <translation>Zweitfarbe für Betonungen</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="42"/>
        <source>Background Highlight Color</source>
        <translation>Hintergrund für Betonungen</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="49"/>
        <source>Dim Highlight Color</source>
        <translation>Dunkle Betonungsfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="56"/>
        <source>Background Glow Color</source>
        <translation>Leuchtfarbe</translation>
    </message>
</context>
<context>
    <name>ColorSwapper</name>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="70"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="70"/>
        <source>Swap</source>
        <translation>Tauschen</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="91"/>
        <location filename="../qml/components/ColorSwapper.qml" line="118"/>
        <source>Primary Color</source>
        <translation>Hauptfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="92"/>
        <location filename="../qml/components/ColorSwapper.qml" line="119"/>
        <source>Secondary Color</source>
        <translation>Zweitfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="93"/>
        <location filename="../qml/components/ColorSwapper.qml" line="120"/>
        <source>Highlight Color</source>
        <translation>Betonungsfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="94"/>
        <location filename="../qml/components/ColorSwapper.qml" line="121"/>
        <source>Secondary Highlight Color</source>
        <translation>Zweitfarbe für Betonungen</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="95"/>
        <location filename="../qml/components/ColorSwapper.qml" line="122"/>
        <source>Background Highlight Color</source>
        <translation>Hintergrund für Betonungen</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="96"/>
        <location filename="../qml/components/ColorSwapper.qml" line="123"/>
        <source>Dim Highlight Color</source>
        <translation>Dunkle Betonungsfarbe</translation>
    </message>
</context>
<context>
    <name>ColorTextInput</name>
    <message>
        <source>Color input</source>
        <translation type="vanished">Farbeingabe</translation>
    </message>
    <message>
        <source>Input value, tap to reset</source>
        <translation type="vanished">Wert eingeben. Tippen zum Zurücksetzen</translation>
    </message>
</context>
<context>
    <name>ColorTextInputDialog</name>
    <message>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="16"/>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="32"/>
        <source>Color input</source>
        <translation>Farbeingabe</translation>
    </message>
</context>
<context>
    <name>ColorTextInputs</name>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Hauptfarbe</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Zweitfarbe</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Betonungsfarbe</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Zweitfarbe für Betonungen</translation>
    </message>
    <message>
        <source>Background Highlight Color</source>
        <translation type="vanished">Hintergrund für Betonungen</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">Dunkle Betonungsfarbe</translation>
    </message>
    <message>
        <source>Background Glow Color</source>
        <translation type="vanished">Leuchtfarbe</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">ThemeColor</translation>
    </message>
</context>
<context>
    <name>DaemonSettings</name>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="109"/>
        <source>Daemon Settings</source>
        <translation>Daemon-Optionen</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="119"/>
        <source>Enable Watcher daemon</source>
        <translation>Daemon aktivieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="114"/>
        <source>Watches for %1 changes and applies actions defined below.</source>
        <translation>Wartet auf Änderungen des %1 und führt die unten angegebenen Aktionen aus.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="120"/>
        <source>Daemon is </source>
        <translation>Daemon ist </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="120"/>
        <source>active</source>
        <translation>aktiviert</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="120"/>
        <source>inactive</source>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="138"/>
        <source>Actions</source>
        <translation>Aktionen</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="144"/>
        <source>Select what happens when an %1 change is detected</source>
        <translation>Bestimme, was passieren soll, wenn eine Änderung des %1 stattfindet</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="149"/>
        <source>launch %1</source>
        <translation>%1 starten</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="150"/>
        <source>just opens the app</source>
        <translation>öffnet nur die App</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="160"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="162"/>
        <source>Warning:</source>
        <translation>Achtung!</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="161"/>
        <source>The features below are experimental and do not work reliably.</source>
        <translation>The folgenden Features sind unter Entwicklung unf funktionieren noch nicht zuverlässig.</translation>
    </message>
    <message>
        <source>The features below are a developer preview and do not work.</source>
        <translation type="vanished">The folgenden Features sind unter Entwicklung unf funktionieren noch nicht.</translation>
    </message>
    <message>
        <source>You can configure them here, but the daemon does not support them yet.</source>
        <translation type="vanished">Du kannst hier Einstellungen treffen, aber der Daemon unterstützt sie noch nicht.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="169"/>
        <source>apply Top theme</source>
        <translation>Standard-Schema anwenden</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="170"/>
        <source>applies theme from the top %1 shelf</source>
        <translation>wendet das Schema des obersten Regals an</translation>
    </message>
    <message>
        <source>launch Application</source>
        <translation type="vanished">starte die App</translation>
    </message>
    <message>
        <source>just opens %1</source>
        <translation type="vanished">öffnet %1</translation>
    </message>
    <message>
        <source>apply Custom theme</source>
        <translation type="vanished">Farbschema anwenden</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="177"/>
        <source>apply Night theme</source>
        <translation>Nachtfarbschema anwenden</translation>
    </message>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">ThemeColor</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="114"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="144"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="170"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="178"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <source>taken from the top %1 shelf</source>
        <translation type="vanished">vom obersten %1-Regalbrett</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="178"/>
        <source>taken from the second %1 shelf</source>
        <translation>vom zweiten %1-Regalbrett</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="191"/>
        <source>Night begins:</source>
        <translation>Die Nacht beginnt:</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="205"/>
        <source>Night ends:</source>
        <translation>Die Nacht endet:</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <source>Adjust Theme Colors</source>
        <translation type="vanished">Farbschema-Editor</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="29"/>
        <source>Atelier</source>
        <translation>Atelier</translation>
    </message>
    <message>
        <source>Tap to switch</source>
        <translation type="vanished">Tippen zum Umstellen</translation>
    </message>
    <message>
        <source>Input Mode:</source>
        <translation type="vanished">Eingabemodus:</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="29"/>
        <source>Showroom</source>
        <translation>Auslage</translation>
    </message>
    <message>
        <source>Swapper/Copier</source>
        <translation type="vanished">Tauschen/Kopieren</translation>
    </message>
    <message>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation type="vanished">Handbuch</translation>
    </message>
    <message>
        <source>Advanced…</source>
        <translation type="vanished">Erweitert…</translation>
    </message>
    <message>
        <source>Open Ambience Settings</source>
        <translation type="vanished">Ambienteeinstellungen öffnen</translation>
    </message>
    <message>
        <source>Applying</source>
        <translation type="vanished">Anwenden</translation>
    </message>
    <message>
        <source>Apply Colors to System</source>
        <translation type="vanished">Farben aufs System anwenden</translation>
    </message>
    <message>
        <source>Generators</source>
        <translation type="vanished">Generatoren</translation>
    </message>
    <message>
        <source>Set up daemon</source>
        <translation type="vanished">Daemoneinstellungen</translation>
    </message>
    <message>
        <source>Export to Ambience package</source>
        <translation type="vanished">Ambiente Paket exportieren</translation>
    </message>
    <message>
        <source>Resetting</source>
        <translation type="vanished">Zurücksetzen</translation>
    </message>
    <message>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation type="vanished">%1 neu starten</translation>
    </message>
    <message>
        <source>Restarting</source>
        <translation type="vanished">Starte neu</translation>
    </message>
    <message>
        <source>Reload Colors from System</source>
        <translation type="vanished">Farben vom System holen</translation>
    </message>
    <message>
        <source>Experimental or dangerous actions</source>
        <translation type="vanished">Experimentelle oder gefährliche Aktionen</translation>
    </message>
    <message>
        <source>Reset all values and restart</source>
        <translation type="vanished">Alle Werte zurücksetzen und neu starten</translation>
    </message>
    <message>
        <source>Reset nonstandard values</source>
        <translation type="vanished">Nicht-Standardwerte zurücksetzen</translation>
    </message>
    <message>
        <source>Sliders</source>
        <translation type="vanished">Schieber</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Text</translation>
    </message>
    <message>
        <source>Jolla Original</source>
        <translation type="vanished">Original Jolla</translation>
    </message>
</context>
<context>
    <name>HelpColors</name>
    <message>
        <source>The four basic Colors</source>
        <comment>Help Section</comment>
        <translation type="vanished">Die vier Hauptfarben</translation>
    </message>
    <message>
        <source>&lt;p&gt;Jolla Ambiences define four colors which are used for the various text elements around the UI.:&lt;br /&gt;</source>
        <translation type="vanished">&lt;p&gt;Jolla Ambenten besitzen vier definiterte Farben, die in verschiedenen Textelementen in der Benutzeroberfläche verwendet werden:&lt;br /&gt;</translation>
    </message>
    <message>
        <source>primary, for most interactive elements (it is black or white depending on Ambience Scheme)</source>
        <translation type="vanished">Hauptfarbe, für die meisten interaktiven Elemente (schwarz oder weiss je nach Ambiente-Schema)</translation>
    </message>
    <message>
        <source>secondary, per default derived from primary by making it a little less opaque, is used for inactive elements, and most text</source>
        <translation type="vanished">Zweitfarbe, die von der Hauptfarbe abgeleitet und ein wenig duchsichtiger ist, wird für inaktive Elemente und für den Großteil der Texte verwendet</translation>
    </message>
    <message>
        <source>highlight, which is what you selected in the Ambience Settings, colors active or activated elements</source>
        <translation type="vanished">Betonung, die Farbe die in den Ambienteneinstellungen gesetzt wird, färbt aktive oder aktivierte Elemente</translation>
    </message>
    <message>
        <source>secondary highlight, derived through some magic from the highlight color.</source>
        <translation type="vanished">Zweitfarbe für Betonungen, die durch Magie von der Betonungsfarbe abgeleitet wird.</translation>
    </message>
    <message>
        <source>Additional Colors</source>
        <comment>Help Section</comment>
        <translation type="vanished">Weitere Farben</translation>
    </message>
    <message>
        <source>
&lt;p&gt;
Other colors used:&lt;br /&gt;
&lt;ul&gt;
&lt;li&gt;Background and HighlightBackground style the background of Menus, Buttons and the like.&lt;/li&gt;
&lt;li&gt;DimmerHighlightBackground does the same, but for darker areas or high contrast elements.&lt;/li&gt;
&lt;li&gt;GlowColor is used for glassy things like Switches, forward/back indicators etc.&lt;/li&gt;
&lt;li&gt;CoverOverlayColor is/was used for Application Covers, but see note below.&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
&lt;p&gt;
For more information about colors and other aspects of the GUI, see the official &lt;a href=&quot;https://sailfishos.org/develop/docs/silica/qml-sailfishsilica-sailfish-silica-theme.html&quot;&gt;Theme Documentation&lt;/a&gt;
&lt;/p&gt;
&lt;p&gt;
&lt;i&gt;
It must be said that while all those &lt;b&gt;Theme&lt;/b&gt; colors are still defined, as SailfishOS progresses, with UI developers continue to use QML&apos;s&lt;b&gt;palette&lt;/b&gt; property directly.
As we only deal with Theme colors, this unfortunately means more and more UI components stop reacting to those changes.
&lt;/i&gt;
&lt;/p&gt;
</source>
        <translation type="vanished">
&lt;p&gt;
Sonstige verwendete Farben:&lt;br /&gt;
&lt;ul&gt;
&lt;li&gt;Hintergrund (Background) und Betonter Hintergrund (HighlightBackground) bilden den Hintergrund von Menus, Knöpfen usw.&lt;/li&gt;
&lt;li&gt;Dunkle Betonungsfarbe (DimmerHighlightBackground) auch, nur für dunklere Bereiche oder solche mit hohem Kontrast.&lt;/li&gt;
&lt;li&gt;Leuchtfarbe (GlowColor) wird für gläserne Elemente verwendet, Schalter, die Vorwärts- und Zurückmarker etc.&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
&lt;p&gt;
Für mehr Informationen zu den Farben und anderen Aspekte der Oberfläche gibts die offizielle Doku unter &lt;a href=&quot;https://sailfishos.org/develop/docs/silica/qml-sailfishsilica-sailfish-silica-theme.html&quot;&gt;Theme Documentation&lt;/a&gt;
&lt;/p&gt;
</translation>
    </message>
    <message>
        <source>
&lt;p&gt;
Other colors used:&lt;br /&gt;
&lt;ul&gt;
&lt;li&gt;Background and HighlightBackground style the background of Menus, Buttons and the like.&lt;/li&gt;
&lt;li&gt;DimmerHighlightBackground does the same, but for darker areas or high contrast elements.&lt;/li&gt;
&lt;li&gt;GlowColor is used for glassy things like Switches, forward/back indicators etc.&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
&lt;p&gt;
For more information about colors and other aspects of the GUI, see the official &lt;a href=&quot;https://sailfishos.org/develop/docs/silica/qml-sailfishsilica-sailfish-silica-theme.html&quot;&gt;Theme Documentation&lt;/a&gt;
&lt;/p&gt;
</source>
        <translation type="vanished">
&lt;p&gt;
Sontige verwendete Farben:&lt;br /&gt;
&lt;ul&gt;
&lt;li&gt;Hintergrund (Background) und Betonter Hintergrund (HighlightBackground) bilden den Hintergrund von Menus, Knöpfen usw..&lt;/li&gt;
&lt;li&gt;Dunkle Betonungsfarbe (DimmerHighlightBackground) auch, nur für dunklere Bereiche oder solche mit hohem Kontrast.&lt;/li&gt;
&lt;li&gt;Leuchtfarbe (GlowColor) wird für gläserne Elemente verwendet, Schalter, die Vorwärts- und Zurückmarker etc.&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
&lt;p&gt;
Für mehr Informationen zu den Farben und anderen Aspekte der Oberfläche gibts die offizielle Doku unter &lt;a href=&quot;https://sailfishos.org/develop/docs/silica/qml-sailfishsilica-sailfish-silica-theme.html&quot;&gt;Theme Documentation&lt;/a&gt;
&lt;/p&gt;
</translation>
    </message>
</context>
<context>
    <name>HelpCredits</name>
    <message>
        <source>About</source>
        <comment>Help Section</comment>
        <translation type="vanished">Über</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">Version:</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation type="vanished">Urheberrecht:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Lizenz:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation type="vanished">Quellcode:</translation>
    </message>
    <message>
        <source>Translations</source>
        <translation type="vanished">Übersetzung</translation>
    </message>
</context>
<context>
    <name>HelpDaemon</name>
    <message>
        <source>The Watcher Daemon</source>
        <comment>Help Section</comment>
        <translation type="vanished">Der Daemon</translation>
    </message>
    <message>
        <source>Ambience</source>
        <translation type="vanished">Ambiente</translation>
    </message>
    <message>
        <source>&lt;p&gt;The Watcher Daemon is a background service which can take care of some tasks while %1 is not running. Its main purpose is to apply color schemes after the %2 has changed.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Der Daemon ist Hintergrunddienst, der gewisse Aufgaben erledigen wenn %1 nicht gestartet ist. Der Hauptzweck ist, ein Farbschema anzuwenden nachdem das %2 sich geändert hat.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;It can be configured by tapping the &lt;i&gt;Daemon Settings&lt;/i&gt; button on the Advanced page.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Du kannst ihn über die&lt;i&gt;Daemon-Einstellungen&lt;/i&gt; Schlatfläche Erweitert-Seite konfigurieren.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;This is an ever-evolving (and experimental) feature with more functions added each release.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Dieses ist ein (experimentelles) Feature das sich ständig weiterentwickelt; jede Version bringt neue Funktionen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;One upcoming feature is to apply &amp;quot;Night Mode&amp;quot; color schemes at the appropriate time.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Eine der zukünftigen Funktionen ist das Anwenden eines &amp;quot;Nachtmodus&amp;quot;-Schemas zur entspechenden Zeit.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The Watcher Daemon is a background service which can take care of some tasks while %1 is not running.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Der Daemon ist Hintergrunddienst, der gewisse Aufgaben eledigen wenn %1 nicht gestartet ist.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>HelpExport</name>
    <message>
        <source>Exporting Ambiences</source>
        <comment>Help Section</comment>
        <translation type="vanished">Ambiente exportieren</translation>
    </message>
    <message>
        <source>&lt;p&gt;To export an Ambience, after having edited the colors, select the &quot;%1&quot; menu entry to bring up the Export page.&lt;/p&gt;</source>
        <extracomment>argument is the name of the menu entry &apos;Export to Ambience Package&apos;</extracomment>
        <translation type="vanished">&lt;p&gt;Zum Exportieren eines Ambientes wähle die „%1“ Menüoption um zum Export-Bildschirm zu springen.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Export to Ambience package</source>
        <translation type="vanished">Ambiente Paket exportieren</translation>
    </message>
    <message>
        <source>&lt;p&gt;Enter a name for your Ambience there and click the Ambience Name box to save.
This will do two things: write a .ambience file to your Documents folder, and save some settings for the %1 companion tool (see below).&lt;br /&gt;
If you like you can now edit the .ambience file using a text editor, e.g. for adding ringtone and sound information.
&lt;/p&gt;</source>
        <extracomment>the argument is also a translated string, the name of the builder tool</extracomment>
        <translation type="vanished">&lt;p&gt;Gib deinem Ambiente einen Namen und tippe auf die Dateinamensbox zum Speichern.
Jetzt geschehen zwei Dinge: eine .ambience Datei wird in denen Dokumente Ordner geschrieben, und ein paar Konfigurationsdaten für %1 (s.u.) gespeichert.&lt;br /&gt;
Wenn du möchtest kannst du die .ambience Datei mit einem Texteditor bearbeiten, z.B. Klingel- und andere Töne hinzufügen.
&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Enter a name for your Ambience there and hit Enter to save.
This will do two things: write a .ambience file to your Documents folder, and save some settings for the %1 companion tool (see below).&lt;br /&gt;
If you like you can now edit the .ambience file using a text editor, e.g. for adding ringtone and sound information.
&lt;/p&gt;</source>
        <extracomment>the argument is also a translated string, the name of the builder tool</extracomment>
        <translation type="vanished">&lt;p&gt;Gib deinem Ambiente einen Namen und tippe Enter zum Speichern.
Jetzt geschehen zwei Dinge: eine .ambience Datei wird in denen Dokumente Ordner geschrieben, und ein paar Konfigurationsdaten für %1 (s.u.) gespeichert.&lt;br /&gt;
Wenn du möchtest kannst du die .ambience Datei mit einem Texteditor bearbeiten, z.B. Klingel- und andere Töne hinzufügen.
&lt;/p&gt;</translation>
    </message>
    <message>
        <source>The Builder</source>
        <comment>Help Section</comment>
        <translation type="vanished">Der „Builder“</translation>
    </message>
    <message>
        <source>&lt;p&gt;
When ready, launch the %1.
The Builder picks up the information from dconf and the .ambience file to create an RPM file, and prompt to install it if successful.
The package will also be saved in the Documents folder so you can share it with your friends.
&lt;/p&gt;</source>
        <extracomment>the argument is also a translated string, the name of the builder tool</extracomment>
        <translation type="vanished">&lt;p&gt;
Wenn alles bereit ist starte %1.
Der holt sich die Informationen aus dconf und der .ambience Datei, erstellt ein RPM-Paket daraus, und wenns geklappt hat fordert er dich zum Installieren auf.
Das Paket wird außerdem im Dokumenteordner abgelegt, sodaß du es mit deinen Freunden teilen kannst.
&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Notes</source>
        <comment>Help Section</comment>
        <extracomment>as in something to add, not a small piece of paper</extracomment>
        <translation type="vanished">Anmerkungen</translation>
    </message>
    <message>
        <source>&lt;ul&gt;
&lt;li&gt;The Builder is non-graphical and non-interactive. It needs to be configured through the app as described or it will not work.&lt;/li&gt;
&lt;li&gt;Installing an Ambience will switch it from Anonymous to a named Ambience. This means it will get a new Ambience shelf, which will be empty. Therefore it is a good idea to save the Scheme to the General Shelf.&lt;/li&gt;
&lt;li&gt;While both the .ambience file and the RPM package include most color values, not all are picked up by lipstick when switching Ambiences.  So, after having switched, open the App again and use the Ambience Shelf to load and apply your favourite scheme.&lt;/li&gt;
&lt;/ul&gt;
</source>
        <translation type="vanished">&lt;ul&gt;
&lt;li&gt;Der „Builder“ hat keine Oberfläche und ist nicht interaktiv. Er muß über die App gesteuert werden damit er funktioniert.&lt;/li&gt;
&lt;li&gt;Installiert man ein Ambiente, wechselt es von einem Anonymen zu einem Benannten Ambiente. Das heisst es bekommt eine neue, leere Ambientestellage. Daher wäre es schlau, das Schema zuerst in die allgemeine Stellage zu stellen.&lt;/li&gt;
&lt;li&gt;Obwohl sowohl die .ambience Datei als auch das RPM-Paket die meisten der Farbwerte enthalten, werden nicht alle von Lipstick ausgewertet wenn man das Ambiente wechselt. Nach einem Wechsel öffnest du am Besten wieder die App und ladest und applizierst dein bevorzugtes Schema.&lt;/li&gt;
&lt;/ul&gt;
</translation>
    </message>
</context>
<context>
    <name>HelpGeneral</name>
    <message>
        <source>General</source>
        <comment>Help Section</comment>
        <translation type="vanished">Allgemeines</translation>
    </message>
    <message>
        <source>&lt;p&gt;
This application allows you to modify the current color scheme of Lipstick (a.k.a. Home Screen).
&lt;/p&gt;
&lt;p&gt;
It can not (yet) however, change saved Ambiences directly. Applied changes will not survive an Ambience change, Lipstick restart, or device reboot.&lt;br/&gt;
However, using the Shelves feature you can restore schemes you previously saved and re-apply them afterwards.  &lt;/p&gt;
Also, by exporting changes to an installable package, changes can be made permanent.
&lt;/p&gt;
&lt;p&gt;
Currently only some colors can be edited. There are others in use by the system which are usually autocomputed from the basic four and can not be modified.&lt;br/&gt;
&lt;/p&gt;
&lt;p&gt;
We are working on overcoming some of these limitations, but some things would require patching the system UI elements which is beyond the scope of this app.
&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;
Die App erlaubt das Verändern des momentanen Farbschemas von Lipstick.
&lt;/p&gt;
&lt;p&gt;
Sie kann aber (noch) keine Ambiente direkt ändern und speichern. Die Änderungen überleben auch keinen Ambientewechel, Lipstick- oder Geräteneustart.&lt;br/&gt;
Aber mittels der Stellagen kannst du veränderte Paletten ablegen und später wiederverwenden und auch neu anwenden.&lt;/p&gt;
Außerdem kann man ein installierbares Paket erzeugen, das die Änderungen permanent macht.
&lt;/p&gt;
&lt;p&gt;
Zur Zeit können nur einige Farben geändert werden.
Das System nutzt auch einige weitere die von den Basisfarben weg berechnet werden, und nicht modifiziert werden können.
&lt;br/&gt;
&lt;/p&gt;
&lt;p&gt;
An der Lösung dieser und anderer Unzulänglichkeiten wird gearbeitet. Allerdings wäre für manche Änderungen ein patchen der Systemkomponenten notwendig, und das liegt außerhalb des Anwendungsbereiches dieser App.
&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;
This application allows you to modify the current color scheme of Lipstick (a.k.a. Home Screen).
&lt;/p&gt;
&lt;p&gt;
It can not (yet) however, change saved Ambiences directly. Applied changes will not survive an Ambience change, Lipstick restart, or device reboot.&lt;br/&gt;
However, using the Shelves feature you can restore schemes you previously saved and re-apply them afterwards.
Also, by exporting changes to an installable package, changes can be made permanent.
&lt;/p&gt;
&lt;p&gt;
Currently only some colors can be edited. There are others in use by the system which are usually autocomputed from the basic four and can not be modified.&lt;br/&gt;
&lt;/p&gt;
&lt;p&gt;
We are working on overcoming some of these limitations, but some things would require patching the system UI elements which is beyond the scope of this app.
&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;
Mit dieser Anwendung kann man das aktuelle Farbschema von Lipstick (auch bekannt als Home Screen) ändern.
&lt;/p&gt;
&lt;p&gt;
Gespeicherte Ambiences können jedoch (noch) nicht direkt geändert werden. Angewandte Änderungen überleben einen Ambience-Wechsel, einen Lipstick-Neustart oder einen Geräte-Neustart nicht.&lt;br/&gt;
Mit der Funktion „Regale“ können Sie jedoch zuvor gespeicherte Schemata wiederherstellen und sie anschließend erneut anwenden.
Durch den Export von Änderungen in ein installierbares Paket können die Änderungen auch dauerhaft gemacht werden.
&lt;/p&gt;
&lt;p&gt;
Derzeit können nur einige Farben bearbeitet werden. Es gibt andere, die vom System verwendet werden, die normalerweise automatisch aus den vier Grundfarben berechnet werden und nicht geändert werden können.&lt;br/&gt;
&lt;/p&gt;
&lt;p&gt;
Wir arbeiten daran, einige dieser Einschränkungen zu überwinden, aber einige Dinge würden einen Patch der System-UI-Elemente erfordern, was den Rahmen dieser Anwendung sprengen würde.
&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Basic Workflow</source>
        <comment>Help Section</comment>
        <translation type="vanished">Grundlegende Arbeitweise</translation>
    </message>
    <message>
        <source>
&lt;p&gt;The basic workflow for re-coloring an existing Ambience or creating a new one would be something like this:&lt;/p&gt;

&lt;ol&gt;
&lt;li&gt;switch to an Ambience of your liking using the usual methods&lt;/li&gt;
&lt;li&gt;launch ThemeColor&lt;/li&gt;
&lt;li&gt;using the Sliders editing mode, adjust the four basic colors (primary, secondary, highlight and secondary highlight) to your liking.&lt;/li&gt;
&lt;li&gt;use the PullDown menu &lt;i&gt;Apply Colors to System&lt;/i&gt; to apply to the current theme&lt;/li&gt;
&lt;li&gt;explore the Sailfish UI a bit to make sure all looks right. Always check the VKB,  that can easily become unreadable&lt;/li&gt;
&lt;li&gt;go back to ThemeColor, and use the Shelves to store your colors for later reuse from within the app.&lt;/li&gt;
&lt;li&gt;adjust if necessary and repeat the last two steps above until everything looks pretty&lt;/li&gt;
&lt;li&gt;(optional:) to export and create an ambience RPM, and optionally install it, use the menu item &lt;i&gt;Export Ambience&lt;/i&gt;&lt;/li&gt;
&lt;/ol&gt;

&lt;p&gt; … and done! Your new Ambience should now show up in the usual place in Settings to select.&lt;/p&gt;

&lt;p&gt;If it does not (sometimes it happens) that means that ambienced has crashed.
In that case you can try to use the two &lt;i&gt;Restart&lt;/i&gt; functions on the Advanced page
to restart either Lipstick (the Home Screen), or just ambienced (the Theme engine)).
&lt;/p&gt;</source>
        <translation type="vanished">
&lt;p&gt;Der grundlegende Ablauf um ein existierendes Ambiente anzupassen oder ein neues zu erstellen geht ca. so:&lt;/p&gt;

&lt;ol&gt;
&lt;li&gt;wechsle auf ein Ambiente wie gewohnt&lt;/li&gt;
&lt;li&gt;starte ThemeColor&lt;/li&gt;
&lt;li&gt;Verwende den Schieberegler-Eingabemodus und passe die vier Hauptfarben (Haupt-, Zweit-, Betonungs- und Zweit-Betonungsfarbe) an.&lt;/li&gt;
&lt;li&gt;verwende das Zieh-Menü &lt;i&gt;Farben aufs System anwenden&lt;/i&gt; um das Schema anzuwenden&lt;/li&gt;
&lt;li&gt;schau dich in der Benutzeroberfläche ein wenig um ob alles gut aussieht. Immer auch das virtuelle Keyboard prüfen, das wird leicht unlesbar&lt;/li&gt;
&lt;li&gt;zurück zu ThemeColor, verwende die Stellagen zum Ablegen und zur späteren Wiederverwendung in der App.&lt;/li&gt;
&lt;li&gt;wiederhole die zwei letzten Schritte bis alles gut aussieht&lt;/li&gt;
&lt;li&gt;(optional:) zum Exportieren und Installieren eines Ambiente-Pakets verwende die Funktion &lt;i&gt;	Ambiente Paket exportieren&lt;/i&gt;&lt;/li&gt;
&lt;/ol&gt;

&lt;p&gt; ... und fertig! Dein neues Ambiente sollte nun am üblichen Ort in den Einstellungen zur Auswahl stehen.&lt;/p&gt;

&lt;p&gt;Falls nicht (das passiert) ist wahrscheinlich ambienced gestorben.
In dem Fall kannst du die &lt;i&gt;Neu Starten&lt;/i&gt; Optionen auf der Erweitert-Seite verwenden.
&lt;/p&gt;</translation>
    </message>
    <message>
        <source>
&lt;p&gt;The basic workflow for re-coloring an existing Ambience or creating a new one would be something like this:&lt;/p&gt;

&lt;ol&gt;
&lt;li&gt;switch to an Ambience of your liking using the usual methods&lt;/li&gt;
&lt;li&gt;launch ThemeColor&lt;/li&gt;
&lt;li&gt;using the Sliders editing mode, adjust the four basic colors (primary, secondary, highlight and secondary highlight) to your liking.&lt;/li&gt;
&lt;li&gt;use the PullDown menu &lt;i&gt;Apply Colors to System&lt;/i&gt; to apply to the current theme&lt;/li&gt;
&lt;li&gt;explore the Sailfish UI a bit to make sure all looks right. Always check the VKB,  that can easily become unreadable&lt;/li&gt;
&lt;li&gt;go back to ThemeColor, and use the Shelves to store your colors for later reuse from within the app.&lt;/li&gt;
&lt;li&gt;adjust if necessary and repeat the last two steps above until eveything looks pretty&lt;/li&gt;
&lt;li&gt;(optional:) to export and create an ambience RPM, use the menu function &lt;i&gt;Export Ambience&lt;/i&gt;&lt;/li&gt;
&lt;li&gt;(optional:) type in a name for your new Ambience, and tap to store&lt;/li&gt;
&lt;li&gt;(optional:) tap the Builder button&lt;/li&gt;
&lt;li&gt;(optional:) that should prompt you to install the new Ambience RPM&lt;/li&gt;
&lt;/ol&gt;

&lt;p&gt; … and done! Your new Ambience should now show up in the usual place in Settings to select.&lt;/p&gt;

&lt;p&gt;If it does not (sometimes it happens) that means that ambienced has crashed.
In that case you can try to use the two &lt;i&gt;Restart&lt;/i&gt; functions in the Pullup menu
from the app to restart either Lipstick (the Home Screen), or just
ambienced (the Theme engine)).
&lt;/p&gt;</source>
        <translation type="vanished">
&lt;p&gt;Der grundlegende Ablauf um ein existierendes Ambiente anzupassen oder ein neues zu erstellen geht ca. so:&lt;/p&gt;

&lt;ol&gt;
&lt;li&gt;wechsle auf ein Ambiente wie gewohnt&lt;/li&gt;
&lt;li&gt;starte ThemeColor&lt;/li&gt;
&lt;li&gt;Verwende den Schieberegler-Eingabemodus und passe die vier Hauptfarben (Haupt-, Zweit-, Betonungs- und Zweit-Betonungsfarbe) an.&lt;/li&gt;
&lt;li&gt;verwende das obere Zieh-Menü &lt;i&gt;Farben aufs System anwenden&lt;/i&gt; um das Schema anzuwenden&lt;/li&gt;
&lt;li&gt;schau dich in der Benutzeroberfläche ein wenig um ob alles gut aussieht. Immer auch das virtuelle Keyboard prüfen, das wird leicht unlesbar&lt;/li&gt;
&lt;li&gt;zurück zu ThemeColor, verwende die Stellagen zum Ablegen und zur späteren Wiederverwendung in der App.&lt;/li&gt;
&lt;li&gt;wiederhole die zwei letzten Schritte bis alles gut aussieht&lt;/li&gt;
&lt;li&gt;(optional:) zum Exportieren und Installieren eines Ambiente-Pakets verwende die Funktion &lt;i&gt;	Ambiente Paket exportieren&lt;/i&gt;&lt;/li&gt;
&lt;li&gt;(optional:) gib dem Ambiente einen Namen und speichere&lt;/li&gt;
&lt;li&gt;(optional:) tippe auf den „Builder“ Knopf&lt;/li&gt;
&lt;li&gt;(optional:) das sollte zum Installieren des Ambiente RPM-Pakets auffordern&lt;/li&gt;
&lt;/ol&gt;

&lt;p&gt; ... und fertig! Dein neues Ambiente sollte nun am üblichen Ort in den Einstellungen zur Auswahl stehen.&lt;/p&gt;

&lt;p&gt;Falls nicht (das passiert) ist wahrscheinlich ambienced gestorben.
In dem Fall kannst du die &lt;i&gt;Neu Starten&lt;/i&gt; Optionen unteren Zieh-Menü verwenden.
&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>HelpHow</name>
    <message>
        <source>Loading and Applying Colors</source>
        <comment>Help Section</comment>
        <translation type="vanished">Laden und Anwenden von Farben</translation>
    </message>
    <message>
        <source>
&lt;p&gt;
        Ambience Colors are stored in several places: the .ambience file for installed Ambiences, a user-local database for user-created Ambiences, and &lt;i&gt;dconf&lt;/i&gt;.
        When an Ambience is loaded, its color values are written to a DConf location, and lipstick reads the values from there and uses it for its Theme.&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
        We use that last part in this application, in that when we &quot;Apply&quot; colors to the system, we simply write to that DConf location, and lipstick notices and adjusts it theme.
        The good thing is, while official Ambiences only define four colors (two of which are always the same in practice), writing additional values to that location works, if they are defined as changable by the Sailfish Theme.
&lt;/p&gt;
&lt;p&gt;
        The dconf path in question is &lt;pre&gt;/desktop/jolla/theme/color&lt;/pre&gt; and can be read from command line using &lt;pre&gt;dconf dump&lt;/pre&gt;
&lt;/p&gt;
</source>
        <translation type="vanished">
&lt;p&gt;
        Ambientefarben werden an mehreren Orten gespeichert: in der .ambience Datei bei installierten Ambienten, einer userspezifischen Datenbank bei usergenerierten Ambienten und &lt;i&gt;dconf&lt;/i&gt;.
        Wird ein Ambiente geladen, werden seine Farben nach DConf geschrieben und Lipstick liest die Werte von dort und verwendet sie für sein Farbschema.&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
        Diesen letzten Schritt nutzen wir in dieser App aus, wenn wir Farben aufs System „anwenden“: wir schreiben einfach nach DConf, und Lipstick kriegt das mit und passt die Farben an.
        Gut ist, daß obwohl die offiziellen Ambiente nur vier Farben definieren, (und zwei davon immmer die selben sind), kann man trotzdem andere Werte nach DConf schreiben und sie werden aufgegriffen, wenn sie als schreibbar im Sailfish Theme definiert sind.
&lt;/p&gt;
&lt;p&gt;
        Der DConf-Pfad ist: &lt;pre&gt;/desktop/jolla/theme/color&lt;/pre&gt; und kann mit folgendm Kommando gelesen werden: &lt;pre&gt;dconf dump&lt;/pre&gt;
&lt;/p&gt;
</translation>
    </message>
    <message>
        <source>App Configuration and State</source>
        <comment>Help Section</comment>
        <translation type="vanished">Konfiguration und Persistenz</translation>
    </message>
    <message>
        <source>
&lt;p&gt;
        Speaking of dconf, the application uses it in its own location to store things like Shelves and information for the builder.&lt;br /&gt;
        Unfortuately, if the user creates and deletes Ambiences a lot, and uses the Ambience Shelf, there might be a lot of stale setting in there which may warrant cleaning out from time to time.
&lt;/p&gt;
&lt;p&gt;
        The location for this is &lt;pre&gt;/org/nephros/openrepos-themecolor/storage&lt;/pre&gt;
&lt;/p&gt;
</source>
        <translation type="vanished">
&lt;p&gt;
        Wenn wir schon bei dconf sind, die App verwendet ihren eigenen Bereich, um Dinge wie Stellageninhalte und Informationen für den Builder zu halten.&lt;br /&gt;
        Dummerweise entstehen viele veraltete und überflüssige Werte, wenn der Benutzer oft Ambiente erstellt und wechselt und das Ambientenregal verwendet. Es sollte also von Zeit zu Zeit ausgemistet werden.
&lt;/p&gt;
&lt;p&gt;
        Der Speicherort ist: &lt;pre&gt;/org/nephros/openrepos-themecolor/storage&lt;/pre&gt;
&lt;/p&gt;
</translation>
    </message>
    <message>
        <source>The Builder</source>
        <comment>Help Section</comment>
        <translation type="vanished">Der „Builder“</translation>
    </message>
    <message>
        <source>
&lt;p&gt;
        The Ambience builder is a shell script launched from a oneshot systemd service with the following mode of operation:
&lt;/p&gt;
&lt;p&gt;
        First it reads DConf values prepared by the App to figure out things like Ambience name and location of files.&lt;br /&gt;
        It then puts them in a temporary directory together with a .spec file. It calls the &lt;pre&gt;rpmbuild&lt;/pre&gt; command which produces (hopfully) a package.&lt;br /&gt;
        If successful, that package is opened, prompting the user to install it.
&lt;/p&gt;
</source>
        <translation type="vanished">
&lt;p&gt;
        Der „Builder“ ist ein shellskript das von einem oneshot systemd service gestartet wird und folgendermassen funktioniert:
&lt;/p&gt;
&lt;p&gt;
        Es liest als Erstes DConf-Werte die von der App vorbereitet wurden, um Dinge wie Hintergrundbild und Ambientenamen herauszufinden.&lt;br /&gt;
        Danach wird alles zusammen mit einer .spec Datei in ein temporäres Verzeichnis kopiert. Es ruft &lt;pre&gt;rpmbuild&lt;/pre&gt; auf, welches (mit etwas Glück) ein Paket schnürt.&lt;br /&gt;
        Wenn das klappt wird das Paket geöffnet und der Benutzer zum Installieren aufgefordert.
&lt;/p&gt;
</translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="65"/>
        <source>Select a chapter</source>
        <translation>Wähle ein Kapitel</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="66"/>
        <source>Tap to switch</source>
        <translation>Tippen zum Umstellen</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="73"/>
        <source>General Information</source>
        <comment>Help Index</comment>
        <translation>Allgemeines</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="74"/>
        <source>About the UI</source>
        <comment>Help Index</comment>
        <translation>Zur Benutzeroberfläche</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="75"/>
        <source>About Colors</source>
        <comment>Help Index</comment>
        <translation>Zu den Farben</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="76"/>
        <source>Exporting Ambiences</source>
        <comment>Help Index</comment>
        <translation>Ambiente exportieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="77"/>
        <source>Watcher Daemon</source>
        <comment>Help Index</comment>
        <translation>Daemon</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="78"/>
        <source>How it works</source>
        <comment>Help Index</comment>
        <translation>Wies funktioniert</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="79"/>
        <source>Limitations , Tips etc.</source>
        <comment>Help Index</comment>
        <translation>Tipps, Tricks, Einschränkungen usw.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="102"/>
        <source>View %1</source>
        <comment>User Manual display language</comment>
        <translation>In %1 anzeigen</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="102"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <source>Credits</source>
        <comment>Help Index</comment>
        <translation type="vanished">Danksagungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="59"/>
        <source>Handbook</source>
        <comment>Help Index</comment>
        <extracomment>User Manual, Application Manual, Handbook, Howto</extracomment>
        <translation>Handbuch</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="65"/>
        <source>Chapter %1:</source>
        <comment>Help Index Chapter Prefix</comment>
        <translation>Kapitel %1:</translation>
    </message>
</context>
<context>
    <name>HelpTips</name>
    <message>
        <source>Tips and Caveats</source>
        <comment>Help Section</comment>
        <translation type="vanished">Tipps und Warnungen</translation>
    </message>
    <message>
        <source>
&lt;p&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.
&lt;/p&gt;
&lt;p&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.
&lt;/p&gt;
</source>
        <translation type="vanished">
&lt;p&gt;
Die App ist hin und wieder verwirrt was die Farben betrifft wenn die Eingabemodi gewechselt werden, Farben auf System appliziert oder von dort geholt, oder in den Stellagen verwaltet werden. Sollte das passieren, versuche die Farben vom System neu zu laden, das sollte helfen.&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
Man kann leicht Farbkombinatinoen erstellen, die Teile der Benutzeroberfläche unlesbar machen. Prüfe im speziellen das virtuelle Keyboard und andere nicht ganz offensichtliche Elemente.&lt;br /&gt;
Es wäre schlau, ein funktionierendes Schema in einem der Stellagen aufzuheben um leicht zurückspringen zu können wenn das passiert.&lt;/p&gt;
&lt;p&gt;
Wenn du die Farben komplett verkorkst hast, verwende das untere Zieh-Menü um alles zurückzusetzen, oder verwende die Kommandozeile:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
…und wiederhole das für alle Farben. &lt;br /&gt;
Das Ambiente in den Einstellungen zu wechseln könnte auch helfen.
&lt;/p&gt;
</translation>
    </message>
</context>
<context>
    <name>HelpUIMain</name>
    <message>
        <source>&lt;p&gt;The main UI consists of the following:
       &lt;ul&gt;
       &lt;li&gt;The Showroom&lt;/li&gt;
       &lt;li&gt;The Atelier&lt;/li&gt;
       &lt;li&gt;The Cupboards&lt;/li&gt;
       &lt;li&gt;PullDown and PushUp Menus&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Die Hauptoberfläche besteht aus:
       &lt;ul&gt;
       &lt;li&gt;der Auslage&lt;/li&gt;
       &lt;li&gt;dem Atelier&lt;/li&gt;
       &lt;li&gt;den Stellagen&lt;/li&gt;
       &lt;li&gt;den Zieh-Menüs&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>The main UI sections</source>
        <comment>Help Section</comment>
        <translation type="vanished">Die Hauptelemente der Oberfläche</translation>
    </message>
    <message>
        <source>&lt;p&gt;The main UI consists of the following:
       &lt;ul&gt;
       &lt;li&gt;The Showroom&lt;/li&gt;
       &lt;li&gt;The Atelier&lt;/li&gt;
       &lt;li&gt;The Cupboards&lt;/li&gt;
       &lt;li&gt;PullDown Menu&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Die Hauptoberfläche besteht aus:
       &lt;ul&gt;
       &lt;li&gt;der Auslage&lt;/li&gt;
       &lt;li&gt;dem Atelier&lt;/li&gt;
       &lt;li&gt;den Stellagen&lt;/li&gt;
       &lt;li&gt;dem Zieh-Menü&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <comment>Help Section</comment>
        <translation type="vanished">Die Auslage</translation>
    </message>
    <message>
        <source>&lt;p&gt;The top area on the first page (%1) shows the colors that are selected currently.
This is so you can preview and check how your creations will look.&lt;/p&gt;
&lt;p&gt;
There are two parts: the top one showing text in various combinations of colors, and the bottom one for the UI elements like buttons etc.
Tapping on either of those parts will hide it. When both are hidden, a miniature version will appear.&lt;br /&gt;
To unhide all again tap the section header.
&lt;/p&gt;</source>
        <extracomment>argument is the translation of &quot;Showroom&quot;</extracomment>
        <translation type="vanished">&lt;p&gt;Der obere Bereich auf der ersten Seite (%1) zeigt die momentan geänderten Farben.
Hier siehst du eine Vorschau, wie deine Kreation aussehen wird.&lt;/p&gt;
&lt;p&gt;
Die Auslage hat zwei Teile: der obere zeigt Text in verschiedenen Farben, der untere Elemente der Benutzeroberfläche wie z.B. Knöpfe usw.
Tippe auf einen der beiden Teile zum Ausblenden. Wenn beide ausgeblendet sind, wird eine Miniaturversion angezeigt.&lt;br /&gt;
Tippe die Titelleiste um das Verstecken rückgängig zu machen.
&lt;/p&gt;</translation>
    </message>
    <message>
        <source>The Atelier</source>
        <comment>Help Section</comment>
        <translation type="vanished">Das Atelier</translation>
    </message>
    <message>
        <source>&lt;p&gt;The next component is the %1. This is the where you edit the various colors.&lt;/p&gt;
&lt;p&gt;
On the top you find the input mode selector.
There are several ways to do this editing, called Input Modes, explained below.
The selector switches between them.
&lt;ul&gt;</source>
        <extracomment>argument is the translation of &quot;Atelier&quot;</extracomment>
        <translation type="vanished">&lt;p&gt;Der nächte Bereich ist das %1. Hier editierst du die unterschiedlichen Farben.&lt;/p&gt;
&lt;p&gt;
Ganz oben findest du den Schalter für den Eingabemodus.
Es gibt mehrere Möglichkeiten des Editierens, genannt Input-Modus, siehe unten.
Der Schalter wechselt zwischen diesen hin und her.
&lt;ul&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;&lt;strong&gt;Slider&lt;/strong&gt; input mode is the main editing mode.
There is a set of sliders for each color you can edit. Use them to adjust the colors using their red, green, blue (RGB) channels. 
Two slim indicators on top of each slider section shows the current, and the edited color, and the hex value will be shown in the area below them. If you tap that area, the sliders will reset to the value of the unedited color.&lt;br /&gt;
The button on the left side opens the Color Selector dialog from Sailfish Silica, in case you want to select a color from there.&lt;br /&gt;
At the bottom below the RGB sliders you will find the Glow editor. This works a little different than the others, allowing you to select the hue of any UI elements that have a glow effect. Below the slider there is a set of buttons that apply either black, white, or the four default colors to the Glow color.</source>
        <translation type="vanished">&lt;li&gt;&lt;strong&gt;Schieberegler&lt;/strong&gt; Input-Modus ist der Hauptmodus.
Es gibt eine Reihe von Schiebereglern für jede Farbe. Benutze die zum Ändern der roten, grünen und blauen (RGB) Farbkanäle. 
Zwei schmale Anziegen über den Reglern zeigen die momentane und die geänderte Farbe an. Darunter wird er hexadezimale Wert dazu angezeigt. Wenn du auf den tippst setzen sich die Regler auf die ursprünglichen Farbwerte zurück.&lt;br /&gt;
Der Kopf links öffner den Sailfish Farbdialog, für den Fall daß du eine Farbe von dort aussuchen willst.&lt;br /&gt;
Unter den RGB Reglern gibts noch den Leuchtfarbeneditor. Der funktioniert etwas anders, er selektiert den Farbton der Elemente mit Leuchteffekt. Unter dem Regler gibts ein paar Knöpfe die diese Farbe auf schwarz, weiss oder eine der vier Hauptfarben anpassen.</translation>
    </message>
    <message>
        <source>&lt;li&gt;&lt;strong&gt;Text&lt;/strong&gt; input mode is similar to the Slider mode, but instead of sliders you enter RGB color values directly as text by tapping the keyboard icon.&lt;/li&gt;</source>
        <translation type="vanished">&lt;li&gt;&lt;strong&gt;Text&lt;/strong&gt; Input-Modus ähnlich wie die Schieberegler, nur statt Reglern verwendest du Texteingabe für den RGB Wert. Tippe dazu auf das Keyboardsymbol.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;&lt;strong&gt;Slider&lt;/strong&gt; input mode is the main editing mode.
There is a set of sliders for each color you can edit. Use them to adjust the colors using their red, green, blue (RGB) channels. 
Two slim indicators on top of each slider section shows the current, and the edited color, and the hex value will be shown in the area below them. If you tap that area, the sliders will reset to the value of the unedited color.&lt;br /&gt;
The button with the keyboard symbol opens a dialog where you can put in the value directly.&lt;br /&gt;
The button on the left side opens the Color Selector dialog from Sailfish Silica, in case you want to select a color from there.&lt;br /&gt;
At the bottom below the RGB sliders you will find the Glow editor. This works a little different than the others, allowing you to select the hue of any UI elements that have a glow effect. Below the slider there is a set of buttons that apply either black, white, or the four default colors to the Glow color.</source>
        <translation type="vanished">&lt;li&gt;&lt;strong&gt;Schieberegler&lt;/strong&gt; Input-Modus ist der Hauptmodus.
Es gibt eine Reihe von Schiebereglern für jede Farbe. Benutze die zum Ändern der roten, grünen und blauen (RGB) Farbkanäle. 
Zwei schmale Anziegen über den Reglern zeigen die momentane und die geänderte Farbe an. Darunter wird er hexadezimale Wert dazu angezeigt. Wenn du auf den tippst setzen sich die Regler auf die ursprünglichen Farbwerte zurück.&lt;br /&gt;
Der Knopf mit dem Keyboardsymbol öffnet einen Dialog zur Texteingabe dieses Wertes.&lt;br /&gt;
Der Kopf links öffnet den Sailfish Farbdialog, für den Fall daß du eine Farbe von dort aussuchen willst.&lt;br /&gt;
Unter den RGB Reglern gibts noch den Leuchtfarbeneditor. Der funktioniert etwas anders, er selektiert den Farbton der Elemente mit Leuchteffekt. Unter dem Regler gibts ein paar Knöpfe die diese Farbe auf schwarz, weiss oder eine der vier Hauptfarben anpassen.</translation>
    </message>
    <message>
        <source>&lt;p&gt;The main UI consists of the following:
       &lt;ul&gt;
       &lt;li&gt;The Showroom&lt;/li&gt;
       &lt;li&gt;The Atelier&lt;/li&gt;
       &lt;li&gt;The Cupboards&lt;/li&gt;
       &lt;li&gt;Main Menu&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Die Hauptoberfläche besteht aus:
           &lt;ul&gt;
           &lt;li&gt;der Auslage&lt;/li&gt;
           &lt;li&gt;dem Atelier&lt;/li&gt;
           &lt;li&gt;den Stellagen&lt;/li&gt;
           &lt;li&gt;den Zieh-Menüs&lt;/li&gt;
            &lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The %1 shows the colors that are selected currently.
This is so you can preview and check how your creations will look.&lt;/p&gt;
&lt;p&gt;
There are two parts: the top one showing text in various combinations of colors, and the bottom one for the UI elements like buttons etc.
&lt;/p&gt;</source>
        <extracomment>argument is the translation of &quot;Showroom&quot;</extracomment>
        <translation type="vanished">&lt;p&gt;Der obere Bereich auf der ersten Seite (%1) zeigt die momentan geänderten Farben.
Hier siehst du eine Vorschau, wie deine Kreation aussehen wird.&lt;/p&gt;
&lt;p&gt;
Die Auslage hat zwei Teile: der obere zeigt Text in verschiedenen Farben, der untere Elemente der Benutzeroberfläche wie z.B. Knöpfe usw.
&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;&lt;strong&gt;Slider&lt;/strong&gt; input mode is the main editing mode.&lt;br /&gt;
There is a set of sliders for each color you can edit. Use them to adjust the colors using their red, green, blue (RGB) channels. 
Two slim indicators on top of each slider section shows the current, and the edited color, and the hex value will be shown in the area below them. If you tap that area, the sliders will reset to the value of the unedited color.&lt;br /&gt;
The button with the keyboard symbol opens a dialog where you can put in the value directly.&lt;br /&gt;
The button on the left side opens the Color Selector dialog from Sailfish Silica, in case you want to select a color from there.&lt;br /&gt;
At the bottom below the RGB sliders you will find the Glow editor. This works a little different than the others, allowing you to select the hue of any UI elements that have a glow effect. Below the slider there is a set of buttons that apply either black, white, or the four default colors to the Glow color.</source>
        <translation type="vanished">&lt;li&gt;&lt;strong&gt;Schieberegler&lt;/strong&gt; Input-Modus ist der Hauptmodus.
Es gibt eine Reihe von Schiebereglern für jede Farbe. Benutze die zum Ändern der roten, grünen und blauen (RGB) Farbkanäle. 
Zwei schmale Anziegen über den Reglern zeigen die momentane und die geänderte Farbe an. Darunter wird er hexadezimale Wert dazu angezeigt. Wenn du auf den tippst setzen sich die Regler auf die ursprünglichen Farbwerte zurück.&lt;br /&gt;
Der Knopf mit dem Keyboardsymbol öffnet einen Dialog zur Texteingabe dieses Wertes.&lt;br /&gt;
Der Kopf links öffnet den Sailfish Farbdialog, für den Fall daß du eine Farbe von dort aussuchen willst.&lt;br /&gt;
Unter den RGB Reglern gibts noch den Leuchtfarbeneditor. Der funktioniert etwas anders, er selektiert den Farbton der Elemente mit Leuchteffekt. Unter dem Regler gibts ein paar Knöpfe die diese Farbe auf schwarz, weiss oder eine der vier Hauptfarben anpassen.</translation>
    </message>
    <message>
        <source>&lt;li&gt;&lt;strong&gt;Generators:&lt;/strong&gt; this is a collection of various functions which manipulate all colors of the theme. Some are more useful than others, some are just for fun.
&lt;ul&gt;
&lt;li&gt;&lt;i&gt;Randomizer&lt;/i&gt; shuffles the color values to generate a random theme&lt;/li&gt;
&lt;li&gt;&lt;i&gt;Filters&lt;/i&gt; will change the existing theme colors slightly, e.g. darken or brighten them&lt;/li&gt;
&lt;li&gt;&lt;i&gt;Scheme Generators&lt;/i&gt; is a collection of things which will create a full theme&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;</source>
        <translation type="vanished">&lt;li&gt;&lt;strong&gt;Generatoren:&lt;/strong&gt; ist eine Sammlung von Funktionen die alle Farben gleichzeitig betreffen. Manche davon sind echt brauchber, andere eher zum Spaß.
&lt;ul&gt;
&lt;li&gt;&lt;i&gt;Zufallsgenerator&lt;/i&gt; würfelt die Farben ducheinander und erzeugt ein zufälliges Schema&lt;/li&gt;
&lt;li&gt;&lt;i&gt;Filter&lt;/i&gt; verändern die existierenden Farben leicht, z. B. um sie dunkler oder heller werden zu lassen&lt;/li&gt;
&lt;li&gt;&lt;i&gt;Schema Generatoren&lt;/i&gt; sind verschiedene Funktionen die ein komplettes Schema erzeugen&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;&lt;strong&gt;Swapper/Copier&lt;/strong&gt; is a helper mode which lets you copy the value of one color to another, or swap two values.&lt;/li&gt;</source>
        <translation type="vanished">&lt;li&gt;&lt;strong&gt;Tauschen/Kopieren&lt;/strong&gt; ist ein Hilfsmodus zum Vertauschen oder Kopieren von Farbwerten.&lt;/li&gt;</translation>
    </message>
    <message>
        <source>&lt;li&gt;&lt;strong&gt;Jolla Original&lt;/strong&gt; you already know, a remake of the element used in the Ambience Settings&lt;/li&gt;</source>
        <translation type="vanished">&lt;li&gt;&lt;strong&gt;Original Jolla&lt;/strong&gt; kennst du schon, ein Remake des Elementes das in den Ambientenoptionen auftaucht&lt;/li&gt;</translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <comment>Help Section</comment>
        <translation type="vanished">Der Palettenschrank</translation>
    </message>
    <message>
        <source>&lt;p&gt;
Cupboards are found to the right of the main page. These allow you to store your created palettes for re-use later.&lt;br /&gt;
The first page is the global Cupboard, where you can store any palette.
The second page is the Cupboard specific for the current Ambience, and its contents will change when the Ambience changes.
&lt;/p&gt;
&lt;p&gt;
Note that only Ambiences installed from a package can have a name, those created from Gallery will show as anonymous (for now)
&lt;/p&gt;
&lt;p&gt;
Per default the shelves are empty (showing all color pots as gray), but tapping the &lt;i&gt;%1&lt;/i&gt; button will save your current palette to that shelf, overwriting any values that may have been there.
The &lt;i&gt;%2&lt;/i&gt; button will load the stored palette and switch back to the main page.
&lt;/p&gt;
</source>
        <extracomment>arguments are the names of the menu entries &quot;Put on Shelf&quot; and &quot;Take to Atelier&quot;</extracomment>
        <translation type="vanished">&lt;p&gt;
Stellagen befunden sich rechts von der Hauptseite. Mit ihnen kannst du deine Farbpalette aufheben und später wiederverwenden.&lt;br /&gt;
Die erste Seite ist die allgemeine Stellage, wo jederzeit Paletten abgestellt werden können.
Die zweite Seite ist eine Ambiente-spezifische Stellage, ihr Inhalt wechselt wenn sich das Ambiente ändert.
&lt;/p&gt;
&lt;p&gt;
Übrigens haben nur installierte Anbiente Namen, selbsterstellte werden als Anonym angezeigt.&lt;/p&gt;
&lt;p&gt;
Zu beginn sind alle Regalbretter leer (alle Farbtöpfe sind grau), tippe den &lt;i&gt;%1&lt;/i&gt; Knopf um deine Palette auf das Regalbrett zu legen (es ersetzt die vorhandene).
Der &lt;i&gt;%2&lt;/i&gt; Knopf lädt die abgelegte Palette und wechselt zurück zur Hauptseite.
&lt;/p&gt;
</translation>
    </message>
    <message>
        <source>Put on Shelf</source>
        <translation type="vanished">Ablegen</translation>
    </message>
    <message>
        <source>Take to Atelier</source>
        <translation type="vanished">Mitnehmen</translation>
    </message>
    <message>
        <source>&lt;p&gt;
       The Main menu contains the most used commands.
       &lt;p&gt;
       &lt;ul&gt;
       &lt;li&gt;Apply: The central command of the app. This will apply the current color theme to the system.&lt;/li&gt;
       &lt;li&gt;Reload: Loads the theme that is currently in use in the system.&lt;/li&gt;
       &lt;li&gt;Export: opens the Export Ambience page. See the &lt;b&gt;Export&lt;/b&gt; chapter for more.&lt;/li&gt;
       &lt;li&gt;Advanced: takes you to a page containing various lesser used, experimental or destructive commands.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;
       Das Zieh-Menü enthält die meistgebrauchten Kommandos.
       &lt;p&gt;
       &lt;ul&gt;
       &lt;li&gt;Farben aufs System anwenden: Die Hauptfunktion der App. Bringt die momentan eingestellten Farben aus dem Atelier aufs System.&lt;/li&gt;
       &lt;li&gt;Farben vom System holen: Lädt das Schema das aktuell am System eingestellt ist.&lt;/li&gt;
       &lt;li&gt;Ambiente Paket exportieren: öffnet die Ambientenexportseite. Siehe das Kapitel &lt;b&gt;Export&lt;/b&gt;.&lt;/li&gt;
       &lt;li&gt;Erweitert: öffnet eine Seite, wo du verschiedene weniger of verwendete, experimentelle oder zerstörerische Funtionen findest.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;</translation>
    </message>
    <message>
        <source>Take to Lab</source>
        <translation type="vanished">Mitnehmen</translation>
    </message>
    <message>
        <source>Menus</source>
        <comment>Help Section</comment>
        <translation type="vanished">Menüs</translation>
    </message>
    <message>
        <source>&lt;p&gt;
       The PullDown menu contains the most used commands.
       &lt;p&gt;
       &lt;ul&gt;
       &lt;li&gt;Apply: The central command of the app. This will apply the current color theme to the system.&lt;/li&gt;
       &lt;li&gt;Reload: Loads the theme that is currently in use in the system.&lt;/li&gt;
       &lt;li&gt;Export: opens the Export Ambience page. See the &lt;b&gt;Export&lt;/b&gt; chapter for more.&lt;/li&gt;
       &lt;li&gt;Advanced: takes you to a page containing various lesser used, experimental or destructive commands.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;
       Das Zieh-Menü enthält die meistgebrauchten Kommandos.
       &lt;p&gt;
       &lt;ul&gt;
       &lt;li&gt;Farben aufs System anwenden: Die Hauptfunktion der App. Bringt die momentan eingestellten Farben aus dem Atelier aufs System.&lt;/li&gt;
       &lt;li&gt;Farben vom System holen: Lädt das Schema das aktuell am System eingestellt ist.&lt;/li&gt;
       &lt;li&gt;Ambiente Paket exportieren: öffnet die Ambientenexportseite. Siehe das Kapitel &lt;b&gt;Export&lt;/b&gt;.&lt;/li&gt;
       &lt;li&gt;Erweitert: öffnet eine Seite, wo du verschiedene weniger of verwendete, experimentelle oder zerstörerische Funtionen findest.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;
       The PullDown menu contains the most used commands.
       &lt;p&gt;
       &lt;ul&gt;
       &lt;li&gt;Apply: The central command of the app. This will apply the current color theme to the system.&lt;/li&gt;
       &lt;li&gt;Reload: Loads the theme that is currently in use in the system.&lt;/li&gt;
       &lt;li&gt;Export: opens the Export Ambience page. See the &lt;b&gt;Export&lt;/b&gt; chapter for more.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;
       &lt;p&gt;
       The PullUp menu at the bottom of the main page has various experimental or destructive commands.
       &lt;ul&gt;
       &lt;li&gt;Restart Lipstick: This will restart the whole Home Screen, closing all applications.&lt;/li&gt;
       &lt;li&gt;Restart ambienced: This will restart the ambience daemon, which will also reinitialize the colors from the current Ambience.&lt;/li&gt;
       &lt;li&gt;Reset: this will delete color values saved by the App in DConf. Use it if something went wrong and some UI elements have become unreadable.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;
       &lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;
       Das Zieh-Menü enthält die meistgebrauchten Kommandos.
       &lt;p&gt;
       &lt;ul&gt;
       &lt;li&gt;	Farben aufs System anwenden: Die Hauptfunktion der App. Bringt die momentan eingestellten Farben aus dem Atelier aufs System.&lt;/li&gt;
       &lt;li&gt;Farben vom System holen: Lädt das Schema das aktuell am System eingestellt ist.&lt;/li&gt;
       &lt;li&gt;	Ambiente Paket exportieren: öffnet die Ambientenexportseite. Siehe das Kapitel &lt;b&gt;Export&lt;/b&gt;.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;
       &lt;p&gt;
       Das Zieh-Menü ganz unten enthält verschiedene Funktionen, die entweder destruktiv oder experimentell sind.
       &lt;ul&gt;
       &lt;li&gt;Lipstick neu starten: startet den „Home Screen“ neu, dabei werden alle Apps geschlossen.&lt;/li&gt;
       &lt;li&gt;ambienced neu starten: startet den Ambiente-Dienst, was auch die Farben auf die des eingestellten Ambiente zurücksetzt.&lt;/li&gt;
       &lt;li&gt;Zurücksetzen: löscht die gespeicherten Farbwerte von DConf. Verwende das falls was schief gelaufen ist und Oberflächenelemente unlesbar geworden sind.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;
       &lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>LabPage</name>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="38"/>
        <source>Atelier</source>
        <translation>Atelier</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="38"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="38"/>
        <source>Cupboards</source>
        <translation>Stellagen</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="43"/>
        <source>Current Palette</source>
        <translation>Aktuelle Palette</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="48"/>
        <source>Editor</source>
        <comment>tool/editing mode</comment>
        <translation>Editor</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="49"/>
        <source>Tap to switch</source>
        <translation>Tippen zum Umstellen</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="57"/>
        <source>Sliders</source>
        <translation>Schieber</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="58"/>
        <source>Swapper/Copier</source>
        <translation>Tauschen/Kopieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="59"/>
        <source>Generators</source>
        <translation>Generatoren</translation>
    </message>
    <message>
        <location filename="../qml/pages/LabPage.qml" line="60"/>
        <source>Jolla Original</source>
        <translation>Original „Jolla“</translation>
    </message>
</context>
<context>
    <name>LoadAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="36"/>
        <source>Load</source>
        <translation>Laden</translation>
    </message>
    <message>
        <source>Load File</source>
        <translation type="vanished">Datei laden</translation>
    </message>
    <message>
        <source>Warning:</source>
        <translation type="vanished">Achtung!</translation>
    </message>
    <message>
        <source>The features below are a developer preview and do not work.</source>
        <translation type="vanished">The folgenden Features sind unter Entwicklung unf funktionieren noch nicht.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="97"/>
        <source>Load Theme</source>
        <translation>Schema laden</translation>
    </message>
    <message>
        <source>Take to Lab</source>
        <translation type="vanished">Mitnehmen</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="97"/>
        <source>Take to Atelier</source>
        <translation>Mitnehmen</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="115"/>
        <source>Here you can load a .ambience (JSON) file.</source>
        <translation>Hier kannst du eine .ambience (JSON)-Datei laden.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="119"/>
        <source>current</source>
        <translation>momentan</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="125"/>
        <source>file data</source>
        <translation>Dateidaten</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="131"/>
        <source>File name </source>
        <translation>Dateiname </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="133"/>
        <source>Click to select</source>
        <translation>Klicke um auszuwählen</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="140"/>
        <source>File content</source>
        <translation>Dateiinhalt</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="149"/>
        <source>No file loaded</source>
        <translation>Keine Datei geladen</translation>
    </message>
</context>
<context>
    <name>LootBoxItem</name>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="27"/>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="32"/>
        <source>ThemeColor</source>
        <translation>ThemeColor</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="31"/>
        <source>A Lootbox was delivered!</source>
        <translation>Eine Lootbox wurde geliefert!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="32"/>
        <source>now has more shelves!</source>
        <translation>besitzt jetzt mehr Regalbretter!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="32"/>
        <source>Your persistence has been rewarded.</source>
        <translation>Deine Beharrlichkeit wurde belohnt.</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="33"/>
        <source>Your persistence has been rewarded!</source>
        <translation>Deine Beharrlichkeit wurde belohnt!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="43"/>
        <source>Purchase Options</source>
        <translation>Kaufoptionen</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="52"/>
        <source>Payment* received!</source>
        <translation>Zahlung* erhalten!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="52"/>
        <source>Buy more shelves</source>
        <translation>Jetzt mehr Lagerplatz kaufen</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="57"/>
        <source>Using Jolla Shop credentials to buy Storage Lootbox</source>
        <translation>Kaufe Lagerplatz-Lootbox via Jolla Store</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="74"/>
        <source>Thank you for your purchase!&lt;br /&gt;Your extra shelves will be delivered in the next update!</source>
        <translation>Danke für deinen Einkauf!&lt;br /&gt;Deine neuen Regalbretter kommen mit dem nächsten Update mit!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="88"/>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;…or did it?</source>
        <translation>*) Jetzt mal im Ernst. Natürlich gibts in dieser App keine in-app Käufe oder auch Loot-Boxen im Jolla Shop. Das wär ja lächerlich.&lt;br /&gt;Es ist nichts bezahlt worden.&lt;br /&gt;Genaugenommen ist gerade gar nichts passiert.&lt;br /&gt;… oder doch?</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="9"/>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation>Handbuch</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="12"/>
        <source>Advanced…</source>
        <translation>Erweitert…</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="15"/>
        <source>Export to Ambience package</source>
        <translation>Ambiente Paket exportieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="18"/>
        <source>Apply Colors to System</source>
        <translation>Farben aufs System anwenden</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="19"/>
        <source>Applying</source>
        <translation>Anwenden</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainMenu.qml" line="21"/>
        <source>Reload Colors from System</source>
        <translation>Farben vom System holen</translation>
    </message>
</context>
<context>
    <name>SaveAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="53"/>
        <source>Export Functions</source>
        <translation>Exportfunktionen</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="67"/>
        <source>Here you can export your creation to a .ambience (JSON) file, create an installable package from it, and optionally install that on your device.</source>
        <translation>Hier kannst du deine Kreation in eine (JSON) Datei exportieren, als RPM-Paket abspeichern und danach (optional) installieren.</translation>
    </message>
    <message>
        <source>To do that, first enter a name for your %1, then tap the &amp;quot;%2&amp;quot; button. After this, you can either edit the file, or generate a package from it.</source>
        <translation type="vanished">Dazu gib als erstes deinem %1 einen Namen und tippe den &amp;quot;%2&amp;quot; Knopf. Danach kannst du die Datei editieren, oder ein Paket daraus erstellen.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="69"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <source>Both the .ambience file as well as any built packages will be stored in the Documents folder. (%1)</source>
        <translation type="vanished">Sowohl die .ambience-Datei als auch alle erstellten Pakete werden im Ordner &quot;Dokumente&quot; gespeichert. (%1)</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="69"/>
        <source>First, enter a name for your %1. You can edit the file by clicking the name, and generate a package from it using the Builder Button.</source>
        <translation>Dazu gib als erstes deinem %1 einen Namen. Danach kannst du die Datei editieren, oder ein Paket daraus erstellen.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="71"/>
        <source>The .ambience file as well as any built packages will be stored in the %1 folder. (%2)</source>
        <comment>first argument is the short name of the location, second argument is the full path</comment>
        <translation>Sowohl die .ambience Datei als auch die Pakete werden im %1 Ordner abgelegt. (%2)</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="89"/>
        <source>Export to File</source>
        <translation>In Datei exportieren</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="94"/>
        <source>Ambience Name</source>
        <translation>Ambientename</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="191"/>
        <source>Click to Open</source>
        <translation>Zum Öffnen klicken</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="202"/>
        <source>Build Package</source>
        <translation>Paket bauen</translation>
    </message>
    <message>
        <source>Click to prepare</source>
        <translation type="vanished">Klicke zur Vorbereitung</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="192"/>
        <source>File Name</source>
        <translation>Dateiname</translation>
    </message>
    <message>
        <source>Open File</source>
        <translation type="vanished">Datei öffnen</translation>
    </message>
    <message>
        <source>Launch Builder</source>
        <translation type="vanished">Builder starten</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="74"/>
        <source>Disclaimer</source>
        <translation>Hinweis</translation>
    </message>
    <message>
        <source>Ambience Package</source>
        <translation type="vanished">Ambientepaket</translation>
    </message>
    <message>
        <source>Both the .ambience file as well as any built packages will be stored in the %1 folder. (%2)</source>
        <comment>first argument is the short name of the locatin, second argument is the full path</comment>
        <translation type="vanished">Sowohl die .ambience Datei als auch die Pakete werden im %1 Ordner abgelegt. (%2)</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="71"/>
        <source>Documents</source>
        <translation>Dokumente</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="86"/>
        <source>If you decide to share or otherwise distribute the package make sure you respect copy- and other rights for any content included.</source>
        <translation>Stelle sicher, daß die Rechte etwaiger Content-Besitzer gewahrt werden, solltest du das Paket teilen oder anders verbreiten.</translation>
    </message>
    <message>
        <source>If you decide to share or otherwise distribute the package make sure you respect copy- and other rights for any content included in them.</source>
        <translation type="vanished">Stelle sicher, daß die Rechte etwaiger Content-Besitzer gewahrt werden, solltest du das Paket teilen oder anders verbreiten.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="96"/>
        <source>A cool Ambience Name</source>
        <translation>ein cooler Name</translation>
    </message>
    <message>
        <source>Click to export</source>
        <translation type="vanished">Klicken zum Export</translation>
    </message>
</context>
<context>
    <name>SaveSlot</name>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="45"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="46"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="45"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="46"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="48"/>
        <source>Shelf</source>
        <translation>Regalfach</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <source>default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="45"/>
        <source>night mode</source>
        <translation>Nachtmodus</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="75"/>
        <source>Take to Atelier</source>
        <translation>Mitnehmen</translation>
    </message>
    <message>
        <source>Take to Lab</source>
        <translation type="vanished">Mitnehmen</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="76"/>
        <source>Put on Shelf</source>
        <translation>Ablegen</translation>
    </message>
</context>
<context>
    <name>Saver</name>
    <message>
        <location filename="../qml/pages/Saver.qml" line="103"/>
        <source>Global Cupboard</source>
        <translation>Hauptstellage</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="104"/>
        <source>Atelier</source>
        <translation>Atelier</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="104"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="132"/>
        <source>Clean out this cupboard</source>
        <translation>Entrümpeln</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="132"/>
        <source>Spring Clean</source>
        <translation>Frühjahrsputz</translation>
    </message>
</context>
<context>
    <name>SaverPlus</name>
    <message>
        <source>anonymous</source>
        <translation type="vanished">incognito</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="120"/>
        <source>Ambience Cupboard</source>
        <translation>Ambientenstellage</translation>
    </message>
    <message>
        <source>default</source>
        <translation type="vanished">Standard</translation>
    </message>
    <message>
        <source>night mode</source>
        <translation type="vanished">Nachtmodus</translation>
    </message>
    <message>
        <source>Set up daemon</source>
        <translation type="vanished">Daemoneinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="20"/>
        <source>User Ambience</source>
        <translation>Benutzerambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="110"/>
        <source>System Ambience</source>
        <translation>Systemambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="121"/>
        <source>Global Cupboard</source>
        <translation>Hauptstellage</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="171"/>
        <source>Clean out this cupboard</source>
        <translation>Entrümpeln</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="171"/>
        <source>Spring Clean</source>
        <translation>Frühjahrsputz</translation>
    </message>
</context>
<context>
    <name>ShowRoom</name>
    <message>
        <source>Text Elements</source>
        <translation type="vanished">Textelemente</translation>
    </message>
    <message>
        <source>UI Elements</source>
        <translation type="vanished">Bedienelemente</translation>
    </message>
</context>
<context>
    <name>ShowRoomMini</name>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="25"/>
        <source>Mini</source>
        <comment>small showroom</comment>
        <translation>Miniatur</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="25"/>
        <source>Showroom</source>
        <translation>Auslage</translation>
    </message>
</context>
<context>
    <name>ShowRoomText</name>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Hauptfarbe</translation>
    </message>
    <message>
        <source>A very long line showing Text in</source>
        <translation type="vanished">Eine relativ lange Zeile mit Text in</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Zweitfarbe</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Betonungsfarbe</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Zweitfarbe für Betonungen</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Text</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Hintergrund für Betonungen</translation>
    </message>
    <message>
        <source>Overlay Background Color</source>
        <translation type="vanished">Hintergrund für Überlagerungen</translation>
    </message>
    <message>
        <source>Wallpaper Overlay Color</source>
        <translation type="vanished">Überlagerung für Hintergrundbild</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">Dunkle Betonungsfarbe</translation>
    </message>
    <message>
        <source>Cover Background Color</source>
        <translation type="vanished">Hintergrund für Covers</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="15"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="22"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="37"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="44"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="51"/>
        <source>%1 text</source>
        <comment>%1 is the name of a color</comment>
        <translation>Text in %1</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="15"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="37"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="51"/>
        <source>Primary</source>
        <translation>Hauptfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="16"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="23"/>
        <source>No Background</source>
        <translation>Kein Hintergrund</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="22"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="44"/>
        <source>Secondary</source>
        <translation>Sekundärfarbe</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="30"/>
        <source>Highlight Background</source>
        <translation>Hintergrund für Betonungen</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="38"/>
        <source>Overlay Background</source>
        <translation>Hintergrund für Überlagerungen</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="45"/>
        <source>Cover Background</source>
        <translation>Hintergrund für Covers</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="52"/>
        <source>Wallpaper Overlay</source>
        <translation>Überlagerung für Hintergrundbild</translation>
    </message>
</context>
<context>
    <name>ShowRoomUI</name>
    <message>
        <source>Progress Bar Demo</source>
        <translation type="vanished">Fortschrittsbalkendemo</translation>
    </message>
    <message>
        <source>Remorse Item Demo</source>
        <translation type="vanished">„Remorse Item“ Demo</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="132"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="133"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="134"/>
        <source>MenuItem</source>
        <translation>Menüelement</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="134"/>
        <source>selected</source>
        <translation>angewählt</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="133"/>
        <source>disabled</source>
        <translation>deaktiviert</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="20"/>
        <source>Waiting…</source>
        <translation>Warte…</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="28"/>
        <source>Feeling Remorse</source>
        <translation>Empfinde Reue</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="108"/>
        <source>App</source>
        <translation>App</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="109"/>
        <source>Clicked</source>
        <translation>Angetippt</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="110"/>
        <source>Dead</source>
        <translation>Tot</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="154"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="155"/>
        <source>Button</source>
        <translation>Knopf</translation>
    </message>
</context>
<context>
    <name>TransparencyEditor</name>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="45"/>
        <source>Edit Transparency</source>
        <translation>Transparenz anpassen</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="53"/>
        <source>This will edit the alpha channel of the color. Note that in the Sailfish UI many elements use their own transparency values and are not affected by the alpha channel of the color. Remorse Timers are one such example.</source>
        <translation>Hier veränderst du den Alpha-Kanal der Farben. NB: Im Sailfish UI verwenden viele Elemente ihre eigenen Werte für Transparenz, und werden nicht von der Farbe beeinflusst. Der Remorse Timer zum Beispiel.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="82"/>
        <source>Highlight Background Opacity</source>
        <translation>Durchsichtigkeit betonter Hintergrund</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="83"/>
        <source>This is used e.g. for Pulley Menu background.</source>
        <translation>wird z.B. für den Menü-Hintergrund verwendet.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="85"/>
        <source>Wallpaper Overlay Opacity</source>
        <translation>Transparenz des Hintergrundbild-Overlays</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="87"/>
        <source>Cover Overlay Opacity</source>
        <translation>Transparenz der App-Cover</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="88"/>
        <source>Transparency of application covers.</source>
        <translation>Transparenz von App Covers.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="94"/>
        <source>Color Alpha Channel</source>
        <translation>Alpha-Kanal</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="95"/>
        <source>Here you can edit Alpha channels for colors that have one.</source>
        <translation>Editiere hier den Alpha-Kanal für Farben, die einen solchen besitzen.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="96"/>
        <source>Secondary Color</source>
        <translation>Zweitfarbe</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="98"/>
        <source>Secondary Highlight Color</source>
        <translation>Zweitfarbe für Betonungen</translation>
    </message>
</context>
</TS>
