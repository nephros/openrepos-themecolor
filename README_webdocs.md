At the moment, the manual is translated from the webdocs branch via weblate.

Weblate Config is in https://hosted.weblate.org/settings/theme-color/handbookgeneral/, the other components inherit from that.

Flow is:

 1. changes in doc/en/*.html are done in local webdocs branch
 1. Commit
 1. push to upstream (gitlab)/repo/webdocs
 1. Weblate wil sync that
 1. Translate at Weblate Web Site
 1. Commit & push at Weblate Web Site
 1. pull to local webdocs branch
 1. merge to local master
 1. push to master
