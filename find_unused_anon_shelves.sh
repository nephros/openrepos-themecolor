#!/usr/bin/env bash

# This file is part of ThemeColor.
# Copyright (c) 2021 Peter Gantner (nephros)
# SPDX-License-Identifier: MIT

AMBDIR=${HOME}/.local/share/ambienced/wallpapers/
DCLOC=/org/nephros/openrepos-themecolor/storage

printf -- '-------\n#### these are orphan keys, execute the following to remove them:\n\n'
for cupboard in $(dconf list  /org/nephros/openrepos-themecolor/storage/ | grep -E '^.{34}/$'); do

  if [ ! -e ${AMBDIR}/${cupboard:0:34}.jpg ]; then
	printf 'dconf reset -f %s/%s\n' ${DCLOC} ${cupboard}
  fi
done


printf "\n#### that's all\n-------\n"
