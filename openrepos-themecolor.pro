
TARGET = openrepos-themecolor
CONFIG += lrelease embed_translations sailfishapp_i18n

lupdate_only {
SOURCES += \
    qml/$${TARGET}.qml \
    qml/pages/*.qml \
    qml/cover/*.qml \
    qml/components/*.qml \
    qml/components/*/*.qml

}

TRANSLATIONS += translations/$${TARGET}-de.ts \
                translations/$${TARGET}-en_GB.ts \
                translations/$${TARGET}-es.ts \
                translations/$${TARGET}-fr.ts \
                translations/$${TARGET}-nb_NO.ts \
                translations/$${TARGET}-sv.ts \
                translations/$${TARGET}-zh_CN.ts

desktop.files = $${TARGET}.desktop
desktop.path = /usr/share/applications
INSTALLS += desktop

qml.files = qml
qml.path = /usr/share/$${TARGET}

INSTALLS += qml

OTHER_FILES += $$files(rpm/*)
OTHER_FILES += $$files(files/*)

include(translations/translations.pri)
include(icons/icons.pri)
